
// STL includes
#include <chrono>
#include <cmath>
#include <complex>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <iomanip>
#include <cstdlib>
#include <memory>
#include <unsupported/Eigen/FFT>

// JSON Includes
#include <nlohmann/json.hpp>

// ArcaWave includes
#include "Geometry/Environment.h"
#include "Radar/RadarPipeline.h"
#include "Radar/Radiation/PlaneWave.h"
#include "Radar/Radiation/PMCW.h"
#include "Geometry/RayTracing/CPURayTracer.h"
#include "Radar/UniformDiscretizer.h"
#include "Utility/Constants.h"
#include "Utility/Interval.h"
#include "Utility/Logs.h"
#include "Radar/BakingPipeline.h"
#include "Radar/Calculator/PO.h"
#include "Radar/DSP/PhaseDSP.h"
#include "Radar/DSP/SignalProcessing.h"

// CONFIG INCLUDE
#include "PipelineConfig.h"

//CUDA INCLUDES
#ifdef USE_CUDA
    #include "Geometry/RayTracing/OptixRayTracer.cuh"
    #include "Radar/CUDARadarPipelne.h"
#endif

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Geometry;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

// Simulation parameters
uint_fast32_t max_threads               = 92;
uint_fast32_t max_jobs                  = 1;
uint_fast32_t burst 					= 2;
uint_fast32_t frame_count               = 1 * burst;

// Antenna parameters
T start_freq                            = 58.869701726e9;
T freq_slope                            = 1.5e12;
uint_fast32_t sample_count              = 1024;
T sample_rate                           = 1e9;
Matrix<T, V, 1> propogation             = Matrix<T, V, 1>(1, 0, 0);
//Matrix<T, V, 1> propogation             = Matrix<T, V, 1>(10, 10, 0).normalized();
Matrix<complex<T>, 2, 1> polarization   = Matrix<complex<T>, 2, 1>(1, 0);
T antenna_d                             = C_0<T> / start_freq / 2;
//T antenna_d                             = 0.0025;
T frame_time                            = (1 / sample_rate) * sample_count;

int main(int argc, const char* argv[])
{
    std::filesystem::path path_to_env = (argv[1]);
    std::filesystem::path path_to_output = (argv[2]);

    cout << "Propogation: " << propogation << endl;

    uint_fast32_t tx_sample_count = 32 * 1024;
    const uint_fast32_t tx_c = 4;
    std::shared_ptr<std::vector<std::complex<T>>> sampled_tx[tx_c];
    ifstream tx_samples_file("../../tx.csv");
    for(int j = 0; j < tx_c; ++j)
        sampled_tx[j] = std::make_shared<std::vector<std::complex<T>>>();
    for(int i = 0; i < tx_sample_count; ++i)
    {
        for(int j = 0; j < tx_c; ++j)
        {
            T r_pt;
            T i_pt;
            tx_samples_file >> r_pt;
            tx_samples_file >> i_pt;
            std::complex<T> c(r_pt, i_pt);
            sampled_tx[j]->push_back(c);
            char delim;
            tx_samples_file >> delim;
            tx_samples_file >> delim;
        }
        std::string rest;
        tx_samples_file >> rest;
    }
    cout << "Samples inputed" << endl;
    
    // Creates the thread pool
    shared_ptr<ThreadPool> thrd_pool = make_shared<ThreadPool>(max_threads, max_jobs);

    // Import the environment
    shared_ptr<Environment<T, V>> env = make_shared<Environment<T, V>>();
    env->ImportEnvironment(path_to_env.string());
    cout << "Imported" << endl;

    // Makes the ray tracer
    #ifdef USE_CUDA
        shared_ptr<RayTracer<T, V>> rtx = make_shared<OptixRayTracer<T, V>>(env);
    #else
        shared_ptr<RayTracer<T, V>> rtx = make_shared<CPURayTracer<T, V>>(env);
    #endif
    cout << "Raytracer chosen" << endl;

    // Makes the discretizer
    shared_ptr<Discretizer<T, V>> disc = make_shared<UniformDiscretizer<T, V>>();

    // Makes the calculator
    shared_ptr<CurrentCalculator<T, V>> calc = make_shared<PO<T, V>>();

    cout << "Loading env" << endl;
    nlohmann::json json_file;
    ifstream(path_to_env.string()) >> json_file;
    Matrix<T, V, V> radar_ori;
    radar_ori = Map<Eigen::Matrix<T, V, V>> (json_file["radar_params"]["orientation"].get<std::vector<T>>().data());
    env->TransmitterOri = radar_ori;
    Matrix<T, V, 1> tx_pos((T)json_file["radar_params"]["tx_position"][0], (T)json_file["radar_params"]["tx_position"][1], (T)json_file["radar_params"]["tx_position"][2]);
    Matrix<T, V, 1> rx_pos((T)json_file["radar_params"]["rx_position"][0], (T)json_file["radar_params"]["rx_position"][1], (T)json_file["radar_params"]["rx_position"][2]);

    // Creates the radar
    propogation = env->TransmitterOri * propogation;
//    shared_ptr<RadiationField<T, V>> rad_field = make_shared<PlaneWave<T, V>>(propogation, polarization, env->TransmitterPos, start_freq, freq_slope);
    shared_ptr<RadiationField<T, V>> rad_field = make_shared<PMCW<T, V>>(propogation, polarization, env->TransmitterPos, start_freq, freq_slope);

    // Creates the list of transmitters
    uint_fast32_t tx_width = (uint_fast32_t)json_file["radar_params"]["tx_width"];
    uint_fast32_t tx_height = (uint_fast32_t)json_file["radar_params"]["tx_height"];

    // Creates the list of receivers
    uint_fast32_t rx_width = (uint_fast32_t)json_file["radar_params"]["rx_width"];
    uint_fast32_t rx_height = (uint_fast32_t)json_file["radar_params"]["rx_height"];

    nlohmann::json radar_config;
    cout << "Loading radar" << endl;
    ifstream("../configs/uhnder_radar.json") >> radar_config;

    shared_ptr<Matrix<T, V, 1>[]> transmitters = shared_ptr<Matrix<T, V, 1>[]>(new Matrix<T, V, 1>[tx_width * tx_height]);
    shared_ptr<Matrix<T, V, V>[]> trans_orientations = shared_ptr<Matrix<T, V, V>[]>(new Matrix<T, V, V>[tx_width * tx_height]);
    for(uint_fast32_t i = 0; i < tx_width; ++i)
    {
        for(uint_fast32_t j = 0; j < tx_height; ++j)
        {
            auto pos = radar_config["tx_position"][i];
            transmitters[(i * tx_height) + j] = env->TransmitterOri * Matrix<T, V, 1>(pos[0], pos[1], pos[2]) + tx_pos;
            trans_orientations[(i * tx_height) + j] = env->TransmitterOri;
        }
    }
    const uint_fast32_t tx_count = tx_width * tx_height;

    shared_ptr<Matrix<T, V, 1>[]> receivers = shared_ptr<Matrix<T, V, 1>[]>(new Matrix<T, V, 1>[rx_width * rx_height]);
    for(uint_fast32_t i = 0; i < rx_width; ++i)
    {
        for(uint_fast32_t j = 0; j < rx_height; ++j)
        {
            auto pos = radar_config["rx_position"][i];
            receivers[(i * rx_height) + j] = env->TransmitterOri * Matrix<T, V, 1>(pos[0], pos[1], pos[2]) + rx_pos;
        }
    }
    uint_fast32_t rx_count = rx_width * rx_height;
    cout << "radar inputed" << endl;

    // Creates the radar pipeline
    RadarPipelineInfo<T, V> rp_info;
    rp_info.ThrdPool            = thrd_pool;
    rp_info.Env                 = env;
    rp_info.RTX                 = rtx;
    rp_info.Disc                = disc;
    // rp_info.RadField            = RAD_FILED;
    rp_info.SampleCount         = sample_count;
    rp_info.SampleRate          = sample_rate;
    // rp_info.TransmitterCount    = tx_width * tx_height;
    rp_info.TransmitterCount    = 1;
    rp_info.Transmitters        = transmitters;
    rp_info.TransOrientations   = trans_orientations;
    rp_info.ReceiverCount       = rx_width * rx_height;
    rp_info.Receivers           = receivers;

    // Simulates each frame of the environment
    const uint_fast32_t output_size = rx_width * rx_height * frame_count * sample_count * tx_count;
    shared_ptr<Matrix<complex<T>, V, 1>[]> output = shared_ptr<Matrix<complex<T>, V, 1>[]>(new Matrix<complex<T>, V, 1>[output_size]);
    for (uint_fast32_t i = 0; i < frame_count/burst; ++i)
	{
		for (uint_fast32_t j = 0; j < burst; ++j)
		{
			cout << "Frame " << i << " Burst " << j << endl;

			// Initializes the radar frame
			shared_ptr<RadarFrame<T, V>> frame = make_shared<RadarFrame<T, V>>();
			frame->Time = j*frame_time;

			// Runs the frame
			// radar_pipeline->RunFrameNearField(frame);
			auto begin = chrono::system_clock::now();   
            for(int m = 0; m < tx_count; m++){
                // -----MIMO
                shared_ptr<RadiationField<T, V>> rad_field = make_shared<PlaneWave<T, V>>(propogation, polarization, transmitters[m], start_freq, freq_slope);
                rp_info.RadField = rad_field;
                rp_info.Transmitters[0] = transmitters[m];
                rp_info.TXSamples = sampled_tx[m];
                rp_info.TXSampleCount = tx_sample_count; // Number of samples
                rp_info.Time = frame->Time;
                shared_ptr<RadarPipeline<T, V>> radar_pipeline = make_shared<CUDARadarPipeline<T, V>>(rp_info);
                // -----
                radar_pipeline->RunFrameFarField(frame);
                auto end = chrono::system_clock::now();
                chrono::duration<double> t = end - begin;
                cout << "FRAME Completed in " << t.count() << " seconds" << endl;

                // TODO: handle multiple transmitters
                // Transfers the mixed signal to the output
                for (uint_fast32_t k = 0; k < (rx_count * sample_count); ++k)
                    output[((i*burst + j) * sample_count * rx_count * tx_count) + (m * sample_count * rx_count) + k] = frame->RXSignal[k];

                // Update within a burst
                env->ApplyVelocities(frame_time);

                // Apply DSP
                // DSPprocess<T,V> DSPinfo;
                // DSPinfo.r_rows = rx_height;
                // DSPinfo.r_cols = rx_width;
                // DSPinfo.num_samples = sample_count;
                // shared_ptr<CUDAFourierTransform<T, V>> Radar_FFT = make_shared<CUDAFourierTransform<T, V>>(frame->MXSignal,DSPinfo);

                // Radar_FFT->SaveResult("data.npy");
                // Radar_FFT->SaveThresholdedResult("threshold.npy");

            }
		}

		// Update SLow Time
		env->UpdateEnvironment();
	}
    
    // Outputs the data
	cout << "Data output begin " << endl;
    std::filesystem::create_directories(path_to_output.parent_path());
	ofstream out(path_to_output, ofstream::out);
    for(uint_fast32_t i = 0; i < output_size; ++i)
    {
        out << output[i].x().real() << " " << output[i].x().imag() << " "
            << output[i].y().real() << " " << output[i].y().imag() << " "
            << output[i].z().real() << " " << output[i].z().imag() << endl;
    }
    out.close();

    cout << "Data output end" << endl;

    return 0;
}