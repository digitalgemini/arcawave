
// STL includes
#include <chrono>
#include <complex>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <vector>

// JSON include
#include <nlohmann/json.hpp>

// ArcaWave includes
#include "Geometry/RayTracing/CPURayTracer.h"
#include "Radar/BakingPipeline.h"
#include "Radar/Calculator/CurrentCalculator.h"
#include "Radar/RadarObject.h"
#include "Radar/UniformDiscretizer.h"
#include "Utility/Constants.h"
#include "Utility/Colormap.h"
#include "Utility/ThreadPool.h"

// Used namespaces
using namespace std;
using namespace nlohmann;
using namespace Eigen;
using namespace ArcaWave::Geometry;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

// Fields used in the JSON configuration
static const string CONFIG_OBJ_PATH     = "object_path";
static const string CONFIG_THREADS      = "threads";
static const string CONFIG_MODE         = "mode";
static const string CONFIG_FREQ         = "frequency";
static const string CONFIG_POLAR        = "polarization";
static const string CONFIG_INC_THETA    = "inc_theta";
static const string CONFIG_INC_PHI      = "inc_phi";
static const string CONFIG_INC_WRAP     = "inc_wrap";
static const string CONFIG_REF_THETA    = "ref_theta";
static const string CONFIG_REF_PHI      = "ref_phi";
static const string CONFIG_REF_WRAP     = "ref_wrap";
static const string CONFIG_X_SYM        = "x_symmetry";
static const string CONFIG_Y_SYM        = "y_symmetry";
static const string CONFIG_Z_SYM        = "z_symmetry";
static const string CONFIG_MESHES       = "meshes";
static const string CONFIG_MESH         = "mesh";
static const string CONFIG_PART         = "partitions";
static const string CONFIG_PART_VERT    = "vertex";
static const string CONFIG_PART_WEIGHT  = "weight";
static const string CONFIG_PART_ENABLE  = "enabled";

template<typename T, uint_fast32_t V>
void ExportMeshes(const json& Config)
{
    // Gets the object
    shared_ptr<Object<T, V>> obj = Object<T, V>::Import(Config[CONFIG_OBJ_PATH]);
    const uint_fast32_t mesh_count = obj->GetMeshCount();

    // Creates a new sub-object for each mesh and exports it
    for(uint_fast32_t i = 0; i < mesh_count; ++i)
    {
        // Creates a list of submeshes, all of which are empty except one
        shared_ptr<shared_ptr<Submesh>[]> submeshes = shared_ptr<shared_ptr<Submesh>[]>(new shared_ptr<Submesh>[mesh_count]);
        for(uint_fast32_t j = 0; j < mesh_count; ++j)
            if(i == j)
                submeshes[j] = obj->GetMesh(j)->GetFullSubmesh();
            else
                submeshes[j] = make_shared<Submesh>();

        // Creates the new sub-object and exports it
        shared_ptr<Object<T, V>> mesh_obj = obj->BuildSubObject(submeshes);
        Object<T, V>::Export(mesh_obj, string("./mesh_").append(to_string(i)).append(".obj"), EX_OBJ_NO_MATERIAL);
    }
}

template<typename T, uint_fast32_t V>
shared_ptr<RadarObject<T, V>> BuildRadarObject(const json& Config)
{
    // Creates the creation info needed for the radar object
    shared_ptr<RadarObjectCreateInfo<T, V>> info = make_shared<RadarObjectCreateInfo<T, V>>();
    info->Obj = Object<T, V>::Import(Config[CONFIG_OBJ_PATH]);
    info->XSym = Config[CONFIG_X_SYM];
    info->YSym = Config[CONFIG_Y_SYM];
    info->ZSym = Config[CONFIG_Z_SYM];

    // Counts the total number of partitions from the config file
    uint_fast32_t total_part_count = 0;
    const uint_fast32_t mesh_count = Config[CONFIG_MESHES].size();
    for(uint_fast32_t i = 0; i < mesh_count; ++i)
        total_part_count += Config[CONFIG_MESHES][i][CONFIG_PART].size();

    // Parses all of the partitions in the config file 
    info->PartitionCount    = total_part_count;
    info->MeshIndicies      = shared_ptr<uint_fast32_t[]>(new uint_fast32_t[info->PartitionCount]);
    info->Partitions        = shared_ptr<shared_ptr<Partition<T, V>>[]>(new shared_ptr<Partition<T, V>>[info->PartitionCount]);
    uint_fast32_t index = 0;
    for(uint_fast32_t i = 0; i < mesh_count; ++i)
    {
        const uint_fast32_t mesh_idx = Config[CONFIG_MESHES][i][CONFIG_MESH];
        const uint_fast32_t part_count = Config[CONFIG_MESHES][i][CONFIG_PART].size();
        for(uint_fast32_t j = 0; j < part_count; ++j)
        {
            json part = Config[CONFIG_MESHES][i][CONFIG_PART][j];
            Matrix<T, V, 1> vert(static_cast<T>(part[CONFIG_PART_VERT][0]), static_cast<T>(part[CONFIG_PART_VERT][1]), static_cast<T>(part[CONFIG_PART_VERT][2]));
            info->MeshIndicies[index] = mesh_idx;
            info->Partitions[index] = make_shared<Partition<T, V>>(vert, static_cast<T>(part[CONFIG_PART_WEIGHT]), static_cast<bool>(part[CONFIG_PART_ENABLE]));
            ++index;
        }
    }

    // Creates and returns the new radar object
    return make_shared<RadarObject<T, V>>(info);
}

template<typename T, uint_fast32_t V>
void ExportPartition(const shared_ptr<RadarObject<T, V>>& RadObj, const string& Filename)
{
    // Colors each mesh (representing a partition) with a random color
    uint_fast32_t mesh_count = RadObj->GetMeshCount();
    default_random_engine rng;
    uniform_real_distribution<double> dist(0.0, 1.0);
    for(uint_fast32_t i = 0; i < mesh_count; ++i)
    {
        shared_ptr<Mesh<T, V>> mesh = RadObj->GetMesh(i);
        shared_ptr<Partition<T, V>> part = RadObj->GetPartition(i);
        if(part->Enabled)
            mesh->MaterialInfo.MaterialColor = {static_cast<T>(dist(rng)), static_cast<T>(dist(rng)), static_cast<T>(dist(rng)), 1.0};
        else
            mesh->MaterialInfo.MaterialColor = {0, 0, 0, 1.0};
    }

    // Exports the object
    Object<T, V>::Export(RadObj, Filename, EX_OBJ);
}

template<typename T, uint_fast32_t V>
void BakeObject(const json& Config, const string& Output)
{
    // Creates the VCM build info
    shared_ptr<VCMBuildInfo<T, V>> build = make_shared<VCMBuildInfo<T, V>>();
    build->Mode = static_cast<VCMMode>(Config[CONFIG_MODE]);
    build->Frequency = static_cast<T>(Config[CONFIG_FREQ]);
    build->Polarization = Matrix<complex<T>, 2, 1>
    (
        complex<T>(Config[CONFIG_POLAR][0], Config[CONFIG_POLAR][1]), 
        complex<T>(Config[CONFIG_POLAR][2], Config[CONFIG_POLAR][3])
    );
    build->IncThetaInterval = Interval<T>
    (
        static_cast<uint_fast32_t>(Config[CONFIG_INC_THETA][0]), 
        static_cast<T>(Config[CONFIG_INC_THETA][1]) * DEG_TO_RAD<T>, 
        static_cast<T>(Config[CONFIG_INC_THETA][2]) * DEG_TO_RAD<T>
    );
    build->IncPhiInterval = Interval<T>
    (
        static_cast<uint_fast32_t>(Config[CONFIG_INC_PHI][0]), 
        static_cast<T>(Config[CONFIG_INC_PHI][1]) * DEG_TO_RAD<T>, 
        static_cast<T>(Config[CONFIG_INC_PHI][2]) * DEG_TO_RAD<T>
    );
    build->RefThetaInterval = Interval<T>
    (
        static_cast<uint_fast32_t>(Config[CONFIG_REF_THETA][0]), 
        static_cast<T>(Config[CONFIG_REF_THETA][1]) * DEG_TO_RAD<T>, 
        static_cast<T>(Config[CONFIG_REF_THETA][2]) * DEG_TO_RAD<T>
    );
    build->RefPhiInterval = Interval<T>
    (
        static_cast<uint_fast32_t>(Config[CONFIG_REF_PHI][0]), 
        static_cast<T>(Config[CONFIG_REF_PHI][1]) * DEG_TO_RAD<T>, 
        static_cast<T>(Config[CONFIG_REF_PHI][2]) * DEG_TO_RAD<T>
    );

    // Bakes the radar object
    shared_ptr<RadarObject<T, V>> rad_obj = BuildRadarObject<T, V>(Config);
    rad_obj->Bake(build, Config[CONFIG_THREADS]);

    // Exports the radar object
    RadarObject<T, V>::Export(Output, rad_obj);
}

/**
 * @brief The top-level function for the baking program
 * 
 * @param argc - The number of arguments given to the program.
 * @param argv - The array of strings of all the arguments
 * @return int - The exit code of the program. If the program exits gracefully, will return 0.
 */
int main(int argc, char* argv[])
{
    // Template parameters
    typedef float T;
    const uint_fast32_t V = 3;

    // Reads the arguments
    string config_file      = argv[1];
    string partition_out    = argv[2];
    string baked_out        = argv[3];

    // Gets the path to the configuration given in the first argument
    json config;
    ifstream(config_file) >> config;

    // Bakes the model
    // ExportMeshes<T, V>(config);

    // shared_ptr<RadarObject<T, V>> rad_obj = BuildRadarObject<T, V>(config);
    // ExportPartition<T, V>(rad_obj, partition_out);

    BakeObject<T, V>(config, baked_out);

    // Return zero to show the program exited gracefully
    return 0;
}
