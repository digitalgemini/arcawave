
#pragma once

// STL includes
#include <memory>
#include <filesystem>

// ArcaWave includes
#include "Geometry/Object.h"
#include "Geometry/RayTracing/RayTracer.h"
#include "Radar/Discretizer.h"
#include "Radar/ObjectSlicer.h"
#include "Radar/Calculator/CurrentCalculator.h"
#include "Utility/Interval.h"
#include "Utility/ThreadPool.h"
#include "Utility/SparseVector.hpp"

namespace ArcaWave::Radar
{
    enum VCMMode
    {
        VCM_MONOSTATIC  = 0,
        VCM_BISTATIC    = 1,
    };

    template<typename T, uint_fast32_t V>
    struct VCMBuildInfo
    {

    // Public members
    public:

        VCMMode Mode;

        T Frequency;

		Eigen::Matrix<std::complex<T>, 2, 1> Polarization;

		Utility::Interval<T> IncThetaInterval;

        Utility::Interval<T> IncPhiInterval;

        Utility::Interval<T> RefThetaInterval;

        Utility::Interval<T> RefPhiInterval;
    };

    template<typename T, uint_fast32_t V>
    struct VCMBakeInfo
    {

    // Public members
    public:

        std::shared_ptr<VCMBuildInfo<T, V>> Build;

        std::shared_ptr<Object<T, V>> Obj;

        std::shared_ptr<Geometry::RayTracer<T, V>> ObjTracer;

        std::shared_ptr<Geometry::RayTracer<T, V>> FullTracer;

        std::shared_ptr<Radar::Discretizer<T, V>> Disc;

        std::shared_ptr<Utility::ThreadPool> ThrdPool;

        std::shared_ptr<Radar::CurrentCalculator<T, V>> CurrentCalculator;
    };

    template<typename T, uint_fast32_t V>
    class VCM
    {

    // Private types
    private:

        struct BakeJobParameter : public Utility::JobParameter
        {
            std::shared_ptr<std::shared_ptr<DiscretizedMesh<T, V>>[]> DiscObj;
            std::shared_ptr<Geometry::Object<T, V>> Obj;
            std::shared_ptr<Geometry::RayTracer<T, V>> Tracer;
            std::shared_ptr<VCM<T, V>> VectorCurrentMoment;
            std::shared_ptr<CurrentCalculator<T, V>> CurrentCalculatorObject;
		};

	// Private Members
    public:

        const std::shared_ptr<VCMBuildInfo<T, V>> Build;

        std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> Moments;

        // std::shared_ptr<uint_fast32_t[]> RefIndicies;

	// Public constructor and destructor
    public:

        VCM(const std::shared_ptr<VCMBuildInfo<T, V>>& Build);

        ~VCM();

    // Public methods
    public:

        /**
         * @brief
         * @param Reflection
         * @return Eigen::Matrix<complex<T>, V, 1>
         */
		Eigen::Matrix<std::complex<T>, V, 1> GetMoment(const Eigen::Matrix<T, 2, 1>& Incidence, const Eigen::Matrix<T, 2, 1>& Reflection);

		/**
		 * @brief
		 * @param Reflection
		 * @param Incidence
		 * @param MomentValue
		 */
		void SetMoment(const Eigen::Matrix<T, 2, 1>& Incidence, const Eigen::Matrix<T, 2, 1>& Reflection, const Eigen::Matrix<std::complex<T>, V, 1>& MomentValue);

        // uint_fast32_t GetStrongestReflection(const Eigen::Matrix<T, 2, 1>& Incidence);

	// Public static methods
    public: 

    	/**
    	 * @brief
    	 * @param BakeInfo
    	 * @return
    	 */
        static std::shared_ptr<VCM<T, V>> Bake(const std::shared_ptr<VCMBakeInfo<T, V>>& BakeInfo);

        // /**
        //  * @brief
        //  * @param Parameters
        //  * @param Data
        //  * @return shared_ptr<VectorCurrentMoment<T, V>>
        //  */
        // static std::shared_ptr<VCM<T, V>> Import(const std::filesystem::path& Parameters, const std::filesystem::path& Data);

        // /**
        //  * @brief 
        //  * 
        //  * @param Parameters 
        //  * @param Data 
        //  * @param VectorCurrentMoment 
        //  */
		// static void Export(const std::filesystem::path& Parameters, const std::filesystem::path& Data, std::shared_ptr<VCM<T, V>> VectorCurrentMoment);


    // Private member functions
    private:
    
    	uint_fast32_t GetIndex(const Eigen::Matrix<T, 2, 1>& Incidence, const Eigen::Matrix<T, 2, 1>& Reflection);

    // Private static methods
    private:

        static void BakeJobFunction(uint_fast32_t ThreadID, uint_fast32_t ThreadCount, const std::shared_ptr<Utility::JobParameter>& Parameter);
    };
}
