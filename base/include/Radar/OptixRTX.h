#pragma once
// STL includes
#include <vector>
#include <string> 
#include <array>
#ifdef USE_CUDA
#include <support/glad/glad.h>

// CUDA optix includes
#include <optix.h>
#include <optix_stubs.h>
#include <cuda_runtime.h>
#include <sampleConfig.h>
#include <cuda/whitted.h>
#include <cuda/Light.h>
#include <cuda_gl_interop.h>

// Sutil includes
#include <sutil/Camera.h>
#include <sutil/Trackball.h>
#include <sutil/CUDAOutputBuffer.h>
#include <sutil/Exception.h>
#include <sutil/GLDisplay.h>
#include <sutil/Matrix.h>
#include <sutil/Scene.h>
#include <sutil/sutil.h>
#include <sutil/vec_math.h>



#include <support/GLFW/include/GLFW/glfw3.h>
#undef GetObject

enum { TX_RAY_TYPE=0, RX_RAY_TYPE, RAY_TYPE_COUNT };

// Arcawave include 
#include "Geometry/Object.h"
#include "Geometry/RayTracing/RayTracer.h"

// test using custom mesh structure for now
struct TriangleMesh {
    std::vector<Eigen::Vector3f > vertex;
    std::vector<Eigen::Vector3f> normal;
    std::vector<Eigen::Vector3i> index;
};

struct instance_t
{
    float transform[12];
};

struct Model {
    ~Model()
    {
        for (auto mesh : meshes) delete mesh;
    }

    std::vector<TriangleMesh*> meshes;
    std::vector<instance_t> InstanceMatrix;
};

struct light_t
{
    float3  pos;
    float3  dir;
};



struct payload_t
{
    uint32_t index;
    float meshID;
    float3 propagation;
    float3 contact;
};

template<typename T, uint_fast32_t V>
struct illuminateParams
{
   ArcaWave::Geometry::ProjectedRay<T, V>* prd;
   uint32_t               	imageWidth;
   uint32_t               	imageHeight;
   OptixTraversableHandle 	handle;
   //@ TODO CHANGE TO ARRAY FOR MULTIPLE TX
   float3 					tx_location;
};

struct displayParams
{
    uint32_t     subframeIndex;
    float4* accumBuffer;
    uchar4* frameBuffer;
    uint32_t               imageWidth;
    uint32_t               imageHeight;

    light_t   light;

    OptixTraversableHandle  handle;
};


struct RayGenData
{
    float3 cam_eye;
    float3 camera_u, camera_v, camera_w;
};

struct MissData
{
    float r, g, b;
};

struct HitGroupData
{
    int  meshID;
    float3* vertex;
    int3* index;
};


namespace ArcaWave
{
    /**
    * @brief Nvidia Optix RTX class for ray tracing
    *
    */
    template<typename T, uint_fast32_t V>
    class OptixRTX
    {


        //Public constructors/deconstructor
    public:

        /**
        * @brief default constructor
        */
        OptixRTX();


        /**
         * @brief Destroys the OptixRTX.
         */
        ~OptixRTX();

        // Public methods
    public:


        /**
        * @brief Initializes optix, creates corresponding pipeline and gas structure with the given mesh
        *
        * @param Model* - A mesh that is imported using function importOBJ
        * TODO: use mesh class
        */
        void InitPointIllumination(const Model* meshes);

        /**
         * @brief Launch optix and render motion
         * @param matrix - A list of transform matrix from environment
         */
        void OptixRender(const std::vector<Eigen::Matrix<T, 4, 4>> matrix);

        /**
         * @brief TODO
         */
        std::shared_ptr<Geometry::Mesh<T, V>> OptixPointIllumination(const std::shared_ptr<Geometry::Object<T, V>> Obj);
        
        Geometry::ProjectedRay<T, V>* OptixProjectRays(const Geometry::LaunchInfo<T, V>& Info,const std::vector<Eigen::Matrix<T, 4, 4>> matrix);

        /**
         * @brief Clean up
         */
        void Destroy();
        /**
        * @brief Import
        *
        * @param obj
        */
        Model* ImportModel(const std::shared_ptr<Geometry::Object<T, V>> Obj,const  Geometry::Pose<T, V> pose);
/**
        * @brief Import
        *
        * @param obj
        */
        Model* ImportEnv(const std::shared_ptr<Geometry::Environment<T, V>> Env);

        // Private fields       
    private:

        /**
         * @brief Creates and configures a optix device context
         */
        void createContext();


        /**
         * @brief Builds acceleration structures, the traversable handle is used as the target for optixLaunch call
         * @param Model* - A mesh that is imported using function importOBJ
         */
        void buildAccel(const Model* model);


        void buildInstanceAccel(const Model* model);


        /**
          * @brief update instance
          * @param
          */
        void updateInstance(const std::vector<Eigen::Matrix<T, 4, 4>> TransformMatrix);

        /**
        * @brief Creates module
        */
        void createModule(char* FileName );

        /**
         * @brief Setup for raygen program
         */
        void createRaygenProgram();

        /**
         * @brief Setup for miss program
         */
        void createMissPrograms();

        /**
         * @brief Setup for hitgroup programs
         */
        void createHitgroupPrograms();

        /**
         * @brief Links program groups into optix pipeline
         */
        void createPipeline();

        /**
         * @brief Creates and setup shader binding table
         */
        void buildSBT();
        // Private fields      
    private:

        /**
         * @brief Module for device program
         */
        const Model* m;
        /**
         * @brief Cuda context  and stream that pipeline will run on
         */
        CUstream           stream;

        /**
         * @brief Pipeline
         */
        OptixPipeline               pipeline;
        OptixPipelineCompileOptions pipelineCompileOptions;
        OptixPipelineLinkOptions    pipelineLinkOptions;

        /**
         * @brief Module for device program
         */
        OptixModule                 module;
        OptixModuleCompileOptions   moduleCompileOptions;


        /**
         * @brief Device context and program groups
         */
        OptixDeviceContext Context;
		std::vector<OptixProgramGroup> raygenPGs;
		std::vector<OptixProgramGroup> hitgroupPGs;
		std::vector<OptixProgramGroup> missPGs;

		/**
         * @brief Shader binding table
         */

        OptixShaderBindingTable sbt;

        /**
         * @brief Geometry acceleration structure handle and buffers
         */
        OptixTraversableHandle gasHandle;
        CUdeviceptr            d_gas_output_buffer;
        std::vector<CUdeviceptr> d_vertices;
        std::vector<CUdeviceptr> d_indices;

        CUdeviceptr    d_ias_output_buffer;
        OptixTraversableHandle iasHandle;
        CUdeviceptr d_instances;
        CUdeviceptr outputBuffer;

        displayParams* d_params;
        CUdeviceptr  raygenRecords;

        /**
         * @brief Optix launch parameters
         */
        int LaunchWidth = 10000;
        int LaunchHeight = 10000;
    };
}
#endif