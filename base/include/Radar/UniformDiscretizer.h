
#pragma once

// ArcaWave includes
#include "Radar/Discretizer.h"

namespace ArcaWave
{
    namespace Radar
    {
        template<typename T, uint_fast32_t V>
        class UniformDiscretizer : public Discretizer<T, V>
        {

        // Public cosntructor and deconstructor
        public:

            UniformDiscretizer();

            virtual ~UniformDiscretizer();

        // Public methods
        public:

            virtual std::shared_ptr<DiscretizedFace<T, V>> DiscretizeFace(const std::shared_ptr<DiscretizeFaceInfo<T, V>>& Info) const;

            virtual std::shared_ptr<DiscretizedMesh<T, V>> DiscretizeMesh(const std::shared_ptr<DiscretizeMeshInfo<T, V>>& Info) const;

            // virtual std::shared_ptr<DiscretizedObject<T, V>> DiscretizeObject(const std::shared_ptr<DiscretizeObjectInfo<T, V>>& Info) const;
        };
    }
}
