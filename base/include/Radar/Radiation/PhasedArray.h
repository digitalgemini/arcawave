#pragma once

#include <memory>
#include <complex>
#include "Radar/Radiation/RadiationField.h"
#include "Radar/Radiation/PlaneWave.h"



using namespace ArcaWave::Radar;
using namespace Eigen;

namespace ArcaWave::Radar
{
    template<typename T, uint_fast32_t V>
    class PhasedArray
    {
        private:

        std::shared_ptr<std::vector<std::shared_ptr<ArcaWave::Radar::RadiationField<T, V>>>> Transmitters;

        Eigen::Matrix<T, 2, 1> BeamDirection;      //(phi, theta);

        T Phase;            //phase between each row, hard coded to PI/2 to eliminate back lobe, could change later
        
        std::uint_fast32_t Row;         // row of tx plane, along x axis

        std::uint_fast32_t Column;      // column of tx plane, along y axis

        public: 

        PhasedArray(Eigen::Matrix<T, 2, 1> BeamDirection, 
                    std::shared_ptr<std::vector<std::shared_ptr<ArcaWave::Radar::RadiationField<T, V>>>> Transmitters, 
                    T Phase, 
                    std::uint_fast32_t Row, 
                    std::uint_fast32_t Column
                    );

        PhasedArray(const PhasedArray<T, V>& other);

        ~PhasedArray()
        {};

        public:

        PhasedArray<T, V>& operator=(const PhasedArray<T, V>& Other);

        public:

        Eigen::Matrix<std::complex<T>, V, 1> ElectricField(T Time, const Eigen::Matrix<T, V, 1>& Point);

        void resetBeamDirection(Eigen::Matrix<T, 2, 1> beam);

        private:
        
        Eigen::Matrix<std::complex<T>, V, 1> CurrentIn(T Time, const Eigen::Matrix<T, V, 1>& Point, std::uint_fast32_t Column_i);


    };
}