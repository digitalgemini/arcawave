#pragma once

// ArcaWave includes
#include "CurrentCalculator.h"

namespace ArcaWave::Radar
{

    /**
     * @brief Physical Optics, material is PEC.
     *
     * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
     * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
     */
    template<typename T, uint_fast32_t V>
    class PO : public CurrentCalculator<T, V>
    {
        /**
         * @brief Calculate the scatter field contribution of this sub face.
         *
         * @param disc_face - The discretized face which contains many sub faces.
         * @param index - The index of this sub face.
         */
        void AddScatter(const std::shared_ptr<DiscretizedFace<T, V>>& disc_face, const uint_fast32_t index);
    };
}
