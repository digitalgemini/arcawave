#pragma once

// STL includes
#include <memory>
#include <complex>

// ArcaWave includes
#include "Radar/Radiation/PlaneWave.h"
#include "Geometry/Material.h"
#include "Radar/Discretizer.h"

namespace ArcaWave::Radar
{

    /**
     * @brief The interface to calculate VCM (or RCS) with different EM approximation methods. They all assume
     * incident wave is planewave at single frequency.
     *
     * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
     * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
     */
    template<typename T, uint_fast32_t V>
    class CurrentCalculator
    {
    // Protected fields
	protected:

        /**
         * @brief The single frequency to calculate on.
         */
		T Frequency;

        /**
         * @brief The incident unit vector.
         */
        Eigen::Matrix<T, V, 1> IncUnit;

        /**
         * @brief The reflected unit vector.
         */
        Eigen::Matrix<T, V, 1> RefUnit;

        /**
         * @brief The incident planewave.
         */
        Radar::PlaneWave<T, V> IncWave = Radar::PlaneWave<T, V>();

        /**
         * @brief The accumulated scatter field for this IncUnit and RefUnit.
         */
        Eigen::Matrix<std::complex<T>, V, 1> Scatter;
        
        /**
         * @brief The material of current mesh.
         */
        Geometry::Material<T> Material;


	// Public constructors
	public:

        /**
         * @brief Creates the CurrentCalculator.
         */
        CurrentCalculator();

        /**
         * @brief Destroys the CurrentCalculator.
         */
        virtual ~CurrentCalculator();

    // Public methods
    public:

        /**
         * @brief Record the incident direction and generate the incident planewave.
         *
         * @param IncUnit - The incident unit vector.
         * @param Frequency - The frequency to operate on.
         * @param Polarization - The polarization of planewave.
         */
        virtual void SetIncident(const Eigen::Matrix<T, V, 1>& IncUnit, T Frequency, const Eigen::Matrix<std::complex<T>, 2, 1>& Polarization);

        /**
         * @brief Record the reflected direction.
         *
         * @param RefUnit - The reflected unit vector.
         */
        virtual void SetReflected(const Eigen::Matrix<T, V, 1>& RefUnit);

        /**
         * @brief Record the material of this mesh for following calculation.
         *
         * @param Material - The material of this mesh.
         */
        virtual void SetMaterial(const Geometry::Material<T>& Material);

        /**
         * @brief Calculate the scatter field contribution of this sub face.
         *
         * @param disc_face - The discretized face which contains many sub faces.
         * @param index - The index of this sub face.
         */
        virtual void AddScatter(const std::shared_ptr<DiscretizedFace<T, V>>& disc_face, const uint_fast32_t index);

        /**
         * @brief Get the accumulated scatter field.
         *
         * @return Eigen::Matrix<std::complex<T>, V, 1> - The scatter field for this IncUnit and RefUnit.
         */
        virtual Eigen::Matrix<std::complex<T>, V, 1> GetScatter() const;
    };
}
