#pragma once

// ArcaWave includes
#include "Radar/DSP/SignalProcessing.h"

namespace ArcaWave::Radar
{
    template<typename T, uint_fast32_t V>
    class PhaseDSP : public SignalProcessing<T, V>
    {
    protected:
        SignalProcessingInfo<T, V> Info;

        std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> Data;

        Eigen::Matrix<T, V, 1> ElectricFieldDirection;

        std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> RangeData;

        std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> AngleData;

        std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> VelocityData;

    // Public constructors and deconstructor
    public:
        PhaseDSP(const SignalProcessingInfo<T, V>& Info, const std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> Data);

        virtual ~PhaseDSP();

    // Public methods
    public:
        virtual void RangeEstimation();

        virtual void AngleEstimation();

        virtual void VelocityEstimation();
    };
}
