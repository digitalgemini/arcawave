#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#elif __GNUG__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#endif
#include <unsupported/Eigen/FFT>
#ifdef __clang__
#pragma clang diagnostic pop
#elif __GNUG__
#pragma GCC diagnostic pop
#endif

namespace ArcaWave::Radar
{
	template <typename T>
	class FFTEngine
	{
	 public:
		std::vector<std::complex<T>> forward_fft(std::vector<T> time_domain);
	};
}