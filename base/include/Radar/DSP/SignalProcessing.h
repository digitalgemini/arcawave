#pragma once

// STL includes
#include <memory>
#include <vector>

// ArcaWave includes
#include "Radar/Radiation/RadiationField.h"
#include "Utility/Constants.h"

namespace ArcaWave::Radar
{
    template<typename T, uint_fast32_t V>
    struct SignalProcessingInfo
    {
        uint_fast32_t tx_width;
        uint_fast32_t tx_height;
        uint_fast32_t rx_width;
        uint_fast32_t rx_height;
        uint_fast32_t chirp_count;
        uint_fast32_t frame_count;
        T SampleRate;
        uint_fast32_t SampleCount;
        Eigen::Matrix<T, V, 1> Propogation;
        Eigen::Matrix<T, 2, 1> Polarization;
        std::shared_ptr<RadiationField<T, V>> RadField;
    };

    template<typename T, uint_fast32_t V>
    class SignalProcessing
    {
    // Protected fields
	 protected:
        SignalProcessingInfo<T, V> Info;

        std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> Data;

        Eigen::Matrix<T, V, 1> ElectricFieldDirection;

        std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> RangeData;

        std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> AngleData;

        std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> VelocityData;

    // Public constructors and deconstructor
    public:
        SignalProcessing()
        {}

        SignalProcessing(const SignalProcessingInfo<T, V>& Info, const std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> Data) : 
            Info(Info), Data(Data)
        {
            Eigen::Matrix<T, V, 1> PlaneA, PlaneB;
            if(fabs(Info.Propogation.dot(Eigen::Matrix<T, V, 1>::UnitX())) - static_cast<T>(1.0) < -Utility::FLOATING_POINT_ERROR<T>)
                PlaneA = Info.Propogation.cross(Eigen::Matrix<T, V, 1>::UnitX()).normalized();
            else
                PlaneA = Info.Propogation.cross(Eigen::Matrix<T, V, 1>::UnitY()).normalized();
            PlaneB = Info.Propogation.cross(PlaneA);
            ElectricFieldDirection = Info.Polarization.x() * PlaneA + Info.Polarization.y() * PlaneB;
        }

        virtual ~SignalProcessing()
        {}

    // Public methods
    public:
        virtual void RangeEstimation() = 0;

        virtual void AngleEstimation() = 0;

        virtual void VelocityEstimation() = 0;

        virtual std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> GetRange() const
        {
            return RangeData;
        }

        virtual std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> GetAngle() const
        {
            return AngleData;
        }

        virtual std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> GetVelocity() const
        {
            return VelocityData;
        }
    };
}
