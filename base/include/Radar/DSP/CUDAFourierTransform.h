#pragma once

#include <iostream>
#include <fstream>
#include <cufft.h>
#include <memory>


#include <cuda/api_wrappers.hpp>
#include <thrust/complex.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/extrema.h>
#include <thrust/transform.h>
#include <thrust/functional.h>

#include "Radar/RadarPipeline.h"
#include "Radar/DSP/FFTEngine.h"

// Npy export library
// #include "cnpy.h"


namespace ArcaWave::Radar
{
    /**
    * @brief Thrust functor for thresholding
    * 
    */
    template<typename T, uint_fast32_t V>
    struct threshold_f
    {
        T threshold;
        threshold_f(T t){threshold = t;}
        __host__ __device__
        T operator()(const T& z)
        {
            if(z>threshold) return z;
            else return 0;
        }
    };

    /**
    * @brief Thrust functor for computing magnitude of complex number
    * 
    */
    template<typename T, uint_fast32_t V>
    struct complex_abs
    {
        __host__ __device__
        T operator()(const thrust::complex<T>& z)
        {
            return thrust::abs(z);
        }
    };

    template <typename T, uint_fast32_t V>
    class CUDAFourierTransform 
    {
        

        public:
            CUDAFourierTransform();

            /**
             * @brief Construct a new CUDA FMCW fft object
             * 
             * @param Mixed 
             * @param info 
             */
            CUDAFourierTransform(
                std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> Mixed, 
                Radar::DSPprocess<T,V>& info);

            ~CUDAFourierTransform()
            {}
            
            /**
             * @brief Save FFT result to a numpy file
             * the result should be transposed using np.transpose(result,axes=(2,0,1)) in order to match DSP Notebook format
             * 
             * @param fname 
             */
            void SaveResult(std::string fname);

            /**
             * @brief Save thresholded FFT result to a numpy file
             * the result should be transposed using np.transpose(result,axes=(2,0,1)) in order to match DSP Notebook format
             * 
             * @param fname 
             */
            void SaveThresholdedResult(std::string fname);

        private:

            std::vector<std::complex<T>> FFTOutput; // FFT output

            std::vector<T> ThresholdedOutput; // Thresholded result 

            unsigned long num_samples;  // Number of samples

        private:
            /**
             * @brief FMCW fft processing with input device_vector of size (info.num_samples*info.r_cols*info.r_rows )
             * 
             * @param info 
             * @param d_rawdata 
             */
            void Processing(Radar::DSPprocess<T,V>& info,thrust::device_vector<std::complex<T>>& d_rawdata);
            
            /**
             * @brief Batched 1d fft using cufft library on the first axis of input device_vector
             * 
             * @param fftsize 
             * @param batches 
             * @param input 
             * @param output 
             */
            void CufftProcessing(int fftsize, int batches,thrust::device_vector<std::complex<T>>& input,thrust::device_vector<std::complex<T>>& output );
            
            /**
             * @brief Given input complex device_vector, compute magnitude and perform thresholding
             * 
             * @param size 
             * @param input 
             */
            void ThresholdProcessing(int size, thrust::device_vector<std::complex<T>>& input);

    };
}


