#pragma once

// #include <stdlib.h>
#include <iostream>
#include <fstream>
#include "Radar/DSP/FFTEngine.h"

namespace ArcaWave::Radar
{

    template <typename T, uint_fast32_t V>
    class FourierTransform : public FFTEngine<T, V>
    {

        public:

            FourierTransform(
                std::vector<Eigen::Matrix<std::complex<T>, V, 1>>& Mixed, 
                Radar::DSPprocess<T,V>& info);

            FourierTransform(FourierTransform<T, V>& other);

            ~FourierTransform()
            {}

        private:

            std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::complex<T>>>>>>> RawData;

            std::shared_ptr<std::vector<std::complex<T>>> Range;

            void processing(DSPprocess<T,V>& info);



            /**
             * @brief Help slice or pad the vector to 128. Helper function
             * 
             * @tparam T 
             * @tparam V 
             * @param vec 
             * @return shared_ptr<vector<complex<T>>> 
             */
            std::shared_ptr<std::vector<std::complex<T>>> slice_vector(std::shared_ptr<std::vector<std::complex<T>>> vec);

            /**
             * @brief Swap the data on given axes. Refer to SwapAxis in python
             * 
             * @tparam T 
             * @tparam V 
             * @param Axis1 
             * @param Axis2 
             * @return shared_ptr<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>> 
             */
            std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::complex<T>>>>>>> SwapAxes(uint_fast32_t Axis1, uint_fast32_t Axis2);

        public:


            /**
            * @brief Do a 1-D fft on the given data. For range of a single chirp use
            * 
            * @tparam T 
            * @tparam V 
            * @param time_domain 
            * @return shared_ptr<vector<complex<T>>> 
            */
            std::shared_ptr<std::vector<std::complex<T>>> forward_fft_single(std::shared_ptr<std::vector<std::complex<T>>> time_domain);



            /**
            * @brief Do FFT on the last Axis
            * 
            * @tparam T 
            * @tparam V 
            * @return shared_ptr<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>> 
            */
            std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::complex<T>>>>>>> forward_fft();
            
            /**
             * @brief Shifting the row and column dimensions. The later half come to the first. Refer to numpy.fftshift
             * 
             * @tparam T 
             * @tparam V 
             * @return shared_ptr<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>> 
             */
		    std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::complex<T>>>>>>> fft_shift();
    
            std::shared_ptr<std::vector<std::complex<T>>> getRange();

            std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::complex<T>>>>>>> getRawData();

    };
}


