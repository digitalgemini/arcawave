#pragma once


#include <memory>
#include <complex>
#include "Utility/Constants.h"
#include "Radar/Radiation/RadiationField.h"
#include "Radar/Radiation/PhasedArray.h"
#include "Radar/Radiation/PatchAntenna.h"


using namespace ArcaWave::Radar;
using namespace Eigen;


namespace ArcaWave::Radar
{
    template<typename T, uint_fast32_t V>
    class RadarClass
    {
        protected:      // Can be accessed by derived class

            uint_fast32_t Row_t;      // Number of tx in a row --- along x-axis

            uint_fast32_t Column_t;    // Number ber of tx in a column -- along y-axis

            uint_fast32_t Row_r;        // Number of rx in a row  -- long y-axis

            uint_fast32_t Column_r;     // Number of rx in a column -- along z-axis

            uint_fast32_t type_info;    // #0 represents PlaneWave (also the default), #1 represents PatchedAntenna

            Eigen::Matrix<T, 2, 1> BeamDirection;      // (phi, theta), with a default value of (PI/2, 0)

            Eigen::Matrix<T, V, 1> Propagation;         // Propagation direction

            Eigen::Matrix<std::complex<T>, 2, 1> Polarization;     

            T Init_Freq;                // Default is 5 GHz

            T Freq_slope;               // Default is 0 GHz

            T Max_Freq;                 // Default is 5 GHz

        private:

            /**
             * @brief Transmitters positions
             * 
             */
            std::shared_ptr<std::vector<Eigen::Matrix<T, V, 1>>> Transmitters;

            /**
             * @brief Receivers positions
             * 
             */
            std::shared_ptr<std::vector<Eigen::Matrix<T, V, 1>>> Receivers;

            /**
             * @brief Radiation field informations
             * 
             */
            std::shared_ptr<std::vector<std::shared_ptr<RadiationField<T, V>>>> Rad_Field;

        public:
            
            /**
             * @brief Construct a new Radar Class object
             * with everything by default
             * mono static radar at position (-10.0, 0.0, 0.0)
             * planewave 
             * (1,0,0) propagation
             * (1.0, 0.0, 0.0) polarization
             * 5e9 inital frequency
             * 0 frequency slope
             * 5e9 maximum frequency
             * 
             */
            RadarClass(); 

            /**
             * @brief Construct a new Radar Class object
             * with defualt rx location (around x-axis at (-10.0, 0.0, 0.0))
             *      defualt tx location (around y-axis at (-10.0, 0.0, 0.0))
             * 
             * @param Row_t 
             * @param Column_t 
             * @param Row_r 
             * @param Column_r 
             * @param type_info 
             */
            RadarClass(uint_fast32_t Row_t, uint_fast32_t Column_t, uint_fast32_t Row_r, uint_fast32_t Column_r, 
                       uint_fast32_t type_info);  

            /**
             * @brief Construct a new Radar Class object, 
             * without any specified rx and tx location.
             * 
             * @param Row_t             Number of tx along x-axis
             * @param Column_t          Number of tx along y-axis
             * @param Row_r             Number of rx along z-axis
             * @param Column_r          Number of rx along y-axis
             * @param type_info         The type of radiation wave wanted for the radar
             * 0 -- PlaneWave, 1 -- PatchAntenna
             * @param Propagation       Propagation direction
             * @param Polarization      Polarization way
             * @param Init_Freq         
             * @param Freq_slope 
             * @param Max_Freq 
             */
            RadarClass(uint_fast32_t Row_t, uint_fast32_t Column_t, 
                       uint_fast32_t Row_r, uint_fast32_t Column_r,
                       uint_fast32_t type_info,
                       Eigen::Matrix<T, V, 1> Propagation, 
                       Eigen::Matrix<std::complex<T>, 2, 1> Polarization,
                       T Init_Freq,
                       T Freq_slope,
                       T Max_Freq);

            /**
             * @brief Construct a new Radar Class object 
             * with fully specified rx and tx position
             * 
             * @param Row_t 
             * @param Column_t 
             * @param tx_arr 
             * @param Row_r 
             * @param Column_r 
             * @param rx_arr 
             * @param type_info 
             * @param Propagation 
             * @param Polarization 
             * @param Init_Freq 
             * @param Freq_slope 
             * @param Max_Freq 
             */
            RadarClass(uint_fast32_t Row_t, uint_fast32_t Column_t, std::shared_ptr<std::vector<Eigen::Matrix<T, V, 1>>> tx_arr,
                       uint_fast32_t Row_r, uint_fast32_t Column_r, std::shared_ptr<std::vector<Eigen::Matrix<T, V, 1>>> rx_arr,
                       uint_fast32_t type_info,
                       Eigen::Matrix<T, V, 1> Propagation, 
                       Eigen::Matrix<std::complex<T>, 2, 1> Polarization,
                       T Init_Freq,
                       T Freq_slope,
                       T Max_Freq);


        public:

            /**
             * @brief Returns the array of TX positions
             * 
             * @return shared_ptr<vector<Matrix<T, V,1>>> 
             */
            std::shared_ptr<std::vector<Eigen::Matrix<T, V,1>>> getTransmitters();

            /**
             * @brief Returns the array of RX positions
             * 
             * @return shared_ptr<vector<Matrix<T, V,1>>> 
             */
            std::shared_ptr<std::vector<Eigen::Matrix<T, V,1>>> getReceivers();

            /**
             * @brief Returns the array of radiation field objects
             * 
             * @return shared_ptr<vector<shared_ptr<RadiationField<T, V>>>> 
             */
            std::shared_ptr<std::vector<std::shared_ptr<RadiationField<T, V>>>> getRadiators();

            /**
             * @brief Initialize the phased array with the speficied beam direction
             * 
             * @param BeamDirection     The beam direction
             * @return shared_ptr<PhasedArray<T, V>> 
             */
            std::shared_ptr<PhasedArray<T, V>> setPhasedArray(Eigen::Matrix<T, 2,1> BeamDirection);
            
    };
}