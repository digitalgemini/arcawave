#pragma once

// STD includes
#include <complex>
#include <random>

// Eigen includes (ignoring warnings)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#endif
#include <Eigen/Geometry>
#ifdef __clang__
#pragma clang diagnostic pop
#endif

namespace ArcaWave::Radar
{
	/**
	 * @brief Base class for defining sources of noise in a pipeline
	 * @tparam T Data type for output of noise
	 * @tparam V Size of vector for output of noise
	 */
	template<typename T, uint_fast32_t V>
	class NoiseSource
	{
		// Public constructor and destructor
	 public:
		/**
		 * @brief Constructor
		 * @param seed for Random Number Generation
		 * @param averagePower The RMS power of noise
		 */
		NoiseSource(uint_fast32_t seed, T averagePower);

		~NoiseSource();

		//protected Fields
	 protected:
		/**
		 * @brief Engine for RNG
		 */
		std::mt19937 Generator;

		/**
		 * @brief RMS Power of Noise
		 */
		T AveragePower;

		// Public Methods
	 public:
		/**
		 * @brief Interface for accessing generated noise
		 * @return Generated noise signal, 1 sample per call.
		 */
		virtual Eigen::Matrix<T, V, 1> Noise(uint_fast32_t index = 0) = 0;

		T GetAveragePower() const;

		virtual void SetAveragePower(T averagePower);
	};
}
