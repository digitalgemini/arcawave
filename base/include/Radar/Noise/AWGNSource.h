#pragma once

//STL Includes
#include <random>

// ArcaWave includes
#include "Radar/Noise/NoiseSource.h"

namespace ArcaWave::Radar
{
	/**
	 * @brief A class for generating White Gaussian Noise with a specified RMS power.
	 * @tparam T DataType for representing the noise signal
	 * @tparam V Size of signal vector
	 */
	template<typename T, uint_fast32_t V>
	class AWGNSource : public NoiseSource<T, V>
	{

		// Private fields
	 private:
		/**
		 * @brief Distribution, intialized with mean as zero and variance as RMS Power
		 */
		std::normal_distribution<T> GaussianDistribution;

		// Public constructor and destructor
	 public:
		/**
		 * @brief Constructor
		 * @param seed Seed For RNG
		 * @param averagePower RMS power of noise required
		 */
		AWGNSource(uint_fast32_t seed, T averagePower);

		/**
		 * @brief Default destructor
		 */
		~AWGNSource();

		// Public methods
	 public:
		/**
		 * @brief Interface for accessing the generated noise signal
		 * @param index UNUSED FOR AWGN, DO NOT PASS; WILL USE DEFAULT
		 * @return Noise signal, one sample at a time
		 */
		virtual Eigen::Matrix<T, V, 1> Noise(uint_fast32_t index = 0) override;

		/**
		 * @brief Change the average power of generated noise
		 * @param averagePower
		 */
		void SetAveragePower(T averagePower) override;
	};
}