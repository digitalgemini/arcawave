#pragma once

// ArcaWave Includes
#include "Utility/Interval.h"
#include "Utility/CudaUtilities.cuh"
// STL includes
#include <memory>
#include <iostream>

// LIB includes
#include <Eigen/Geometry>
#include <cuda/api_wrappers.hpp>
#include <thrust/complex.h>
#include <curand_kernel.h>

#ifdef __CUDACC__
#define CUDA_HOSTDEV __device__ __host__
#define CUDA_DEV __device__
#else
#define CUDA_HOSTDEV
#define CUDA_DEV __device__
#endif

using namespace ArcaWave::Utility;

namespace ArcaWave::Radar
{
    template<typename T, uint_fast32_t V>
    class StochasticVCM
    {
        // private data members
    public:
        /*
         * Pointer to device memory containing moments
         */
        T* CumulativeMoment;
        Eigen::Matrix<thrust::complex<T>, V, 1>* DeviceMoments;

        Utility::Interval<T> IncPhiInterval;
        Utility::Interval<T> IncThetaInterval;
        Utility::Interval<T> RefThetaInterval;
        Utility::Interval<T> RefPhiInterval;

        Utility::Interval<T>* DevIncPhiInterval;
        Utility::Interval<T>* DevIncThetaInterval;
        Utility::Interval<T>* DevRefThetaInterval;
        Utility::Interval<T>* DevRefPhiInterval;

        // public member functions
    public:
        CUDA_HOSTDEV uint_fast32_t GetIndex(const Eigen::Matrix<T, 2, 1>& Incidence, const Eigen::Matrix<T, 2, 1>& Reflection)
        {
            const auto ref_phi_dim = RefThetaInterval.Size;
            const auto inc_theta_dim = RefPhiInterval.Size * RefThetaInterval.Size;
            const auto inc_phi_dim =  IncThetaInterval.Size * RefPhiInterval.Size * RefThetaInterval.Size;

            return  RefThetaInterval.Find(Reflection.x())  + RefPhiInterval.Find(Reflection.y()) * ref_phi_dim + IncThetaInterval.Find(Incidence.x() * inc_theta_dim) + IncPhiInterval.Find(Incidence.y()) * inc_phi_dim;
        }

        CUDA_HOSTDEV Eigen::Matrix<thrust::complex<T>, V, 1> GetMoment(const Eigen::Matrix<T, 2, 1>& Incidence, const Eigen::Matrix<T, 2, 1>& Reflection)
        {
            return DeviceMoments[GetIndex(Incidence, Reflection)];
        }

        CUDA_HOSTDEV float3 ConvertToDir(const Eigen::Matrix<T, 2, 1>& Angle)
        {
            float x = sin(Angle.y()) * cos(Angle.x());
            float y = cos(Angle.y());
            float z = sin(Angle.y()) * sin(Angle.x());
            return make_float3(x, y, z);
        }

        CUDA_HOSTDEV Eigen::Matrix<T, 2, 1> ConvertToAngle(const float3& Dir)
        {
            T phi = acos(Dir.y);
            T theta = atan2(Dir.z, Dir.x);
            return Eigen::Matrix<T, 2, 1>(theta, phi);
        }

        CUDA_DEV float3 GenerateReflectionDir(const float3& Dir, curandState_t* State)
        {
            auto inc_angle = ConvertToAngle(Dir);

            const auto inc_theta_dim = DevRefPhiInterval->Size * DevRefThetaInterval->Size;
            const auto inc_phi_dim =  DevIncThetaInterval->Size * DevRefPhiInterval->Size * DevRefThetaInterval->Size;

            auto l_moment_index = DevIncThetaInterval->Find(inc_angle.x()) * inc_theta_dim + DevIncPhiInterval->Find(inc_angle.y()) * inc_phi_dim;
            auto r_moment_index = l_moment_index + inc_theta_dim;

            auto uni_sample = curand_uniform(State);

            int index_of_ref;

            for (int i = l_moment_index; i < r_moment_index; ++i)
            {
                if (CumulativeMoment[i] > uni_sample)
                {
                    index_of_ref = i - l_moment_index;
                    break;
                }
            }

            auto ref_theta = DevRefThetaInterval->Evaluate(index_of_ref % DevRefThetaInterval->Size);
            auto ref_phi = DevRefThetaInterval->Evaluate(index_of_ref / DevRefThetaInterval->Size);

            return ConvertToDir(Eigen::Matrix<T, 2,1>{ref_theta, ref_phi});
        }

        // @TODO change this to a build struct
        StochasticVCM(std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]>Moments, Interval<T> IncThetaInterval, Interval<T> IncPhiInterval, Interval<T> RefThetaInterval, Interval<T> RefPhiInterval):
        IncThetaInterval(IncThetaInterval), DevIncThetaInterval(ConstructOnDevice<Interval<T>>( cuda::device::get(0), IncThetaInterval)),
        IncPhiInterval(IncPhiInterval), DevIncPhiInterval(ConstructOnDevice<Interval<T>>( cuda::device::get(0), IncPhiInterval)),
        RefThetaInterval(RefThetaInterval), DevRefThetaInterval(ConstructOnDevice<Interval<T>>( cuda::device::get(0), RefThetaInterval)),
        RefPhiInterval(RefPhiInterval) , DevRefPhiInterval(ConstructOnDevice<Interval<T>>( cuda::device::get(0), RefPhiInterval))
        {
            //@TODO figure out global device management
            auto target_device = cuda::device::get(0);
            //std::cout << "LOADING VCM onto " << target_device.name() << std::endl;
            /*
            std::vector<T> host_norm;

            for (int inc_phi = 0; inc_phi < IncPhiInterval.Size; ++inc_phi)
                for (int inc_theta = 0; inc_theta < IncThetaInterval.Size; ++inc_theta)
                {
                    T partial_sum = 0;
                    Eigen::Matrix<T, 2, 1> inc = {IncThetaInterval.Evaluate(inc_theta), IncPhiInterval.Evaluate(inc_phi)};

                    for (int ref_phi = 0; ref_phi < RefPhiInterval.Size; ++ref_phi)
                        for (int ref_theta = 0; ref_theta < RefThetaInterval.Size; ++ ref_theta)
                        {
                            Eigen::Matrix<T, 2, 1> ref = {RefThetaInterval.Evaluate(ref_theta), RefPhiInterval.Evaluate(ref_phi)};
                            partial_sum += std::abs(Moments[GetIndex(inc, ref)].norm());
                            host_norm.push_back(partial_sum);
                        }

                    const auto inc_theta_dim = RefPhiInterval.Size * RefThetaInterval.Size;
                    const auto inc_phi_dim =  IncThetaInterval.Size * RefPhiInterval.Size * RefThetaInterval.Size;
                    for (int i = inc_phi * inc_phi_dim + inc_theta * inc_theta_dim; i < inc_phi * inc_phi_dim + inc_theta * inc_theta_dim + inc_theta_dim; ++i)
                        host_norm[i] = host_norm[i]/partial_sum;
                }
            */
            if (RefPhiInterval.Size == 1583) std::cout << "HO" << std::endl;
            DeviceMoments = (Eigen::Matrix<thrust::complex<T>, V, 1>*)target_device.memory().allocate(sizeof(Eigen::Matrix<thrust::complex<T>, V, 1>) * IncThetaInterval.Size * IncPhiInterval.Size * RefThetaInterval.Size * RefPhiInterval.Size);
            cuda::memory::copy(DeviceMoments, Moments.get(), sizeof(Eigen::Matrix<thrust::complex<T>, V, 1>) * IncThetaInterval.Size * IncPhiInterval.Size * RefThetaInterval.Size * RefPhiInterval.Size);
        }

        StochasticVCM() {}
    };
}