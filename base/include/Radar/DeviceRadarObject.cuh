#pragma once
#include "Geometry/Partition.h"
#include "Radar/StochasticVCM.cuh"

namespace ArcaWave::Radar
{
    template<typename T, uint_fast32_t V>
    class DeviceRadarObject
    {

    // Public fields
    public:
        uint_fast32_t VCMCount;
        uint_fast32_t PartitionCount;
        ArcaWave::Geometry::Partition<T, V>* DevicePartitions;
        StochasticVCM<T, V>* DeviceVCMSet;
        uint_fast32_t* DeviceBoundaryIndex;

    //Constructor and Destructor
    public:
        DeviceRadarObject() = default;

        DeviceRadarObject(uint_fast32_t* BoundaryIndex, ArcaWave::Geometry::Partition<T, V>* Partitions, StochasticVCM<T, V>* VCMSet, uint_fast32_t VCMCount, uint_fast32_t PartitionCount)
        : VCMCount(VCMCount), PartitionCount(PartitionCount){
            //@TODO figure out global device management
            auto target_device = cuda::device::get(0);

            DevicePartitions = (ArcaWave::Geometry::Partition<T, V>*)target_device.memory().allocate(sizeof(ArcaWave::Geometry::Partition<T, V>) * PartitionCount);
            cuda::memory::copy(DevicePartitions, Partitions, sizeof(ArcaWave::Geometry::Partition<T, V>) * PartitionCount);

            DeviceVCMSet = (StochasticVCM<T, V>*)target_device.memory().allocate(sizeof(StochasticVCM<T, V>) * VCMCount);
            cuda::memory::copy(DeviceVCMSet, VCMSet, sizeof(StochasticVCM<T, V>) * VCMCount);

            DeviceBoundaryIndex = (uint_fast32_t*)target_device.memory().allocate(sizeof(uint_fast32_t) * PartitionCount);
            cuda::memory::copy(DeviceBoundaryIndex, BoundaryIndex, sizeof(uint_fast32_t) * PartitionCount);
        }

        __device__ uint_fast32_t GetMeshIndex(uint_fast32_t PrimitiveIndex)
        {
            for (uint_fast32_t i = 0; i < PartitionCount; ++i)
            {
                if (DeviceBoundaryIndex[i] > PrimitiveIndex) return i;
            }
            return PartitionCount - 1;
        }
    };
}

