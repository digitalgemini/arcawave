
#pragma once

// STL includes
#include <cstdint>
#include <memory>

// ArcaWave includes
#include "Geometry/Object.h"

namespace ArcaWave
{
    namespace Radar
    {
        template<typename T, uint_fast32_t V>
        struct DiscretizeFaceInfo
        {
            T Resolution;
            bool Interpolate;
            Geometry::Triangle<T, V> Face;
        };

        template<typename T, uint_fast32_t V>
        struct DiscretizeMeshInfo
        {
            T Resolution;
            bool Interpolate;
            std::shared_ptr<Geometry::Mesh<T, V>> Msh;
            std::shared_ptr<Geometry::Submesh> Sub;
        };

        template<typename T, uint_fast32_t V>
        struct DiscretizeObjectInfo
        {
            T Resolution;
            bool Interpolate;
            std::shared_ptr<Geometry::Object<T, V>> Obj;
        };

        template<typename T, uint_fast32_t V>
        struct DiscretizedFace
        {
            uint_fast32_t VertexCount;
            std::vector<Eigen::Matrix<T, V, 1>> Vertices;
            std::vector<Eigen::Matrix<T, V, 1>> Normals;
            std::vector<float> Areas;
        };

        template<typename T, uint_fast32_t V>
        struct DiscretizedMesh
        {
            std::shared_ptr<Geometry::Mesh<T, V>> Msh;
            uint_fast32_t FaceCount;
            std::shared_ptr<std::shared_ptr<DiscretizedFace<T, V>>[]> Faces;
        };

        // template<typename T, uint_fast32_t V>
        // struct DiscretizedObject
        // {
        //     std::shared_ptr<Geometry::Object<T, V>> Obj;
        //     uint_fast32_t MeshCount;
        //     std::shared_ptr<std::shared_ptr<DiscretizedMesh<T, V>>[]> Meshes;
        // };

        template<typename T, uint_fast32_t V>
        class Discretizer
        {

        // Public constructor and deconstructor
        public:

            Discretizer()
            {}

            virtual ~Discretizer()
            {}

        // Public methods
        public:

            virtual std::shared_ptr<DiscretizedFace<T, V>> DiscretizeFace(const std::shared_ptr<DiscretizeFaceInfo<T, V>>& Info) const = 0;

            virtual std::shared_ptr<DiscretizedMesh<T, V>> DiscretizeMesh(const std::shared_ptr<DiscretizeMeshInfo<T, V>>& Info) const = 0;

            // virtual std::shared_ptr<DiscretizedObject<T, V>> DiscretizeObject(const std::shared_ptr<DiscretizeObjectInfo<T, V>>& Info) const = 0;
        };
    }
}