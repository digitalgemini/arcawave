#pragma once

#include "Radar/RadarPipeline.h"

// stl includes
#include <complex>
#include <iostream>

// lib includes
#include <cuda/api_wrappers.hpp>
#include <thrust/complex.h>

// ArcaWave includes
#include "Radar/StochasticVCM.cuh"
#include "Utility/CudaUtilities.cuh"
#include "Utility/Constants.h"
#include "Geometry/RayTracing/OptixRayTracer.cuh"
#include "Radar/Radiation/PMCW.h"

namespace ArcaWave
{
	namespace Radar
	{
		template <typename T, uint_fast32_t V>
		struct DeviceRadarPipelineInfo
		{
			T Time;
			T SampleRate;
			uint_fast32_t SampleCount;
			uint_fast32_t TransmitterCount;
			uint_fast32_t ReceiverCount;
			uint_fast32_t TXSampleCount;
			Eigen::Matrix<T, V, 1>* Transmitters;
			Eigen::Matrix<T, V, 1>* Receivers;
			thrust::complex<T>* TXSamples;
			ArcaWave::Radar::RadiationField<T, V>* RadField;
		};

		template<typename T, uint_fast32_t V>
		class CUDARadarPipeline : public RadarPipeline<T, V>
		{
		 private:
			DeviceRadarPipelineInfo<T, V> DeviceInfo;

		 public:
			CUDARadarPipeline(const RadarPipelineInfo<T, V>& info);

            virtual ~CUDARadarPipeline();

        protected:
			void ComputeRXSignalFarField(const std::shared_ptr<RadarFrame<T, V>>& Frame) override;
		};

	}
}