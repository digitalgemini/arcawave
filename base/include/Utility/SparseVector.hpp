#pragma once

// STL includes
#include <map>
#include <vector>
#include <fstream>
#include <filesystem>
#include <algorithm>
#include <mutex>

// ArcaWave includes

namespace ArcaWave::Utility
{
	template<typename T>
	class SparseVector
	{
		// Typedef for pair representing data index and data;
		typedef std::pair<uint_fast32_t, T> SparsePair;

		// Private members
	 public:
		/**
		 * @brief the actual container that stores the data, contigously in memory
		 */
		std::vector<SparsePair> container;

		/**
		 * @brief Value returned when an index does not exist in the container
		 */
		T DefaultValue;

		/**
		 * @brief Mutex for inserting into Sparse Vector
		 */
		 std::mutex InsertLock;

		// Private methods
	 private:
		/**
		 * @brief Use to compare a given index with the index inside the sparsepair
		 * @param Lhs The sparsepair of concern
		 * @param Rhs The value to be compared with
		 * @return True if the index in the sparsepair is less than the rhs index
		 */
		static bool CompLower(SparsePair const & Lhs, uint_fast32_t const Rhs)
		{
			return  Lhs.first < Rhs;
		};

		static bool CompPair(SparsePair const & Lhs, SparsePair const& Rhs)
		{
			return  Lhs.first < Rhs.first;
		};

		/**
		 * @brief This is returned by the [] operator of sparsevector. Used to deal with reads and writes differently
		 */
		class SparseProxy
		{
		 private:
			/**
			 * @brief Pointer to sparsevector the proxy was returned from
			 */
			SparseVector<T>* Vector;

			/**
			 * @brief The index that was looked up
			 */
			size_t Idx;

		 public:
			/**
			 * @brief Constructor for sparseproxy, used by the [] operator
			 * @param Vector Pointer to sparsevector proxy is being constructed for
			 * @param Idx Index proxy is being constructed for
			 */
			SparseProxy(SparseVector<T> *Vector, size_t Idx ) : Vector(Vector), Idx(Idx)
			{}

			/**
			 * @brief Assignment operator: This is overloaded because its callled when writes are made using [] operator
			 * on the sparese vector. It finds the position in the internal container at which index should be inserted and
			 * writes to it.
			 * @param Item value that is being written into sparsevector
			 * @return the return value should be discared;
			 */
			SparseProxy & operator = (const T & Item)
			{
				std::lock_guard<std::mutex> lock(Vector->InsertLock);
				Vector->container.push_back(SparsePair(Idx, Item));
				return *this;
			}

			/**
			 * @brief The casting operator is overladed because its called when reads are made using [] operator
			 * on the sparse vecotr. It finds the the position in the internal container at which index should be. If found
			 * it returns the value containted at the index, else returns the default value for the spares vector.
			 * @return
			 */
			operator T()
			{
				auto low = std::lower_bound (Vector->container.begin(), Vector->container.end(), Idx, SparseVector<T>::CompLower);
				if ((low != Vector->container.end()) && (low->first == Idx)) return low->second;
				else return Vector->DefaultValue;
			}
		};

		// Public methods
	 public:
		/**
		 * @brief Constructor for sparse vector
		 * @param DefaultValue value that should be returned when a given index is not present
		 */
		SparseVector(T DefaultValue) : DefaultValue(DefaultValue)
		{}

		/**
		 * @brief Dumps the contents of the container in a binary format to disk
		 * @param Path filepath for destination
		 */
		void SaveToFile(std::filesystem::path const& Path)
			{
			std::ofstream stream;
			stream.open(Path, std::ios::out | std::ios::binary);
			stream.write(reinterpret_cast<char*>(container.data()), container.size() * sizeof(SparsePair));
			stream.close();
		}

		/**
		 * @brief Reads and builds the container from a binary dump
		 * @param Path filepath for source
		 */
		void ReadFromFile(std::filesystem::path const& Path)
		{
			size_t count = std::filesystem::file_size(Path) / sizeof(SparsePair);
			container = std::vector<SparsePair>(count);
			std::ifstream stream;
			stream.open(Path, std::ios::in | std::ios::binary);
			stream.read(reinterpret_cast<char*>(container.data()), count * sizeof(SparsePair));
		}

		/**
		 * @brief Allows for outside access to contents of the sparsevector
		 * @param Idx Index to read/write from
		 * @return A sparseproxy that can be casted to T or assinged T. Note this is not the same as refrence returned
		 * by STL containers.
		 */
		SparseProxy operator [](size_t Idx)
		{
			return SparseVector::SparseProxy(this, Idx);
		}

		/**
		 * @brief The actual number of elements inserted into the sparsevector
		 * @return Count, that does not include "missing" elements.
		 */
		size_t Size()
		{
			return container.size();
		}

		void Sort()
		{
			std::lock_guard<std::mutex> lock(InsertLock);
			std::sort(container.begin(), container.end(), SparseVector<T>::CompPair);
		}
	};
}
