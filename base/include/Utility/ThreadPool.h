
#pragma once

// STL includes
#include <functional>
#include <memory>
#include <mutex>
#include <thread>

namespace ArcaWave::Utility
{
    struct JobParameter
    {};

    typedef std::function<void(uint_fast32_t, uint_fast32_t, const std::shared_ptr<JobParameter>&)> JobFunction;

    class ThreadPool
    {

    // Private fields
    private:

        const uint_fast32_t MaxThreadCount;

        const uint_fast32_t MaxJobCount;

        std::mutex JobMutex;

        uint_fast32_t CurrentJobs;

        std::unique_ptr<std::mutex[]> WorkerMutexes;

        std::unique_ptr<std::shared_ptr<std::thread>[]> WorkerThreads;

    // Public constructor and deconstructor
    public:

        ThreadPool(uint_fast32_t MaxThreadCount, uint_fast32_t MaxJobCount);

        virtual ~ThreadPool();

    // Public methods
    public:

        uint_fast32_t GetMaxThreads() const;

        uint_fast32_t GetMaxJobs() const;

        bool Dispatch(uint_fast32_t RequestedThreads, JobFunction Function, const std::shared_ptr<JobParameter>& Parameter);

    // Private static methods
    private:

        static void RunJob(ThreadPool* Pool, uint_fast32_t RequestedThreads, JobFunction Function, const std::shared_ptr<JobParameter>& Parameter);
    };
}