
#pragma once

// STL includes
#include <algorithm>

#ifdef __CUDACC__
#define CUDA_HOSTDEV __device__ __host__
#else
#define CUDA_HOSTDEV
#endif

namespace ArcaWave::Utility
{

    /**
     * @brief An interval over some range of numbers that is discretized into a set number of steps. Note that this interval is inclusive to the beginning, but not to the 
     * end of the interval. Only numerical types should be used to template this structure.
     * 
     * @tparam T - A numerical type which represents the type the interval is evaluate to.
     */
    template<typename T>
    struct Interval
    {
    
    // Public fields
    public:

        /**
         * @brief The number of steps that the interval respects, also reprsenting one above the maximum index accepted by the interval for evaluation.
         */
        uint_fast32_t Size;

        /**
         * @brief The difference in value between each step of the interval.
         */
        T Step;

        /**
         * @brief The initial offset from zero of the first value of the interval, which represents the value of the interval evaluated at index zero.
         */
        T Offset;

    // Public constructors and deconstructor
    public:

        /**
         * @brief Default constructor for an interval, initializing size, step and offset to 0.
         */
        CUDA_HOSTDEV
        Interval() :
            Size(0), Step(static_cast<T>(0)), Offset(static_cast<T>(0))
        {}

        /**
         * @brief Creates a new interval from the given values.
         * 
         * @param Size - The size of the interval, also represents the indicies accepted by the interval for evaluation.
         * @param Step - The difference in value between each step of the interval. Defaults to a value of one.
         * @param Offset - The offset from zero of the first value of the interval. Defaults to a value of zero.
         */
        CUDA_HOSTDEV
        Interval(uint_fast32_t Size, T Step = static_cast<T>(1), T Offset = static_cast<T>(0)) :
            Size(Size), Step(Step), Offset(Offset)
        {}

        /**
         * @brief Creates a new interval by copying the contents of the given interval.
         * 
         * @param Other - The interval to copy the values from.
         */
        CUDA_HOSTDEV
        Interval(const Interval& Other) :
            Size(Other.Size), Step(Other.Step), Offset(Other.Offset)
        {}

        /**
         * @brief Destroys the interval.
         */
        CUDA_HOSTDEV
        ~Interval()
        {}

    // Public methods
    public:

        /**
         * @brief Gets the first value in the interval, or in other words, the value of the interval evaluated at index zero.
         * 
         * @return T - The first value in the interval.
         */
        CUDA_HOSTDEV
        inline T First() const
        {
            return Evaluate(0);
        }

        /**
         * @brief Gets the last value in the interval, or in other words, the value of the interval evaluated at index size minus one.
         * 
         * @return T - The last value in the interval.
         */
        CUDA_HOSTDEV
        inline T Last() const
        {
            return Evaluate(Size - 1);
        }

        /**
         * @brief Evaluates the interval at the given index. If the index is greater than or equal to the size of the size of the interval, the interval is instead evaluted
         * at the last acceptable index, which is the size of the interval subtracting one.
         * 
         * @param Index - The index at which to evaluate the interval.
         * @return T - The value of the interval evaluated at the given index.
         */
        CUDA_HOSTDEV inline T Evaluate(uint_fast32_t Index) const
        {
            return Offset + (std::min<uint_fast32_t>(Index, Size - 1) * Step);
        }

        /**
         * @brief Finds the closest index of the interval that evaluates to the given value. Note this will respect the bounds of the interval, meaning if the given value is 
         * less than the offset, it will only return zero, and likewise if the function would return an index larger or equal to the size, it will instead return the largest 
         * index accepted by the interval being the size subtract one.
         * 
         * @param Value - The value to find the closest lower index for in the interval.
         * @return uint_fast32_t - The closest index that evaluates to the given value in the interval, respecting the bounds of the interval.
         */
        CUDA_HOSTDEV inline uint_fast32_t Find(T Value) const
        {
            if((Step > 0 && Value < Offset) || (Step < 0 && Value > Offset))
                return static_cast<uint_fast32_t>(0);
            return std::min<uint_fast32_t>(lround((Value - Offset) / Step), Size - 1);
        }

        /**
         * @brief Determines and returns whether the given value is considered within the interval or not.
         * 
         * @param Value - The value to check on whether it exists within the interval or not.
         * @return true - The value does exist within the interval, and thus has a proper, not necessarily unique, index associated with it.
         * @return false - The value does not exist within the interval.
         */
        CUDA_HOSTDEV inline bool In(T Value) const
        {
            int_fast32_t index = static_cast<int_fast32_t>((Value - Offset) / Step);
            return (index >= 0 && index < Size);
        }

    // Public operator overloads
    public:

        /**
         * @brief Evaluates the interval at the given index. If the index is greater than or equal to the size of the size of the interval, the interval is instead evaluted
         * at the last acceptable index, which is the size of the interval subtracting one.
         * 
         * @param Index - The index at which to evaluate the interval.
         * @return T - The value of the interval evaluated at the given index.
         */
        CUDA_HOSTDEV inline T operator[](uint_fast32_t Index) const
        {
            return this->Evaluate(Index);
        }

        /**
         * @brief Copies the values from the given interval and sets the current intervals values to the copied values.
         * 
         * @param Other - The other intervale to copy the values from.
         * @return Interval& - A reference to the current interval that was changed.
         */
        CUDA_HOSTDEV
        Interval& operator=(const Interval& Other)
        {
            this->Size = Other.Size;
            this->Step = Other.Step;
            this->Offset = Other.Offset;
            return *this;
        }
    };
}


