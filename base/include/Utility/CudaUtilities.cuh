#pragma once

// CUDA lib includes
#include <cuda/api_wrappers.hpp>
#include <thrust/complex.h>

namespace ArcaWave
{
	namespace Utility
	{
		namespace CUDA
		{
			/**
			 * @brief A CUDA kernel to allocate a object on the
			 * @tparam Class
			 * @tparam Arguments
			 * @param ObjectPointer
			 * @param Args
			 */
			template<typename Class, typename... Arguments>
			__global__ void ConstructOnDevice(Class** ObjectPointer, Arguments... Args)
			{
				*ObjectPointer = new Class(Args...);
			}

            template<typename Class>
			__global__ void DestructOnDevice(Class * ObjectPointer)
            {
                delete ObjectPointer;
            }

			template <typename T, uint_fast32_t V>
			__device__ void AtomicComplexVector(Eigen::Matrix<thrust::complex<T>, V, 1>* Location, Eigen::Matrix<thrust::complex<T>, V, 1> Value)
			{
				auto cast_pod_location = reinterpret_cast<T*>(Location);
				atomicAdd(&(cast_pod_location[0]), Value.x().real());
				atomicAdd(&(cast_pod_location[1]), Value.x().imag());
				atomicAdd(&(cast_pod_location[2]), Value.y().real());
				atomicAdd(&(cast_pod_location[3]), Value.y().imag());
				atomicAdd(&(cast_pod_location[4]), Value.z().real());
				atomicAdd(&(cast_pod_location[5]), Value.z().imag());
			}
		}

		template <typename Class, typename... Arguments>
		Class* ConstructOnDevice(cuda::device_t Device, Arguments... Args)
		{
			// Pointer to newly constructed object will be placed here by the GPU
			Class** ptr_to_ptr_object = reinterpret_cast<Class**>(Device.memory().allocate(sizeof(void*)));

			// Launch kernel to create object
			Device.launch(
				ArcaWave::Utility::CUDA::ConstructOnDevice<Class, Arguments...>,
				cuda::make_launch_config(1,1),
				ptr_to_ptr_object, Args...
				);

			// Copy back result, this is a pointer on the GPU to the newly constructed object
			Class* dev_ptr;
			cuda::memory::copy(&dev_ptr, ptr_to_ptr_object, sizeof(void*));

			// Free memory allocated to store result
			cuda::memory::device::free(ptr_to_ptr_object);

			return dev_ptr;
		}

        template <typename Class>
		void DestructOnDevice(cuda::device_t Device, Class* ObjectPointer)
        {
		    Device.launch(
		        ArcaWave::Utility::CUDA::DestructOnDevice<Class>,
                cuda::make_launch_config(1,1),
                ObjectPointer
            );
        }
	}
}