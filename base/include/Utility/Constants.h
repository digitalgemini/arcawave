
#pragma once

#include <complex>

namespace ArcaWave::Utility
{
    /**
     * @brief The acceptable error of a single or double precision, normalized floating-point operation. No units are associated with this constant.
     */
    template<typename T>
    constexpr T FLOATING_POINT_ERROR                                    = T(0.01);

    /**
     * @brief The acceptable error of a single precision, normalized floating-point operation. No units are associated with this constant.
     */
    template<> inline constexpr float FLOATING_POINT_ERROR<float>       = float(0.0001);

    /**
     * @brief The acceptable error of a double precision, normalized floating-point operation. No units are associated with this constant.
     */
    template<> inline constexpr double FLOATING_POINT_ERROR<double>     = double(0.0000001);

    /**
     * @brief The constant pi, otherwise known as the ratio between a circle's circumfrence and its diameter. No units are associated 
     * with this constant.
     */
    template<class T>
    constexpr T PI                                                      = T(3.1415926535897);

    /**
     * @brief The constant to multiply by when converting from degrees to radians.
     * 
     * @tparam T - The data type of the constant.
     */
    template<class T>
    constexpr T DEG_TO_RAD                                              = T(0.0174532925199);

    /**
     * @brief The constant to multiply by when converting from radians to degrees.
     * 
     * @tparam T - The data type of the constant.
     */
    template<class T>
    constexpr T RAD_TO_DEG                                              = T(57.295779513084);

    /**
     * @brief The square root of two. No units are associated with this constant.
     */
    template<class T>
    constexpr T SQRT_2                                                  = T(1.41421356237);

    /**
     * @brief The square root of three. No units are associated with this constant.
     */
    template<class T>
    constexpr T SQRT_3                                                  = T(1.73205080757);

    /**
     * @brief The unit imaginary number, otherwise known as the square root of negative one. No units are associated with this constant.
     */
    template<class T>
    constexpr std::complex<T> I                                         = std::complex<T>(0, 1);

    /**
     * @brief The universal constant C, also known as the speed of light in a vacuum. The implied units of this constant are meters per
     * second (m/s). This can be calculated as 1 / sqrt(EPSILON_0 * MU_0).
     */
    template<class T>
    constexpr T C_0                                                     = T(2.99792458e8);

    /**
     * @brief The vacuum permittivity or permittivity of free space, which is also known as the electric constant. The implied units of
     * this constant are farads per meter (F/m).
     */
    template<class T>
    constexpr T EPSILON_0                                               = T(8.8541878128e-12);

    /**
     * @brief The vacuum permeability or permeability of free space, which is also known as the magnetic constant. The implied units of
     * this constant are henrys per meter (H/m), or newtons per square ampere (N/A^2).
     */
    template<class T>
    constexpr T MU_0                                                    = T(1.25663706212e-6);

    /**
     * @brief The vacuum impedance or impedance of free space. The implied units of this constant is omhs. This can be calculated as
     * sqrt(MU_0 / EPSILON_0).
     */
    template<class T>
    constexpr T Z_0                                                     = T(3.76730313461e2);
}
