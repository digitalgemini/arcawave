#pragma once

namespace ArcaWave::Geometry {
    struct VCMIndex {
        int_fast32_t Index;
        bool XFlip;
        bool YFlip;
        bool ZFlip;
    };

    template<typename T, uint_fast32_t V>
    struct Partition {

        // Public fields
    public:

        Eigen::Matrix<T, V, 1> Vertex;
        T Weight;
        bool Enabled;
        VCMIndex Index;

        // Public constructors and deconstructor
    public:

        Partition(Eigen::Matrix<T, V, 1> Vertex, T Weight, bool Enabled) :
                Vertex(Vertex), Weight(Weight), Enabled(Enabled) {}

        ~Partition() = default;
    };
}