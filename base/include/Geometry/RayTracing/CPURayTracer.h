
#pragma once

// ArcaWave includes
#include "Geometry/RayTracing/RayTracer.h"

namespace ArcaWave
{
    namespace Geometry
    {
        template<typename T, uint_fast32_t V>
        class CPURayTracer : public RayTracer<T, V>
        {

        // Public constructor and deconstructor
        public:

            CPURayTracer();

			CPURayTracer(const std::shared_ptr<Environment<T, V>>& env);

			~CPURayTracer();

        // Public methods
        public:

            virtual std::shared_ptr<IlluminatedInstance<T, V>> IlluminateInstance(const std::shared_ptr<IlluminateInstanceInfo<T, V>>& Info) const;

            virtual std::shared_ptr<IlluminatedEnvironment<T, V>> IlluminateEnvironment(const std::shared_ptr<IlluminateEnvironmentInfo<T, V>>& Info) const;

            virtual ProjectedRay<T, V> ProjectRay(const std::shared_ptr<ProjectRayInfo<T, V>>& Info) const;

			virtual std::vector<ProjectedRay<T, V>> ProjectRays(const LaunchInfo<T, V>& Info);

			// Private methods
        private:

            std::shared_ptr<std::shared_ptr<Submesh>[]> IntersectsInstance(const std::shared_ptr<IlluminatedInstance<T, V>>& Inst, const Ray<T, V>& R) const;

        // Private static methods
        private:

            static bool PointIllumination(const Triangle<T, V>& Tri, const Eigen::Matrix<T, V, 1>& Point, const Pose<T, V>& Pos);

            static bool DirectionIllumination(const Triangle<T, V>& Tri, const Eigen::Matrix<T, V, 1>& Direction, const Pose<T, V>& Pos);

        };
    }
}
