
#pragma once

// ArcaWave includes
#include "Geometry/Environment.h"
#include "Geometry/Ray.h"
#include <complex>

#ifdef USE_CUDA
#include <thrust/complex.h>
#endif

namespace ArcaWave
{
    namespace Geometry
    {
        enum IlluminationType
        {
            ILLUMINATION_POINT = 0,
            ILLUMINATION_DIRECTION = 1
        };

        template<typename T, uint_fast32_t V>
        struct IlluminateInstanceInfo
        {
            IlluminationType Type;
            Eigen::Matrix<T, V, 1> Vec;
            std::shared_ptr<Object<T, V>> Obj;
            std::shared_ptr<Instance<T, V>> Inst;
        };

        template<typename T, uint_fast32_t V>
        struct IlluminatedInstance
        {
            std::shared_ptr<Object<T, V>> Obj;
            std::shared_ptr<Instance<T, V>> Inst;
            std::shared_ptr<std::shared_ptr<Submesh>[]> IlluminatedSubmeshes;
        };

        template<typename T, uint_fast32_t V>
        struct IlluminateEnvironmentInfo
        {
            IlluminationType Type;
            Eigen::Matrix<T, V, 1> Vec;
            std::shared_ptr<Environment<T, V>> Env;
        };

        template<typename T, uint_fast32_t V>
        struct IlluminatedEnvironment
        {
            std::shared_ptr<Environment<T, V>> Env;
            std::shared_ptr<std::shared_ptr<IlluminatedInstance<T, V>>[]> IlluminatedInstances;
        };

        template<typename T, uint_fast32_t V>
        struct ProjectRayInfo
        {
            Ray<T, V> Ry;
            std::shared_ptr<Environment<T, V>> Env;
        };

		template<typename T, uint_fast32_t V>
		struct LaunchInfo
		{
			Matrix<T, V, 1> Pos;
            Matrix<T, V, V> Ori;
			uint_fast32_t Width;
			uint_fast32_t Height;
			T FOV;
			Matrix<T, V, 1> Up;
			Matrix<T, V, 1> LookAt;
			T WinWidth;
			T WinHeight;
			T FocalLen;
		};

        template<typename T, uint_fast32_t V>
        struct ProjectedRay
        {
        	bool Contact;
        	bool RxVisible;
            T Dist;
#ifdef USE_CUDA
            Eigen::Matrix<thrust::complex<T>, V, 1> Amplitude;
#endif
            Eigen::Matrix<T, V, 1> Point;
        };

		template<typename T, uint_fast32_t V>
        class RayTracer
        {

		 protected:
        	std::shared_ptr<Environment<T, V>> Env;

        // Public constructor and destructor
        public:

            RayTracer()
            {}

            RayTracer(std::shared_ptr<Environment<T, V>> Env) : Env(Env)
			{}

            virtual ~RayTracer()
            {}

        // Public methods
        public:

            virtual std::shared_ptr<IlluminatedInstance<T, V>> IlluminateInstance(const std::shared_ptr<IlluminateInstanceInfo<T, V>>& Info) const = 0;

            virtual std::shared_ptr<IlluminatedEnvironment<T, V>> IlluminateEnvironment(const std::shared_ptr<IlluminateEnvironmentInfo<T, V>>& Info) const = 0;

            virtual ProjectedRay<T, V> ProjectRay(const std::shared_ptr<ProjectRayInfo<T, V>>& Info) const = 0;

            virtual std::vector<ProjectedRay<T, V>> ProjectRays(const LaunchInfo<T, V>& Info) = 0;
        };
    }
}
