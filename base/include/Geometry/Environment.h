 
 #pragma once

 // STL includes
 #include <memory>
 #include <unordered_map>

 // ArcaWave includes
 #include "Geometry/Instance.h"
#include "Geometry/Object.h"

namespace ArcaWave
{
    namespace Geometry {
        /**
         * @brief The environment of the simulation which stores all the objects within the simulation
         */
        template<typename T, uint_fast32_t V>
        class Environment {

            // Private fields
        private:

            /**
             * @brief Keys for the Arcawave Environment and Scene JSONs
             */
            static constexpr auto OBJECTS_KEY = "objects";
            static constexpr auto INSTANCES_KEY = "instances";
            static constexpr auto OBJECT_ID_KEY = "object_index";
            static constexpr auto INSTANCE_ID_KEY = "instance_index";
            static constexpr auto INSTANCE_DESC_KEY = "instance_descriptions";
            static constexpr auto POSES_KEY = "poses";
            static constexpr auto VELOCITIES_KEY = "velocities";
            static constexpr auto INSTANCE_TYPE_KEY = "instance_type";
            static constexpr auto POSITION_KEY = "position";
            static constexpr auto ORIENTATION_KEY = "orientation";
            static constexpr auto TRANSLATIONAL_KEY = "translational";
            static constexpr auto ROTATIONAL_KEY = "rotational";
            static constexpr auto RADAR_KEY = "radar_params";


            /**
             * @brief timestep in simulation that the environment is currently representing
             */
            uint_fast32_t TimeStep = 0;

            /**
             * @brief The list of objects that can be instantiated within the environment.
             */
            std::vector<std::shared_ptr<Object<T, V>>> Objects;

            /**
             * @brief The list of instances of all objects that exist in the environment.
             */
            std::vector<std::shared_ptr<Instance<T, V>>> Instances;


            /**
             * @brief key is instance ID and stores a list of poses over time
             */
            std::unordered_map<uint_fast32_t, std::vector<Pose<T, V>>> Poses;

            /**
             * @brief key is instance id and stores a list of velocities over time
             */
            std::unordered_map<uint_fast32_t, std::vector<Velocity<T, V>>> Velocities;

            void BuildEnvironment(const std::string &Path);

            void BuildScene(const std::string &Path);

            // Public constructors and deconstructor
        public:

            /**
            * Position of antenna
            */
            Matrix<T, V, 1> TransmitterPos;

            Matrix<T, V, V> TransmitterOri;

            /**
             * @brief Creates an initially empty environment.
             */
            Environment();

            /**
             * @brief Destroys the environment.
             */
            ~Environment();


            // Public methods
        public:

            /**
             * @brief Import an Environment from an Arcawave Environment JSON.
             * @param Path - The location of JSON decribing environment, must follow schema as defined by ArcaWaveEnvironmentSchema.json
             */
            void ImportEnvironment(const std::string &Path);

            uint_fast32_t AddInstance(std::shared_ptr<Instance<T, V>> Inst);

            uint_fast32_t AddObject(std::shared_ptr<Object<T, V>> Obj);

            std::shared_ptr<Instance<T, V>> GetInstance(uint_fast32_t Index) const;

            std::shared_ptr<Object<T, V>> GetObject(uint_fast32_t Index) const;

            uint_fast32_t GetInstanceCount() const;

            uint_fast32_t GetObjectCount() const;

            const std::vector<std::shared_ptr<Object<T, V>>>& GetObjects() const;

            const std::vector<std::shared_ptr<Instance<T, V>>>& GetInstances() const;

            /* TEMPORARY @TODO */
            void UpdateEnvironment();

            void ApplyVelocities(const T Duration);
        };
    }
 }
