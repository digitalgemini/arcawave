
#pragma once

// STL includes
#include <vector>

// ArcaWave includes
#include "Geometry/Ray.h"
namespace ArcaWave {
    namespace  Geometry
    {
        /**
         * @brief A group of three vectors to be used in the triangle as either the positions or normals of the triangle's vertices.
         * 
         * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
         * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
         */
        template<typename T, uint_fast32_t V>
        struct Triad
        {
            /**
             * @brief The first vector of the triad.
             */
            Eigen::Matrix<T, V, 1> A;

            /**
             * @brief The second vector of the triad.
             */
            Eigen::Matrix<T, V, 1> B;

            /**
             * @brief The third vector of the triad.
             */
            Eigen::Matrix<T, V, 1> C;
        };

        /**
         * @brief Represents a triangular face of an object in 3D space.
         *
         * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
         * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
         */
        template<typename T, uint_fast32_t V>
        class Triangle
        {

        // Public fields
        public:

            /**
             * @brief The vertices of the triangle. Note these are stored in left-handed format, which is clockwise relative to the surface normal.
             */
            Triad<T, V> Vertices;

            /**
             * @brief The normal vectors of each vertex of the triangle. These should all be normallized to a length of one.
             */
            Triad<T, V> Normals;

            // Constructors and deconstructor
        public:

            /**
             * @brief Creates a new triangle without initializing the vertices or normals of the triangle. Note this constructor should only be used if the triangle will be edited
             * at some later point to give it valid vertices and normals. Otherwise use of this constructor should be limited.
             */
            Triangle();

            /**
             * @brief Creates a new triangle with the given vertices. Each normal is set to the orthogonal normal of the triangle surface.
             *
             * @param Vertices - The vertices of the triangle.
             */
            Triangle(const Triad<T, V>& Vertices);

            /**
             * @brief Creates a new triangle with the given vertices and normals.
             *
             * @param Vertices - The vertices of the triangle.
             * @param Normals - The normals of each vertex of the triangle. These should all be normalized to a length of one, but it is not enforced.
             */
            Triangle(const Triad<T, V>& Vertices, const Triad<T, V>& Normals);

            /**
             * @brief Destroys the triangle object.
             */
            ~Triangle();

            // Public methods
        public:

            /**
             * @brief Calculates and returns the area of the triangle.
             *
             * @return T - The area of the triangle.
             */
            inline T Area() const
            {
                return Area(this->Vertices);
            }

            /**
             * @brief Calculates and returns the barycentric coordinates of the given point on the triangle. Note if a point is given that is not
             * on the triangle it will return invalid barycentric coordinates, i.e. the sum of the coordinates will not equal one.
             *
             * @param Point - The point to compute the barycentric coordinates of.
             * @return Eigen::Matrix<T, V, 1> - The barycentric coordinates of the point on the triangle
             */
            Eigen::Matrix<T, V, 1> Barycentric(const Eigen::Matrix<T, V, 1>& Point) const;

            /**
             * @brief Calculates and returns the center of the triangle defined by centroid, or the intersection of the line going from the bisection
             * of each side to the opposite angle of the triangle. It is also the arithmetic average of the three vertices of the triangle.
             *
             * @return Eigen::Matrix<T, V, 1> - The coordinates of the centeroid of the triangle.
             */
            Eigen::Matrix<T, V, 1> Center() const;

            /**
             * @brief Determines if the given ray intersects with this triangle. If it does intersect, returns the distance from the ray's origin
             * (inversely scaled by the length of the ray), otherwise returns floating-point infinity.
             *
             * @param R - The ray with which to check intersection.
             * @return T - The distance from the origin of the ray to the triangle if it intersects, otherwise floating-point infinity.
             */
            T Intersects(const Ray<T, V>& R) const;

            /**
             * @brief Calculates the surface normal of the triangle, normallized to a lenght of one. Note, this is the clockwise normal of the
             * triangle with respect to vertices A, B, and C, and thus follows the left-hand rule.
             *
             * @return Eigen::Matrix<T, V, 1> - The surface normal of the triangle, normallized to a lenght of one.
             */
            Eigen::Matrix<T, V, 1> Normal() const;

            /**
             * @brief Calculate the linearly interpolated normal of the triangle at the given point, normallized to a length of one. Note, if
             * the given point is not on the triangle, then it is undefinied behavior.
             *
             * @param Point - The point on the triangle to get the interpolated normal of.
             * @return Eigen::Matrix<T, V, 1> - The interpolated normal at the given point, normallized to a length of one.
             */
            Eigen::Matrix<T, V, 1> Normal(const Eigen::Matrix<T, V, 1>& Point) const;

            /**
             * @brief Checks if the given three dimensional point is on the surface of the triangle.
             *
             * @param Point - The point to check.
             * @return true - The point is on the surface of the triangle.
             * @return false - The point is not on the surface of the triangle.
             */
            bool On(const Eigen::Matrix<T, V, 1>& Point) const;

            /**
             * @brief Calculates and returns a vector of the side lengths of the triangle. Note, the first element is the side length between A and B,
             * the second is the side length between B and C, and the third is the side length between A and C.
             *
             * @return Eigen::Matrix<T, V, 1> - The side lenghts of the triangle.
             */
            Eigen::Matrix<T, V, 1> Sides() const;

        // Private static methods
        private:

            /**
             * @brief Calculates the area of the implicit triangle defined by the given vertices.
             *
             * @param Vertices - The vertices of the triangle.
             */
            static T Area(const Triad<T, V>& Vertices);
        };
    }
}