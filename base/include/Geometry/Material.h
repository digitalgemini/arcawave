#pragma once

// STL includes
#include <memory>
#include <complex>

// ArcaWave includes
#include "Utility/Colormap.h"

namespace ArcaWave {
    namespace Geometry {
        /**
         * @brief The material for a single mesh, imported from assimp.
         *
         * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
         * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
         */
        template<typename T>
        class Material {
            // Private fields
        public:

            Utility::Color<T> MaterialColor;

            /**
             * @brief The permittivity of the mesh, this value of metal is 0.
             */
            T permittivity;

            /**
             * @brief The permeability of the mesh.
             */
            T permeability;

            /**
             * @brief The conductivity of the mesh.
             */
            T conductivity;

            // Public constructors
        public:

            /**
             * @brief Creates the Material of vacuum.
             */
            Material();

            /**
             * @brief Creates the Material given relative permittivity, relative permeability and conductivity.
             */
            Material(const T rel_permittivity, const T rel_permeability, const T conductivity);

            /**
             * @brief Destroys the Material.
             */
            ~Material();

            // Public methods
        public:

            /**
             * @brief Decide whether this material is PEC
             *
             * @return bool - True for PEC.
             */
            bool IsPEC() const;

            /**
             * @brief Gets the impedance of given material.
             *
             * @param angular_freq - The angular frequency to calculate impedance.
             * @return complex<T> - The impedance at angular_freq.
             */
            std::complex<T> GetImpedance(const T angular_freq) const;
            
            std::complex<T> GetWaveNumber(T angular_freq) const;
        };
    }
}