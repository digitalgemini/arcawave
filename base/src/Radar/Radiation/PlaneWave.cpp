#include "Radar/Radiation/PlaneWave.h"

// STL includes
#include <cmath>
#include <iostream>

// ArcaWave incldues
#include "Utility/Constants.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
PlaneWave<T, V>::PlaneWave()
{}

template<typename T, uint_fast32_t V>
CUDA_HOSTDEV PlaneWave<T, V>::PlaneWave(const Matrix<T, V, 1> Propogation, const Matrix<complex<T>, 2, 1>& Polarization, const Matrix<T, V, 1>& Source, T Freq, T FreqSlope) :
    RadiationField<T ,V>(Polarization, Source, Freq, FreqSlope), Propogation(Propogation.normalized())
{
    // Determine the plane vectors relative to the propogation vector (do not take a cross product of two parallel vectors as it will yield the zero vector)
    if(fabs(Propogation.dot(Matrix<T, V, 1>::UnitX())) - static_cast<T>(1.0) < -FLOATING_POINT_ERROR<T>)
        PlaneA = Propogation.cross(Matrix<T, V, 1>::UnitX()).normalized();
    else
        PlaneA = Propogation.cross(Matrix<T, V, 1>::UnitY()).normalized();
    PlaneB = Propogation.cross(PlaneA);
    // Follow the jones vector: (1, 0) means horizontal, (0, 1) means vertical. In our coordinate +x is front, +y is up.
    // if(fabs(Propogation.dot(Matrix<T, V, 1>::UnitY())) - static_cast<T>(1.0) < -FLOATING_POINT_ERROR<T>)
    //     HorizontalPlane = Propogation.cross(Matrix<T, V, 1>::UnitY()).normalized();
    // else
    //     HorizontalPlane = Propogation.cross(Matrix<T, V, 1>::UnitZ()).normalized();

    // if(fabs(Propogation.dot(Matrix<T, V, 1>::UnitX())) - static_cast<T>(1.0) < -FLOATING_POINT_ERROR<T>)
    //     VerticalPlane = Propogation.cross(Matrix<T, V, 1>::UnitX()).normalized();
    // else
    //     VerticalPlane = Propogation.cross(Matrix<T, V, 1>::UnitZ()).normalized();
}

template<typename T, uint_fast32_t V>
PlaneWave<T, V>::PlaneWave(const PlaneWave<T, V>& Other) :
    RadiationField<T, V>(Other.Polarization, Other.Source, Other.Freq, Other.FreqSlope), Propogation(Other.Propogation), PlaneA(Other.PlaneA), PlaneB(Other.PlaneB)
    // RadiationField<T, V>(Other.Polarization, Other.Source, Other.Freq, Other.FreqSlope), Propogation(Other.Propogation), HorizontalPlane(Other.HorizontalPlane), VerticalPlane(Other.VerticalPlane)
{}

template<typename T, uint_fast32_t V>
CUDA_HOSTDEV PlaneWave<T, V>::~PlaneWave()
{}

template<typename T, uint_fast32_t V>
PlaneWave<T, V>& PlaneWave<T, V>::operator=(const PlaneWave<T, V>& Other)
{
    this->Polarization = Other.Polarization;
    this->Source = Other.Source;
    this->Freq = Other.Freq;
    this->FreqSlope = Other.FreqSlope;
    Propogation = Other.Propogation;
    PlaneA = Other.PlaneA;
    PlaneB = Other.PlaneB;
    // HorizontalPlane = Other.HorizontalPlane;
    // VerticalPlane = Other.VerticalPlane;
    return *this;
}

template<typename T, uint_fast32_t V>
T PlaneWave<T, V>::Phase(T Time, const Matrix<T, V, 1>& Point) const
{
    const T adj_time = Time - ((Point - this->Source).dot(Propogation) / C_0<T>);
    const T full_phase = static_cast<T>(2) * PI<T> * adj_time * (this->Freq + (this->FreqSlope * adj_time / static_cast<T>(2)));
    return fmod(full_phase, 2 * PI<T>);
//    return full_phase;
}

template<typename T, uint_fast32_t V>
CUDA_HOSTDEV T PlaneWave<T, V>::Phase(T Time, T Dist) const
{
	const T adj_time = Time - (Dist) / C_0<T>;
    const T full_phase = static_cast<T>(2) * PI<T> * adj_time * (this->Freq + (this->FreqSlope * adj_time / static_cast<T>(2)));
    return fmod(full_phase, 2 * PI<T>);
//	return -static_cast<T>(2) * PI<T> * adj_time * (this->Freq + (this->FreqSlope * adj_time / static_cast<T>(2)));
}

template<typename T, uint_fast32_t V>
T PlaneWave<T, V>::DopplerPhase(T Time, T Dist, T RelVel) const
{
    const T doppler = (C_0<T> - RelVel) / (C_0<T> + RelVel);
    return doppler * Phase(Time, Dist);
}

template<typename T, uint_fast32_t V>
T PlaneWave<T, V>::Frequency(T Time, const Matrix<T, V, 1>& Point) const
{
    // Computes the adjusted time to reach the point from the source plane and uses that to compute frequency
    const T adj_time = Time - ((Point - this->Source).dot(Propogation) / C_0<T>);
    return this->Freq + this->FreqSlope * adj_time;
}

template<typename T, uint_fast32_t V>
Matrix<complex<T>, V, 1> PlaneWave<T, V>::ElectricField(T Time, const Matrix<T, V, 1>& Point) const
{    
    // Project the E field onto the plane taking polarization into account
    const complex<T> e = std::exp(I<T> * Phase(Time, Point));
    return (e * this->Polarization.x() * PlaneA) + (e * this->Polarization.y() * PlaneB);
    // return (e * this->Polarization.x() * HorizontalPlane) + (e * this->Polarization.y() * VerticalPlane);
}

template<typename T, uint_fast32_t V>
Matrix<complex<T>, V, 1> PlaneWave<T, V>::MagneticField(T Time, const Matrix<T, V, 1>& Point) const
{
    // Take the cross product of the propogation vector and E field
    return Propogation.cross(ElectricField(Time, Point)) / Z_0<T>;
}

// Explicitly defines templated instances
template class ArcaWave::Radar::PlaneWave<float, 3>;
template class ArcaWave::Radar::PlaneWave<double, 3>;
