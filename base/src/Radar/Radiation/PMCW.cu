
#include "Radar/Radiation/PMCW.h"

// STL includes
#include <cmath>
#include <iostream>

// ArcaWave incldues
#include "Utility/Constants.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
CUDA_HOSTDEV PMCW<T, V>::PMCW(const Matrix<T, V, 1> Propogation, const Matrix<complex<T>, 2, 1>& Polarization, const Matrix<T, V, 1>& Source, T Freq, T FreqSlope) :
    RadiationField<T ,V>(Polarization, Source, Freq, FreqSlope), Propogation(Propogation.normalized())
{
    // Determine the plane vectors relative to the propogation vector (do not take a cross product of two parallel vectors as it will yield the zero vector)
    if(fabs(Propogation.dot(Matrix<T, V, 1>::UnitX())) - static_cast<T>(1.0) < -FLOATING_POINT_ERROR<T>)
        PlaneA = Propogation.cross(Matrix<T, V, 1>::UnitX()).normalized();
    else
        PlaneA = Propogation.cross(Matrix<T, V, 1>::UnitY()).normalized();
    PlaneB = Propogation.cross(PlaneA);
//    AllPhase();
}

template<typename T, uint_fast32_t V>
PMCW<T, V>::PMCW(const PMCW<T, V>& Other) :
    RadiationField<T, V>(Other.Polarization, Other.Source, Other.Freq, Other.FreqSlope), Propogation(Other.Propogation), PlaneA(Other.PlaneA), PlaneB(Other.PlaneB)
{}

template<typename T, uint_fast32_t V>
CUDA_HOSTDEV PMCW<T, V>::~PMCW()
{}

template<typename T, uint_fast32_t V>
PMCW<T, V>& PMCW<T, V>::operator=(const PMCW<T, V>& Other)
{
    this->Polarization = Other.Polarization;
    this->Source = Other.Source;
    this->Freq = Other.Freq;
    this->FreqSlope = Other.FreqSlope;
    Propogation = Other.Propogation;
    PlaneA = Other.PlaneA;
    PlaneB = Other.PlaneB;
    return *this;
}

//template<typename T, uint_fast32_t V>
//void PMCW<T, V>::AllPhase()
//{
//    // Generate prbs
//    uint_fast32_t start = 2;
//    uint_fast32_t a = start;
//    period = 1;
//    string prbs;
//    uint_fast32_t mask = (1 << k) - 1;
//    while (true) {
//        uint_fast32_t bit = (((a >> 9) ^ (a >> 6)) & 1);
//        a = ((a << 1) | bit) & mask;
//        prbs += bit + '0';
//        if (a == start)
//            break;
//        period++;
//    }
//    // Convert to phase
//    phase.resize(period);
//    for (uint_fast32_t i = 0; i < period; ++i)
//        phase[i] = (prbs[i] - '0') * PI<T>;
//}

template<typename T, uint_fast32_t V>
T PMCW<T, V>::Phase(T Time, const Matrix<T, V, 1>& Point) const
{
    const T Dist = (Point - this->Source).dot(Propogation);
    return Phase(Time, Dist);
}

template<typename T, uint_fast32_t V>
CUDA_HOSTDEV T PMCW<T, V>::Phase(T Time, T Dist) const
{
	const T adj_time = Time - (Dist) / C_0<T>;
    uint_fast32_t index = adj_time * Rc;
    index = ((index % period) + period) % period;

    uint_fast32_t start = 2;
    uint_fast32_t a = start;
    uint_fast32_t mask = (1 << k) - 1;
    T p;
//    printf("index = %d, ", index);
    while (true) {
        uint_fast32_t bit = (((a >> 9) ^ (a >> 6)) & 1);
        a = ((a << 1) | bit) & mask;
        if (index == 0)
        {
            p = bit * PI<T>;
            break;
        }
        if (a == start)
            break;
        index--;
    }

//    printf("phase = %f\n", p);
    return static_cast<T>(2) * PI<T> * adj_time * this->Freq + p;
}

template<typename T, uint_fast32_t V>
T PMCW<T, V>::DopplerPhase(T Time, T Dist, T RelVel) const
{
    const T doppler = (C_0<T> - RelVel) / (C_0<T> + RelVel);
    return doppler * Phase(Time, Dist);
}

template<typename T, uint_fast32_t V>
T PMCW<T, V>::Frequency(T Time, const Matrix<T, V, 1>& Point) const
{
    return this->Freq;
}

template<typename T, uint_fast32_t V>
Matrix<complex<T>, V, 1> PMCW<T, V>::ElectricField(T Time, const Matrix<T, V, 1>& Point) const
{    
    // Project the E field onto the plane taking polarization into account
    complex<T> e = std::exp(I<T> * Phase(Time, Point));
//    e /= abs(e.real());
    return (e * this->Polarization.x() * PlaneA) + (e * this->Polarization.y() * PlaneB);
}

template<typename T, uint_fast32_t V>
Matrix<complex<T>, V, 1> PMCW<T, V>::MagneticField(T Time, const Matrix<T, V, 1>& Point) const
{
    // Take the cross product of the propogation vector and E field
    return Propogation.cross(ElectricField(Time, Point)) / Z_0<T>;
}

// Explicitly defines templated instances
template class ArcaWave::Radar::PMCW<float, 3>;
template class ArcaWave::Radar::PMCW<double, 3>;
