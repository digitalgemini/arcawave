#include <iostream>
#include <fstream>
#include <cmath>

#include "Radar/RadarClass.h"

using namespace std;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;
using namespace Eigen;

/**
 * @brief Construct a new Radar Class< T,  V>:: Radar Class object
 * 
 * Everything is by defualt
 * 
 */
template<typename T, uint_fast32_t V>
RadarClass<T, V>::RadarClass() :
Row_t (1), Column_t (1), Row_r(1), Column_r (1), type_info (1), 
BeamDirection (Matrix<T, 2, 1> (0.0, 0.0)),
Propagation (Matrix<T, V, 1>(1, 0, 0)), 
Polarization (Matrix<T, 2, 1>(1, 0)), 
Init_Freq (static_cast<T>(5e9)), 
Freq_slope (static_cast<T>(0.0)),
 Max_Freq (static_cast<T>(5e9))
{
    Transmitters = make_shared<vector<Matrix<T, V, 1>>>();
    Receivers = make_shared<vector<Matrix<T, V, 1>>>();
    Rad_Field = make_shared<vector<shared_ptr<RadiationField<T, V>>>>();

    Matrix<T, V, 1> trx(-10.0, 0.0, 0.0);
    // Creating default tx
    Transmitters->push_back(trx);
    shared_ptr<PlaneWave<T,V>> wave = make_shared<PlaneWave<T, V>>(Propagation, Polarization, trx, Init_Freq, Freq_slope);
    Rad_Field->push_back(wave);

    // Creating default rx
    Receivers->push_back(trx);
}    




/**
 * @brief Construct a new Radar Class< T,  V>:: Radar Class object
 * 
 * With default rx and tx locations around x and y axis
 * @param Row_t 
 * @param Column_t 
 * @param Row_r 
 * @param Column_r 
 * @param type_info 
 * @param Propagation 
 * @param Polarization 
 * @param Init_Freq 
 * @param Freq_slope 
 * @param Max_Freq 
 */
template<typename T, uint_fast32_t V>
RadarClass<T, V>::RadarClass(uint_fast32_t Row_t, uint_fast32_t Column_t, uint_fast32_t Row_r, uint_fast32_t Column_r, uint_fast32_t type_info,
                             Matrix<T, V, 1> Propagation, 
                             Matrix<complex<T>, 2, 1> Polarization,
                             T Init_Freq,
                             T Freq_slope,
                             T Max_Freq) :
Row_t (Row_t), Column_t (Column_t), Row_r(Row_r), Column_r (Column_r), type_info (type_info), 
BeamDirection (Matrix<T, 2, 1> (0.0, 0.0)),
Propagation (Propagation), 
Polarization (Polarization), 
Init_Freq (Init_Freq), 
Freq_slope (Freq_slope), 
Max_Freq (Max_Freq)
{
    Transmitters = make_shared<vector<Matrix<T, V, 1>>>();
    Receivers = make_shared<vector<Matrix<T, V, 1>>>();
    Rad_Field = make_shared<vector<shared_ptr<RadiationField<T, V>>>>();
    T lamda_half =  C_0<T>/(Max_Freq*2.0) ;
    for(uint_fast32_t i = 0; i < Row_t; i++)
    {
        for(uint_fast32_t j = 0; j < Column_t; j++)
        {
            Matrix<T, V, 1> tx = Matrix<T, V, 1>(-10.0-lamda_half* floor(Row_t/2)+i*lamda_half,  -floor(Column_t/2)*lamda_half+j*lamda_half, 0.0);
            if(type_info == 0)
            {
                shared_ptr<PlaneWave<T, V>> wave = make_shared<PlaneWave<T, V>>(Propagation, Polarization, tx, Init_Freq, Freq_slope);
                Rad_Field->push_back(wave);
            }
            if(type_info == 1)
            {
                shared_ptr<PatchAntenna<T, V>> wave = make_shared<PatchAntenna<T, V>>(Polarization, tx, Init_Freq, Freq_slope);
                Rad_Field->push_back(wave);
            }
            Transmitters->push_back(tx);
        }
    }

    for(uint_fast32_t i = 0; i < Row_r; i++)
    {
        for(uint_fast32_t j = 0; j < Column_r; j++)
        {
            Matrix<T, V, 1> rx = Matrix<T, V, 1>(-10.0, (-lamda_half)*floor(Row_r/2)+lamda_half*i, (-lamda_half)*floor(Column_r/2)+lamda_half*j);
            Receivers->push_back(rx);
        }
    }

}

            

/**
 * @brief Construct a new Radar Class< T,  V>:: Radar Class object
 * 
 * Everything except rx and tx locations are by defuault
 * @param Row_t 
 * @param Column_t 
 * @param Row_r 
 * @param Column_r 
 * @param type_info 
 */
template<typename T, uint_fast32_t V>
RadarClass<T, V>::RadarClass(uint_fast32_t Row_t, uint_fast32_t Column_t,uint_fast32_t Row_r, uint_fast32_t Column_r,  
                             uint_fast32_t type_info):
Row_t (Row_t), Column_t (Column_t), Row_r(Row_r), Column_r (Column_r), type_info (type_info), 
BeamDirection (Matrix<T, 2, 1> (0.0, 0.0)),
Propagation (Matrix<T, V, 1>(1, 0, 0)), 
Polarization (Matrix<T, 2, 1>(1, 0)), 
Init_Freq (static_cast<T>(5e9)), 
Freq_slope (static_cast<T>(0.0)), 
Max_Freq (static_cast<T>(5e9))
{
    Transmitters = make_shared<vector<Matrix<T, V, 1>>>();
    Receivers = make_shared<vector<Matrix<T, V, 1>>>();
    Rad_Field = make_shared<vector<shared_ptr<RadiationField<T, V>>>>();


    T lamda_half =  C_0<T>/(Max_Freq*2.0) ;
    for(uint_fast32_t i = 0; i < Row_t; i++)
    {
        for(uint_fast32_t j = 0; j < Column_t; j++)
        {
            Matrix<T, V, 1> tx = Matrix<T, V, 1>(-10.0-lamda_half* floor(Row_t/2)+i*lamda_half,  -floor(Column_t/2)*lamda_half+j*lamda_half, 0.0);
            if(type_info == 0)
            {
                shared_ptr<PlaneWave<T, V>> wave = make_shared<PlaneWave<T, V>>(Propagation, Polarization, tx, Init_Freq, Freq_slope);
                Rad_Field->push_back(wave);
            }
            if(type_info == 1)
            {
                shared_ptr<PatchAntenna<T, V>> wave = make_shared<PatchAntenna<T, V>>(Polarization, tx, Init_Freq, Freq_slope);
                Rad_Field->push_back(wave);
            }
            Transmitters->push_back(tx);
        }
    }

    for(uint_fast32_t i = 0; i < Row_r; i++)
    {
        for(uint_fast32_t j = 0; j < Column_r; j++)
        {
            Matrix<T, V, 1> rx = Matrix<T, V, 1>(-10.0, (-lamda_half)*floor(Row_r/2)+lamda_half*i, (-lamda_half)*floor(Column_r/2)+lamda_half*j);
            Receivers->push_back(rx);
        }
    }
}


/**
 * @brief Construct a new Radar Class< T,  V>:: Radar Class object
 * 
 * Fully customized Radar construction 
 * @param Row_t 
 * @param Column_t 
 * @param tx_arr 
 * @param Row_r 
 * @param Column_r 
 * @param rx_arr 
 * @param type_info 
 * @param Propagation 
 * @param Polarization 
 * @param Init_Freq 
 * @param Freq_slope 
 * @param Max_Freq 
 */
template<typename T, uint_fast32_t V>
RadarClass<T, V>::RadarClass(uint_fast32_t Row_t, uint_fast32_t Column_t, shared_ptr<vector<Matrix<T, V, 1>>> tx_arr, 
                             uint_fast32_t Row_r, uint_fast32_t Column_r, shared_ptr<vector<Matrix<T, V, 1>>> rx_arr, 
                             uint_fast32_t type_info,
                             Eigen::Matrix<T, V, 1> Propagation, 
                             Eigen::Matrix<std::complex<T>, 2, 1> Polarization,
                             T Init_Freq,
                             T Freq_slope,
                             T Max_Freq) :
Row_t (Row_t), Column_t (Column_t), Row_r(Row_r), Column_r (Column_r), type_info (type_info), 
BeamDirection (Matrix<T, 2, 1> (0.0, 0.0)),
Propagation (Propagation), 
Polarization (Polarization), 
Init_Freq (Init_Freq), 
Freq_slope (Freq_slope), 
Max_Freq (Max_Freq)
{
    Transmitters = make_shared<vector<Matrix<T, V, 1>>>();
    Receivers = make_shared<vector<Matrix<T, V, 1>>>();
    Rad_Field = make_shared<vector<shared_ptr<RadiationField<T, V>>>>();

    // recheck this
    if(Row_t*Column_t != tx_arr->size()) 
    {
        cout << "Inconsisiten Transmitters array dimensions" << endl;
        return;
    }else if(Row_r*Column_r != rx_arr->size())
    {
        cout << "Inconsisiten Receivers array dimensions" << endl;
        return;
    }

    T lamda_half =  C_0<T>/(Max_Freq*2.0) ;

    for(uint_fast32_t i = 0; i < tx_arr->size(); i++)
    {
        Matrix<T, V, 1> tx = tx_arr->at(i);
        if(type_info == 0)
        {
            shared_ptr<PlaneWave<T, V>> wave = make_shared<PlaneWave<T, V>>(Propagation, Polarization, tx, Init_Freq, Freq_slope);
            Rad_Field->push_back(wave);
        }
        if(type_info == 1)
        {
            shared_ptr<PatchAntenna<T, V>> wave = make_shared<PatchAntenna<T, V>>(Polarization, tx, Init_Freq, Freq_slope);
            Rad_Field->push_back(wave);
        }

        Transmitters->push_back(tx);
    }

    for(uint_fast32_t j = 0; j < rx_arr->size(); j++)
    {
        Matrix<T, V, 1> rx = rx_arr->at(j);
        Receivers->push_back(rx);
    }


}



template<typename T, uint_fast32_t V>
shared_ptr<vector<Matrix<T, V, 1>>> RadarClass<T, V>::getTransmitters()
{
    return Transmitters;
}

            
template<typename T, uint_fast32_t V>
shared_ptr<vector<Matrix<T, V, 1>>> RadarClass<T, V>::getReceivers()  
{
    return Receivers;
}

            
template<typename T, uint_fast32_t V>
shared_ptr<vector<shared_ptr<RadiationField<T, V>>>> RadarClass<T, V>::getRadiators()  
{
    return Rad_Field;
}

template<typename T, uint_fast32_t V>
shared_ptr<PhasedArray<T, V>> RadarClass<T, V>::setPhasedArray(Matrix<T, 2,1> BeamDirection)
{
    T phase = PI<T>/2.0;
    shared_ptr<PhasedArray<T, V>> pha = make_shared<PhasedArray<T, V>>(BeamDirection, Rad_Field, phase, Row_t, Column_t);
    return pha;
}

            

template class ArcaWave::Radar::RadarClass<float, 3>;
template class ArcaWave::Radar::RadarClass<double, 3>;