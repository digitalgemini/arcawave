#include "Radar/VCM.h"

// STL includes
#include <chrono>
#include <cstdint>
#include <iostream>
#include <memory>

// ArcaWave includes
#include "Radar/Radiation/PlaneWave.h"
#include "Utility/Constants.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Geometry;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
VCM<T, V>::VCM(const shared_ptr<VCMBuildInfo<T, V>>& Build) :
	Build(Build)
{
	const uint_fast32_t size = Build->IncThetaInterval.Size * Build->IncPhiInterval.Size * Build->RefThetaInterval.Size * Build->RefPhiInterval.Size;
    Moments = shared_ptr<Matrix<complex<T>, V, 1>[]>(new Matrix<complex<T>, V, 1>[size]);
	// RefIndicies = shared_ptr<uint_fast32_t[]>(new uint_fast32_t[Build->IncThetaInterval.Size * Build->IncPhiInterval.Size]);
}

template<typename T, uint_fast32_t V>
VCM<T, V>::~VCM()
{}

template<typename T, uint_fast32_t V>
uint_fast32_t VCM<T, V>::GetIndex(const Matrix<T, 2, 1>& Incidence, const Matrix<T, 2, 1>& Reflection)
{
	const uint_fast32_t ref_phi_dim = Build->RefThetaInterval.Size;
	const uint_fast32_t inc_theta_dim = Build->RefPhiInterval.Size * Build->RefThetaInterval.Size;
	const uint_fast32_t inc_phi_dim =  Build->IncThetaInterval.Size * Build->RefPhiInterval.Size * Build->RefThetaInterval.Size;

	return Build->RefThetaInterval.Find(Reflection.x()) +
		(Build->RefPhiInterval.Find(Reflection.y()) * ref_phi_dim) +
		(Build->IncThetaInterval.Find(Incidence.x()) * inc_theta_dim) +
		(Build->IncPhiInterval.Find(Incidence.y()) * inc_phi_dim);
}

template<typename T, uint_fast32_t V>
Matrix<std::complex<T>, V, 1> VCM<T, V>::GetMoment(const Matrix<T, 2, 1>& Incidence, const Matrix<T, 2, 1>& Reflection)
{
	return Moments[GetIndex(Incidence, Reflection)];
}

template<typename T, uint_fast32_t V>
void VCM<T, V>::SetMoment(const Matrix<T, 2, 1>& Incidence, const Matrix<T, 2, 1>& Reflection, const Matrix<std::complex<T>, V, 1>& MomentValue)
{
	Moments[GetIndex(Incidence, Reflection)] = MomentValue;
}

// template<typename T, uint_fast32_t V>
// uint_fast32_t VCM<T, V>::GetStrongestReflection(const Matrix<T, 2, 1>& Incidence)
// {
// 	uint_fast32_t idx = Build->IncThetaInterval.Find(Incidence.x()) + (Build->IncPhiInterval.Find(Incidence.y()) * Build->IncThetaInterval.Size);
// 	return RefIndicies[idx];
// }

template<typename T, uint_fast32_t V>
shared_ptr<VCM<T, V>> VCM<T, V>::Bake(const shared_ptr<VCMBakeInfo<T, V>>& BakeInfo)
{
    // Creates the VCM
    shared_ptr<VCM<T, V>> vcm = make_shared<VCM<T, V>>(BakeInfo->Build);

	cout << "Discretizing... ";

    // Discretizes each mesh of the object
    shared_ptr<shared_ptr<DiscretizedMesh<T, V>>[]> disc_obj = shared_ptr<shared_ptr<DiscretizedMesh<T, V>>[]>(new shared_ptr<DiscretizedMesh<T, V>>[BakeInfo->Obj->GetMeshCount()]);
    for(uint_fast32_t i = 0; i < BakeInfo->Obj->GetMeshCount(); ++i)
    {
        shared_ptr<DiscretizeMeshInfo<T, V>> mesh_info = make_shared<DiscretizeMeshInfo<T, V>>();
        mesh_info->Msh = BakeInfo->Obj->GetMesh(i);
        mesh_info->Sub = mesh_info->Msh->GetFullSubmesh();
        mesh_info->Resolution = C_0<T> / (BakeInfo->Build->Frequency * 10);
        mesh_info->Interpolate = false;
        disc_obj[i] = BakeInfo->Disc->DiscretizeMesh(mesh_info);
    }

	// Counts points in the discretized mesh
	// uint_fast32_t count = 0;
	// for(uint_fast32_t i = 0; i < disc_obj[0]->FaceCount; ++i)
	// 	count += disc_obj[0]->Faces[i]->VertexCount;

	cout << "Done: " << endl;

    // Load job parameters
    shared_ptr<BakeJobParameter> job_parameter = make_shared<BakeJobParameter>();
    job_parameter->DiscObj = disc_obj;
    job_parameter->Obj = BakeInfo->Obj;
    job_parameter->Tracer = BakeInfo->ObjTracer;
	job_parameter->VectorCurrentMoment = vcm;
	job_parameter->CurrentCalculatorObject = BakeInfo->CurrentCalculator;

    // Illuminating
	cout << "Baking... ";
    BakeInfo->ThrdPool->Dispatch(BakeInfo->ThrdPool->GetMaxThreads(), JobFunction(BakeJobFunction), reinterpret_pointer_cast<JobParameter, BakeJobParameter>(job_parameter));
	cout << "Done" << endl;

	// Calculates the surface area of the object
	T surface_area = static_cast<T>(0);
	for(uint_fast32_t i = 0; i < BakeInfo->Obj->GetMeshCount(); ++i)
	{
		shared_ptr<Mesh<T, V>> mesh = BakeInfo->Obj->GetMesh(i);
		for(uint_fast32_t j = 0; j < mesh->GetFaceCount(); ++j)
		{
			Triangle<T, V> tri = mesh->GetTriangle(j);
			surface_area += tri.Area();
		}
	}

	// Normalizes the VCM by surface area
	const uint_fast32_t size = vcm->Build->IncThetaInterval.Size * vcm->Build->IncPhiInterval.Size * vcm->Build->RefThetaInterval.Size * vcm->Build->RefPhiInterval.Size;
	for(uint_fast32_t i = 0; i < size; ++i)
	{
		vcm->Moments[i] = vcm->Moments[i] / surface_area;
	}

    // Returns the baked VCM
    return vcm;
}

// template<typename T, uint_fast32_t V>
// shared_ptr<VCM<T, V>> VCM<T, V>::Import(const filesystem::path& Parameters, const filesystem::path& Data)
// {
//     // Constructing a VCM instance
//     T frequency;
//     Interval<T> ref_phi(1);
//     Interval<T> ref_theta(1);
//     Interval<T> inc_phi(1);
//     Interval<T> inc_theta(1);
//     T p_real_x, p_imag_x, p_real_y, p_imag_y;

// 	// Imports the parameters
//     ifstream import_parameters(Parameters, ios::in);
//     import_parameters	>> frequency
//     					>> p_real_x >> p_imag_x >> p_real_y >> p_imag_y
// 						>> inc_theta.Size >> inc_theta.Step >> inc_theta.Offset
// 						>> inc_phi.Size >> inc_phi.Step >> inc_phi.Offset
//     					>> ref_theta.Size >> ref_theta.Step >> ref_theta.Offset
// 						>> ref_phi.Size >> ref_phi.Step >> ref_phi.Offset;

// 	import_parameters.close();

// 	// Constructs the VCM object
// 	const shared_ptr<VCMBuildInfo<T, V>> build_info = make_shared<VCMBuildInfo<T, V>>();
// 	build_info->Mode = VCM_BISTATIC;
// 	build_info->Frequency = frequency;
// 	build_info->Polarization = Matrix<complex<T>, 2, 1>(complex<T>(p_real_x, p_imag_x), complex<T>(p_real_y, p_imag_y));
// 	build_info->IncThetaInterval = inc_theta;
// 	build_info->IncPhiInterval = inc_phi;
// 	build_info->RefThetaInterval = ref_theta;
// 	build_info->RefPhiInterval = ref_phi;
// 	shared_ptr<VCM<T,V>> vcm = make_shared<VCM<T,V>>(build_info);

// 	// Imports the data
// 	ifstream import_data(Data, ios::in);
// 	const uint_fast32_t size = inc_theta.Size * inc_phi.Size * ref_theta.Size * ref_phi.Size;
// 	for(uint_fast32_t i = 0; i < size; ++i)
// 	{
// 		T rx, ix, ry, iy, rz, iz;
// 		import_data >> rx >> ix >> ry >> iy >> rz >> iz;
// 		vcm->Moments[i] = Matrix<complex<T>, V, 1>(complex<T>(rx, ix), complex<T>(ry, iy), complex<T>(rz, iz));
// 	}
// 	import_data.close();

// 	// TODO: Temporary
// 	for(uint_fast32_t inc_phi = 0; inc_phi < vcm->Build->IncPhiInterval.Size; ++inc_phi)
// 	{
// 		for(uint_fast32_t inc_theta = 0; inc_theta < vcm->Build->IncThetaInterval.Size; ++inc_theta)
// 		{
// 			const T i_theta = vcm->Build->IncThetaInterval[inc_theta];
// 			const T i_phi = vcm->Build->IncPhiInterval[inc_phi];
// 			const Matrix<T, 2, 1> inc(i_theta, i_phi);
// 			int_fast32_t strongest_idx = -1;

// 			for(uint_fast32_t ref_phi = 0; ref_phi < vcm->Build->RefPhiInterval.Size; ++ref_phi)
// 			{
// 				for(uint_fast32_t ref_theta = 0; ref_theta < vcm->Build->RefThetaInterval.Size; ++ref_theta)
// 				{
// 					const T r_theta = vcm->Build->RefThetaInterval[ref_theta];
// 					const T r_phi = vcm->Build->RefPhiInterval[ref_phi];
// 					const Matrix<T, 2, 1> ref(r_theta, r_phi);

// 					// Don't consider the same the same direction towards source
// 					if(abs(r_theta - i_theta) < FLOATING_POINT_ERROR<T> && abs(r_phi - i_phi) < FLOATING_POINT_ERROR<T>)
// 						continue;

// 					const uint_fast32_t idx = vcm->GetIndex(inc, ref);
// 					if(strongest_idx == -1 || vcm->Moments[idx].norm() > vcm->Moments[strongest_idx].norm())
// 						strongest_idx = static_cast<int_fast32_t>(idx);
// 				}
// 			}

// 			vcm->RefIndicies[inc_theta + (inc_phi * vcm->Build->IncThetaInterval.Size)] = static_cast<uint_fast32_t>(strongest_idx);
// 		}
// 	}

// 	// Returns the pointer to the imported VCM
//     return vcm;
// }

// template<typename T, uint_fast32_t V>
// void VCM<T, V>::Export(const filesystem::path& Parameters, const filesystem::path& Data, std::shared_ptr<VCM<T, V>> VectorCurrentMoment)
// {
// 	shared_ptr<VCM<T, V>> vcm = VectorCurrentMoment;

// 	// Outputs the parameters
//     ofstream recording (Parameters, ios::out);
//     recording 	<< vcm->Build->Frequency << " "
//     			<< vcm->Build->Polarization.x().real() << " "  << vcm->Build->Polarization.x().imag() << " " 
// 				<< vcm->Build->Polarization.y().real() << " " << vcm->Build->Polarization.y().imag() << " "
//     			<< vcm->Build->IncThetaInterval.Size << " "<< vcm->Build->IncThetaInterval.Step << " " << vcm->Build->IncThetaInterval.Offset << " "
// 				<< vcm->Build->IncPhiInterval.Size << " " << vcm->Build->IncPhiInterval.Step << " " << vcm->Build->IncPhiInterval.Offset << " "
// 				<< vcm->Build->RefThetaInterval.Size << " " << vcm->Build->RefThetaInterval.Step << " " << vcm->Build->RefThetaInterval.Offset << " "
// 				<< vcm->Build->RefPhiInterval.Size << " " << vcm->Build->RefPhiInterval.Step << " " << vcm->Build->RefPhiInterval.Offset << endl;
// 	recording.close();

// 	// Outputs the data
// 	ofstream export_data (Data, ios::out);
// 	uint_fast32_t size =  	vcm->Build->IncThetaInterval.Size * 
// 							vcm->Build->IncPhiInterval.Size * 
// 							vcm->Build->RefThetaInterval.Size * 
// 							vcm->Build->RefPhiInterval.Size;
// 	for(uint_fast32_t i = 0; i < size; ++i)
// 	{
// 		Eigen::Matrix<std::complex<T>, V, 1>  k = vcm->Moments[i];
// 		export_data	<< k.x().real() << " " << k.x().imag() << " "
// 					<< k.y().real() << " " << k.y().imag() << " "
// 					<< k.z().real() << " " << k.z().imag() << endl;
// 	}
// 	export_data.close();
// }

template<typename T, uint_fast32_t V>
void VCM<T, V>::BakeJobFunction(uint_fast32_t ThreadID, uint_fast32_t ThreadCount, const shared_ptr<JobParameter>& Parameter)
{
	// Gets the job parameter
    const shared_ptr<BakeJobParameter> param = reinterpret_pointer_cast<BakeJobParameter, JobParameter>(Parameter);
	const shared_ptr<Object<T, V>> obj = param->Obj;
	const shared_ptr<shared_ptr<DiscretizedMesh<T, V>>[]> disc_obj = param->DiscObj;
	const shared_ptr<VCM<T, V>> vcm = param->VectorCurrentMoment;
	const shared_ptr<CurrentCalculator<T, V>> calculator = make_shared<CurrentCalculator<T, V>>(*param->CurrentCalculatorObject);

	// Computes the starting and ending indices for the thread to work on
	const uint_fast32_t work_size = vcm->Build->IncThetaInterval.Size * vcm->Build->IncPhiInterval.Size;
	const uint_fast32_t start = (work_size / ThreadCount) * ThreadID + (ThreadID < (work_size % ThreadCount) ? ThreadID : (work_size % ThreadCount));
	const uint_fast32_t end = start + (work_size / ThreadCount) + (ThreadID < (work_size % ThreadCount) ? 1 : 0);

	// Goes through each index the thread must compute
	for (uint_fast32_t idx = start; idx < end; ++idx)
	{
		// Gest the vector of incidence
		const T inc_theta = vcm->Build->IncThetaInterval.Evaluate(idx % vcm->Build->IncThetaInterval.Size);
		const T inc_phi = vcm->Build->IncPhiInterval.Evaluate(idx / vcm->Build->IncThetaInterval.Size);
		const Matrix<T, V, 1> inc_vec(cos(inc_theta) * sin(inc_phi), cos(inc_phi), sin(inc_theta) * sin(inc_phi));

		// Illuminates the object
		const shared_ptr<IlluminateInstanceInfo<T, V>> info = make_shared<IlluminateInstanceInfo<T, V>>();
		info->Type = IlluminationType::ILLUMINATION_DIRECTION;
		info->Vec = inc_vec;
		info->Obj = param->Obj;
		info->Inst = make_shared<Instance<T, V>>(0, 0, STATIC);
		const shared_ptr<IlluminatedInstance<T, V>> illum = param->Tracer->IlluminateInstance(info);
		
		// Records the incident direction and generates the source to excite the object and induce currents
		// calculator->SetIncident(inc_vec, vcm->Build->Frequency, vcm->Build->Polarization);
		const PlaneWave<T, V> pw(inc_vec, vcm->Build->Polarization, Matrix<T, V, 1>(0, 0, 0), vcm->Build->Frequency, 0);

		// Goes through each necessary reflection angle
		for (uint_fast32_t ref_phi = 0; ref_phi < vcm->Build->RefPhiInterval.Size; ++ref_phi)
		{
			const T phi = vcm->Build->RefPhiInterval.Evaluate(ref_phi);
			for (uint_fast32_t ref_theta = 0; ref_theta < vcm->Build->RefThetaInterval.Size; ++ref_theta)
			{
				// auto begin_time = chrono::high_resolution_clock::now();

				// Calculates the reflection angle
				const T theta = vcm->Build->RefThetaInterval.Evaluate(ref_theta);
				const Matrix<T, V, 1> r_unit(cos(theta) * sin(phi), cos(phi), sin(theta) * sin(phi));

				// Records the reflection direction and initialize zero scatter field
				// calculator->SetReflected(r_unit);

				// Computes the scatter field for each mesh
				Matrix<complex<T>, V, 1> sum(0, 0, 0);
				for (uint_fast32_t i = 0; i < obj->GetMeshCount(); ++i)
				{
					shared_ptr<Submesh> sub = illum->IlluminatedSubmeshes[i];

					// calculator->SetMaterial(param->Obj->GetMesh(i)->GetMaterial());

					for (uint_fast32_t j = 0; j < sub->GetFaceCount(); ++j)
					{
						uint_fast32_t face = sub->GetFace(j);
						shared_ptr<DiscretizedFace<T, V>> disc_face = disc_obj[i]->Faces[face];

						for (uint_fast32_t k = 0; k < disc_face->VertexCount; ++k)
						{
							// Adds scatter field of this discretized face
							// calculator->AddScatter(disc_face, k);

							const Matrix<complex<T>, V, 1> current = static_cast<T>(2.0) * static_cast<Matrix<complex<T>, V, 1>>(disc_face->Normals[k]).cross(pw.MagneticField(0, disc_face->Vertices[k]));
							const Matrix<T, V, 1> vert = disc_face->Vertices[k];
							const T wavenumber = static_cast<T>(2) * PI<T> * pw.Frequency(0, vert) / C_0<T>;
							sum += disc_face->Areas[k] * wavenumber * current * std::exp(-I<T> * wavenumber * r_unit.dot(vert));
						}
					}
				}
				sum *= I<T> * Z_0<T> / (4 * PI<T>);

				// Loads the moment
				// vcm->SetMoment(Matrix<T, 2, 1>(inc_theta, inc_phi), Matrix<T, 2, 1>(theta, phi), calculator->GetScatter());
				vcm->SetMoment(Matrix<T, 2, 1>(inc_theta, inc_phi), Matrix<T, 2, 1>(theta, phi), sum);

				// auto end_time = chrono::high_resolution_clock::now();
				// chrono::duration<double> elapsed = end_time - begin_time;
				// cout 	<< "[" << ThreadID << "] " << (idx - start) << "/" << (end - start)
				// 		<< " --- " << (ref_phi * vcm->Build->RefThetaInterval.Size + ref_theta) << "/" << (vcm->Build->RefPhiInterval.Size * vcm->Build->RefThetaInterval.Size) 
				// 		<< " --- " << elapsed.count() << " seconds" << endl;
			}
		}
	}
}

// Explicitly defines templated instances
template class ArcaWave::Radar::VCM<float, 3>;
template class ArcaWave::Radar::VCM<double, 3>;
