#include <Radar/DSP/FFT.h>

template<typename T>
std::vector<std::complex<T>> ArcaWave::Radar::FFTEngine<T>::forward_fft(std::vector<T> time_domain)
{
	auto output = std::vector<std::complex<T>>();
	Eigen::FFT<T> engine;
	engine.fwd(output, time_domain);
	return output;
}

// Explicitly defines templated instances
template class ArcaWave::Radar::FFTEngine<float>;
template class ArcaWave::Radar::FFTEngine<double>;