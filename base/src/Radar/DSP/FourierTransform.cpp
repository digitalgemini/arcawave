#include "Radar/DSP/FourierTransform.h"
#include "Radar/DSP/FFTEngine.h"
#include <cstdio>
#include <type_traits>
#include <algorithm>


# define FFT_SIZE 128


using namespace std;
using namespace ArcaWave;
using namespace Eigen;
using namespace ArcaWave::Radar;



template <typename T, uint_fast32_t V>
FourierTransform<T, V>::FourierTransform(vector<Matrix<complex<T>, V, 1>>& Mixed, DSPprocess<T,V>& info)
{

	Range = make_shared<vector<complex<T>>>();
	RawData = make_shared<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>>();

	uint_fast32_t jump = info.r_rows*info.r_cols;

	/* moving simulation data into a dsp data structure easy for the first round of dsp use */
	for(int i = 0; i < info.r_rows; i++)
	{
		shared_ptr<vector<shared_ptr<vector<complex<T>>>>> outter_collector = make_shared<vector<shared_ptr<vector<complex<T>>>>>();
		for(int j = 0; j < info.r_cols; j++)
		{
			uint_fast32_t idx = i * info.r_rows + j;
			shared_ptr<vector<complex<T>>> collector = make_shared<vector<complex<T>>> ();
			for(int s = 0; s < info.num_samples; s++)
			{
				collector->push_back(Mixed[s *jump + idx].y());
			}
			outter_collector->push_back(collector);
		}
		RawData->push_back(outter_collector);
	}
	processing(info);
}


/* DSP for Range Azimuth and Elevation */

template<typename T, uint_fast32_t V>
void FourierTransform<T, V>::processing(DSPprocess<T,V>& info)
{
	// Range fft processing 
	Range = forward_fft_single(RawData->at(0)->at(0));

	/* first round of fft */
	RawData = forward_fft();

	/* second roung of fft */
	if(info.r_rows > 1)
	{
		RawData = SwapAxes(0, 2);
		RawData = forward_fft();
		RawData = SwapAxes(0, 2);
	}
		
	/* third round of fft */
	if(info.r_cols > 1)
	{
		RawData = SwapAxes(1, 2);
		RawData = forward_fft();
		RawData = SwapAxes(1, 2);
	}

	/* last shift */
	RawData = fft_shift();
}



template<typename T, uint_fast32_t V>
shared_ptr<vector<complex<T>>> FourierTransform<T, V>::slice_vector(shared_ptr<vector<complex<T>>> vec)
{
	if(vec->size() >= FFT_SIZE)
	{
		auto begin = vec->begin();
		auto end = vec->begin() + FFT_SIZE;
		shared_ptr<vector<complex<T>>> newvec = make_shared<vector< complex<T>>> (begin, end);
		return newvec;
	}else{
		auto begin = vec->begin();
		auto end = vec->begin() + vec->size();
		shared_ptr<vector<complex<T>>> newvec = make_shared<vector< complex<T>>> (begin, end);
		while(newvec->size() < FFT_SIZE)
		{
			newvec->push_back(complex<T> (0,0));
		}
		return newvec;
	}
}





template<typename T, uint_fast32_t V>
shared_ptr<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>> FourierTransform<T, V>::SwapAxes(uint_fast32_t Axis1, uint_fast32_t Axis2)
{
	uint_fast32_t dim_0 = 0;
	uint_fast32_t dim_1 = 0;
	uint_fast32_t dim_2 = 0;
	if((Axis1 == 0 && Axis2 == 1) || (Axis1 == 1 && Axis2 == 0))
	{
		dim_0 = RawData->at(0)->size();
		dim_1 = RawData->size();
		dim_2 = RawData->at(0)->at(0)->size();
	}
	if((Axis1 == 0 && Axis2 == 2) || (Axis1 == 2 && Axis2 == 0))
	{
		dim_0 = RawData->at(0)->at(0)->size();
		dim_1 = RawData->at(0)->size();
		dim_2 = RawData->size();
	}
	if((Axis1 == 1 && Axis2 == 2) || (Axis1 == 2 && Axis2 == 1))
	{
		dim_0 = RawData->size();
		dim_1 = RawData->at(0)->at(0)->size();
		dim_2 = RawData->at(0)->size();
	}


	shared_ptr<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>> ret = make_shared<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>>();

	for(int i = 0; i < dim_0; i++)	
	{
		shared_ptr<vector<shared_ptr<vector<complex<T>>>>> outter = make_shared<vector<shared_ptr<vector<complex<T>>>>>();
		for(int j = 0; j < dim_1; j++)	
		{
			shared_ptr<vector<complex<T>>> inner = make_shared<vector<complex<T>>>();
			for(int k = 0; k < dim_2; k++)	
			{
				if((Axis1 == 0 && Axis2 == 2) || (Axis1 == 2 && Axis2 == 0))
					inner->push_back(RawData->at(k)->at(j)->at(i));
				if((Axis1 == 0 && Axis2 == 1) || (Axis1 == 1 && Axis2 == 0))
					inner->push_back(RawData->at(j)->at(i)->at(k));
				if((Axis1 == 1 && Axis2 == 2) || (Axis1 == 2 && Axis2 == 1))
					inner->push_back(RawData->at(i)->at(k)->at(j));
			}
			outter -> push_back(inner);
		}
		ret->push_back(outter);
	}
	return ret;
}




template<typename T, uint_fast32_t V>
shared_ptr<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>> FourierTransform<T, V>::forward_fft()
{
	shared_ptr<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>> ret = make_shared<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>>();
	for(int i = 0; i < RawData->size(); i++)
	{
		shared_ptr<vector<shared_ptr<vector<complex<T>>>>> dim_1 = make_shared<vector<shared_ptr<vector<complex<T>>>>>();
		for(int j = 0; j < RawData->at(i)->size(); j++)
		{
			shared_ptr<vector<complex<T>>> ori_dim_2 = slice_vector(RawData->at(i)->at(j));
			shared_ptr<vector<complex<T>>> dim_2 = make_shared<vector<complex<T>>>();
			Eigen:FFT<T> fft;
			fft.fwd(*dim_2, *ori_dim_2);
			dim_1->push_back(dim_2);
		}
		ret->push_back(dim_1);
	}
	return ret;
}





template<typename T, uint_fast32_t V>
shared_ptr<vector<complex<T>>> FourierTransform<T, V>::forward_fft_single(shared_ptr<vector<complex<T>>> time_domain)
{
	shared_ptr<vector<complex<T>>> freq = make_shared<vector<complex<T>>>();
	Eigen::FFT<T> engine;
	engine.fwd((*freq), (*time_domain));
	return freq;
}




template<typename T, uint_fast32_t V>
shared_ptr<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>> FourierTransform<T, V>::fft_shift()
{
	int samples = RawData->at(0)->at(0)->size();
	int rows = RawData->size();
	int cols = RawData->at(0)->size();
	shared_ptr<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>> ptr = make_shared<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>>();

	for(int i = 0; i < rows; i++)
	{
		shared_ptr<vector<shared_ptr<vector<complex<T>>>>> collector = make_shared<vector<shared_ptr<vector<complex<T>>>>>();
		uint_fast32_t r_idx = (i + rows/2)%rows;
		for(int j = 0; j < cols; j++)
		{
			shared_ptr<vector<complex<T>>> temp = make_shared<vector<complex<T>>>();
			uint_fast32_t c_idx = (j + cols/2)%cols;
			for(int s = 0; s < samples; s++)
			{
				temp->push_back(RawData->at(r_idx)->at(c_idx)->at(s));
			}
			collector->push_back(temp);
		}
		ptr->push_back(collector);
	}
	return ptr;
}



template<typename T, uint_fast32_t V>
shared_ptr<vector<complex<T>>> FourierTransform<T, V>::getRange()
{
	return Range;
}



template<typename T, uint_fast32_t V>
shared_ptr<vector<shared_ptr<vector<shared_ptr<vector<complex<T>>>>>>> FourierTransform<T, V>::getRawData()
{
	return RawData;
}


// // Explicitly defines templated instances
template class ArcaWave::Radar::FourierTransform<float, 3>;
template class ArcaWave::Radar::FourierTransform<double, 3>;