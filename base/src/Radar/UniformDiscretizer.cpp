
#include "Radar/UniformDiscretizer.h"

// STL includes
#include <queue>

// ArcaWave includes
#include "Utility/Constants.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Geometry;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
UniformDiscretizer<T, V>::UniformDiscretizer()
{}

template<typename T, uint_fast32_t V>
UniformDiscretizer<T, V>::~UniformDiscretizer()
{}

template<typename T, uint_fast32_t V>
shared_ptr<DiscretizedFace<T, V>> UniformDiscretizer<T, V>::DiscretizeFace(const shared_ptr<DiscretizeFaceInfo<T, V>>& Info) const
{
    // Creates the discretized face 
    shared_ptr<DiscretizedFace<T, V>> disc_face = make_shared<DiscretizedFace<T, V>>();

    // Creates the queue of triangles to process for tesselation
    queue<Triangle<T, V>> q;
    q.emplace(Info->Face.Vertices, Info->Face.Normals);

    // Tesselate each triangle in the queue until it is small enough
    while(!q.empty())
    {
        // Get the next triangle to tesselate
        const Triangle<T, V> t = q.front();
        q.pop();

        // If the sides of the triangle are too big, tesselate it further
        const Matrix<T, V, 1> sides = t.Sides();
        if(sides.x() > Info->Resolution || sides.y() > Info->Resolution || sides.z() > Info->Resolution)
        {
            // Bisect each side to get its midpoint
            Matrix<T, V, 1> ab = (t.Vertices.A + t.Vertices.B)/2;
            Matrix<T, V, 1> bc = (t.Vertices.B + t.Vertices.C)/2;
            Matrix<T, V, 1> ca = (t.Vertices.C + t.Vertices.A)/2;

            // Defines the normals of each midpoint
            const Matrix<T, V, 1> n_ab = t.Normal(ab);
            const Matrix<T, V, 1> n_bc = t.Normal(bc);
            const Matrix<T, V, 1> n_ca = t.Normal(ca);

            // If we are interpolating the surface
            if(Info->Interpolate)
            {
                // Adjust the positon of the new points to extend the surface (avoiding divide by zero and the square root of a negative number)
                const T dotAB = t.Normals.A.dot(t.Normals.B);
                const T dotBC = t.Normals.B.dot(t.Normals.C);
                const T dotCA = t.Normals.C.dot(t.Normals.A);
                if((dotAB - static_cast<T>(1.0)) < -FLOATING_POINT_ERROR<T>)
                    ab += (n_ab - (t.Normals.A + t.Normals.B)/2) * sides.x() / sqrt(2 - 2*dotAB);
                if((dotBC - static_cast<T>(1.0)) < -FLOATING_POINT_ERROR<T>)
                    bc += (n_bc - (t.Normals.B + t.Normals.C)/2) * sides.y() / sqrt(2 - 2*dotBC);
                if((dotCA - static_cast<T>(1.0)) < -FLOATING_POINT_ERROR<T>)
                    ca += (n_ca - (t.Normals.C + t.Normals.A)/2) * sides.z() / sqrt(2 - 2*dotCA);
            }

            // Adds the new triangles to the queue
            q.emplace(Triad<T, V>({t.Vertices.A, ab, ca}), Triad<T, V>({t.Normals.A, n_ab, n_ca}));
            q.emplace(Triad<T, V>({t.Vertices.B, bc, ab}), Triad<T, V>({t.Normals.B, n_bc, n_ab}));
            q.emplace(Triad<T, V>({t.Vertices.C, ca, bc}), Triad<T, V>({t.Normals.C, n_ca, n_bc}));
            q.emplace(Triad<T, V>({ab, bc, ca}), Triad<T, V>({n_ab, n_bc, n_ca}));
        }
        else // Otherwise, add the center vertex, its normal, and area to the discretized face
        {
            // Gets the center vertex, normal, and area of the triangle
            const Eigen::Matrix<T, V, 1> center = t.Center();
            const Eigen::Matrix<T, V, 1> norm = t.Normal(center);
            const T area = t.Area();

            // Adds the center vertex, normal, and area to the discretized face
            disc_face->VertexCount += 1;
            disc_face->Vertices.push_back(center);
            disc_face->Normals.push_back(norm);
            disc_face->Areas.push_back(area);
        }
    }

    // Returns the pointer to the discretized face
    return disc_face;
}

template<typename T, uint_fast32_t V>
shared_ptr<DiscretizedMesh<T, V>> UniformDiscretizer<T, V>::DiscretizeMesh(const shared_ptr<DiscretizeMeshInfo<T, V>>& Info) const
{
    // Creates the discretized surface
    shared_ptr<DiscretizedMesh<T, V>> disc_mesh = make_shared<DiscretizedMesh<T, V>>();
    disc_mesh->FaceCount = Info->Msh->GetFaceCount();
    disc_mesh->Msh = Info->Msh;
    disc_mesh->Faces = shared_ptr<shared_ptr<DiscretizedFace<T, V>>[]>(new shared_ptr<DiscretizedFace<T, V>>[disc_mesh->FaceCount]);
    for(uint_fast32_t i = 0; i < disc_mesh->FaceCount; ++i)
        disc_mesh->Faces[i].reset();

    // Discretizes each face of the mesh that is also included in the given submesh
    for(uint_fast32_t i = 0; i < Info->Sub->GetFaceCount(); ++i)
    {
        // Gets the needed triangle of the face
        const uint_fast32_t face = Info->Sub->GetFace(i);

        // Collects the information needed to discretize the face
        shared_ptr<DiscretizeFaceInfo<T, V>> face_info = make_shared<DiscretizeFaceInfo<T, V>>();
        face_info->Face = Info->Msh->GetTriangle(face);
        face_info->Resolution = Info->Resolution;
        face_info->Interpolate = Info->Interpolate;

        // Discretizes the face
        disc_mesh->Faces[face] = DiscretizeFace(face_info);
    }

    // Returns the pointer to the discretized surface
    return disc_mesh;
}

template class ArcaWave::Radar::UniformDiscretizer<float, 3>;
template class ArcaWave::Radar::UniformDiscretizer<double, 3>;
