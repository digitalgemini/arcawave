#include "Radar/Calculator/PO.h"

// ArcaWave include
#include "Utility/Constants.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
void PO<T, V>::AddScatter(const shared_ptr<DiscretizedFace<T, V>>& disc_face, const uint_fast32_t index)
{
    const Matrix<T, V, 1>& vert = disc_face->Vertices[index];
    const Matrix<complex<T>, V, 1> current = static_cast<T>(2)
        * static_cast<Matrix<complex<T>, V, 1>>(disc_face->Normals[index])
        .cross(this->IncWave.MagneticField(0, vert));
    const T wavenumber = static_cast<T>(2) * PI<T> * this->Frequency / C_0<T>;
    // Calculate as [theta * theta * J + phi * phi * J] (= r x J x r) to get result of same meaning as MECA
    this->Scatter += I<T> * Z_0<T> * wavenumber * this->RefUnit.cross(current).cross(this->RefUnit)
        * exp(-I<T> * wavenumber * this->RefUnit.dot(vert)) * disc_face->Areas[index] / (4 * PI<T>);
}


template class ArcaWave::Radar::PO<float, 3>;
template class ArcaWave::Radar::PO<double, 3>;
