#include "Radar/Calculator/MECA.h"

// ArcaWave include
#include "Utility/Constants.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
void MECA<T, V>::AddScatter(const shared_ptr<DiscretizedFace<T, V>>& disc_face, const uint_fast32_t index)
{
    const Matrix<T, V, 1>& vert = disc_face->Vertices[index];
    T angular_freq = 2 * PI<T> * this->Frequency;
    T lambda = C_0<T> / this->Frequency;

    // Calculate the surface current at that point approximated by MECA
    Matrix<complex<T>, V, 1> E_inc = this->IncWave.ElectricField(0, vert);

    // Unit vector calculation
    Matrix<T, V, 1> n_unit = disc_face->Normals[index];
    Matrix<T, V, 1> k_unit_inc = this->IncUnit;
    Matrix<T, V, 1> e_unit_TE = k_unit_inc.cross(n_unit) / k_unit_inc.cross(n_unit).norm();
    Matrix<T, V, 1> e_unit_TM = e_unit_TE.cross(k_unit_inc);

    // Cosine of angle of incidence
    T cos_theta_inc = -k_unit_inc.dot(n_unit) / (k_unit_inc.norm() * n_unit.norm());

    // Coordinate transformation
    complex<T> E_inc_TE = E_inc.dot(e_unit_TE);
    complex<T> E_inc_TM = E_inc.dot(e_unit_TM);

    // Reflection coefficients
    complex<T> R_TE, R_TM;
    if (this->Material.IsPEC())
    {
        // (1) PEC
        // Conductivity of object is infinite, default imported material, equal to PO
        R_TE = -1;
        R_TM = -1;
    }
    else
    {
        // (2) Non-metallic and metallic
        // The refractive index, wavenumber, impedance and angle are all complex number, 
        // the imaginary part describes attenuation
        const Geometry::Material<T> vacuum = Geometry::Material<T>();
        T permittivity_1 = vacuum.permittivity;
        T permeability_1 = vacuum.permeability;
        T conductivity_1 = vacuum.conductivity;
        // complex<T> impedance_1 = vacuum.GetImpedance(angular_freq);
        T permittivity_2 = this->Material.permittivity;
        T permeability_2 = this->Material.permeability;
        T conductivity_2 = this->Material.conductivity;
        // complex<T> impedance_2 = this->Material.GetImpedance(angular_freq);

        complex<T> k1 = vacuum.GetWaveNumber(angular_freq);
        complex<T> k2 = this->Material.GetWaveNumber(angular_freq);
        complex<T> k2_2 = k2 * k2;
        complex<T> k1_2 = k1 * k1;
        T sin_theta_inc_2 = 1 - cos_theta_inc * cos_theta_inc;
        complex<T> k_iw = k1 * cos_theta_inc;
        
        // MECA
        T real_refracted = sqrt(1.0 / 2 * (k2_2.real() - (k1_2 * sin_theta_inc_2).real() + 
                            sqrt(k2_2.real() - (k1_2 * sin_theta_inc_2).real() + k2_2.imag())));
        T imag_refracted = k2.imag() / (2 * real_refracted);
        complex<T> k_tw = real_refracted + I<T> * imag_refracted;

        R_TE = (permeability_2 * k_iw - permeability_1 * k_tw) / (permeability_2 * k_iw + permeability_1 * k_tw);
        R_TM = ((permittivity_2 - I<T> * conductivity_2 / angular_freq) * k_iw - (permittivity_1 - I<T> * conductivity_1 / angular_freq) * k_tw)
            / ((permittivity_2 - I<T> * conductivity_2 / angular_freq) * k_iw + (permittivity_1 - I<T> * conductivity_1 / angular_freq) * k_tw);
        
        // Extended MECA
        // T ex_real_refracted = sqrt(1.0 / 2 * (k2_2.real() - (k1_2 * sin_theta_inc_2).real() + 
        //                     sqrt(pow((k2_2.real() - (k1_2 * sin_theta_inc_2).real()), 2) +
        //                         pow((k2_2.imag() - (k1_2 * sin_theta_inc_2).imag()), 2))));
        // T ex_imag_refracted = sqrt(1.0 / 2 * (-k2_2.real() + (k1_2 * sin_theta_inc_2).real() + 
        //                     sqrt(pow((k2_2.real() - (k1_2 * sin_theta_inc_2).real()),2) +
        //                         pow((k2_2.imag() - (k1_2 * sin_theta_inc_2).imag()), 2))));
        // complex<T> k_tw = ex_real_refracted + I<T> * ex_imag_refracted;
    }

    // Calculate electric and magnetic current densities
    Matrix<complex<T>, V, 1> J_MECA =
     (E_inc_TE * cos_theta_inc * (complex<T>(1) - R_TE) * e_unit_TE
     + E_inc_TM * (complex<T>(1) - R_TM) * (n_unit.cross(e_unit_TE)))
     / Z_0<T>;

    Matrix<complex<T>, V, 1> M_MECA =
     (E_inc_TE * (complex<T>(1) + R_TE) * (e_unit_TE.cross(n_unit))
     + E_inc_TM * cos_theta_inc * (complex<T>(1) + R_TM) * e_unit_TE);
    
    // Compute the scatter field contribution of the point and add it to the integration sum
    T G_phase = -2 * PI<T> / lambda * this->RefUnit.dot(vert);
    complex<T> G_e = cos(G_phase) + I<T> * sin(G_phase);
    this->Scatter += I<T> / (complex<T>(2) * lambda) * (this->RefUnit.cross(M_MECA) - Z_0<T> * this->RefUnit.cross(J_MECA).cross(this->RefUnit))
        * G_e * disc_face->Areas[index];
}

template class ArcaWave::Radar::MECA<float, 3>;
template class ArcaWave::Radar::MECA<double, 3>;
