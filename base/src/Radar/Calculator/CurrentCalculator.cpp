#include "Radar/Calculator/CurrentCalculator.h"

// ArcaWave include
#include "Utility/Constants.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
CurrentCalculator<T, V>::CurrentCalculator()
{}

template<typename T, uint_fast32_t V>
CurrentCalculator<T, V>::~CurrentCalculator()
{}

template<typename T, uint_fast32_t V>
void CurrentCalculator<T, V>::SetIncident(const Matrix<T, V, 1>& IncUnit, T Frequency, const Matrix<complex<T>, 2, 1>& Polarization)
{
    this->IncUnit = IncUnit;
    this->Frequency = Frequency;
    // Create planewave with single frequency. The source of planewave not really affects result, so set it to origin
    IncWave = PlaneWave<T, V>(IncUnit, Polarization, Matrix<T, V, 1>(0, 0, 0), Frequency, 0);
}

template<typename T, uint_fast32_t V>
void CurrentCalculator<T, V>::SetReflected(const Matrix<T, V, 1>& RefUnit)
{
    this->RefUnit = RefUnit;
    // Initialize scatter field for this IncUnit and RefUnit
    Scatter = Matrix<complex<T>, V, 1>(0, 0, 0);
}

template<typename T, uint_fast32_t V>
void CurrentCalculator<T, V>::SetMaterial(const Geometry::Material<T>& Material)
{
    this->Material = Material;
}

template<typename T, uint_fast32_t V>
void CurrentCalculator<T, V>::AddScatter(const shared_ptr<DiscretizedFace<T, V>>& disc_face, const uint_fast32_t index)
{}

template<typename T, uint_fast32_t V>
Matrix<complex<T>, V, 1> CurrentCalculator<T, V>::GetScatter() const
{
    return Scatter;
}


template class ArcaWave::Radar::CurrentCalculator<float, 3>;
template class ArcaWave::Radar::CurrentCalculator<double, 3>;
