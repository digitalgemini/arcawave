
#include "Geometry/Mesh.h"

// STL includes
#include <iostream>

// ArcaWave includes
#include "Utility/Constants.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Geometry;

Submesh::Submesh() :
    Faces()
{}

Submesh::~Submesh()
{}

void Submesh::AddFace(uint_fast32_t Face)
{
    // If the faces is empty, to push it to the end, otherwise insert the face in sorted order
    if(Faces.empty())
        Faces.push_back(Face);
    else
    {
        uint_fast32_t idx = GetIndex(Face);
        if(Faces[idx] != Face)
            Faces.insert(Faces.begin() + idx, Face);
    }
}

bool Submesh::ContainsFace(uint_fast32_t Face) const
{
    // Handle if the submesh is empty
    if(Faces.empty())
        return false;

    // Return true if the face could be found in the submesh
    uint_fast32_t idx = GetIndex(Face);
    return (Faces[idx] == Face);
}

uint_fast32_t Submesh::GetFaceCount() const
{
    return Faces.size();
}

uint_fast32_t Submesh::GetFace(uint_fast32_t Index) const
{
    return Faces[Index];
}

bool Submesh::IsEmpty() const
{
    return Faces.empty();
}

void Submesh::RemoveFace(uint_fast32_t Face)
{
    // If the list of faces is empty, just exit
    if(Faces.empty())
        return;

    // Otherwise if the face exists within the submesh, remove it
    uint_fast32_t idx = GetIndex(Face);
    if(Faces[idx] == Face)
        Faces.erase(Faces.begin() + idx);
}

uint_fast32_t Submesh::operator[](uint_fast32_t Index) const
{
    return Faces[Index];
}

uint_fast32_t Submesh::GetIndex(uint_fast32_t Face) const
{
    // Initializes the boundaries of the list on which to search
    uint_fast32_t begin = 0;
    uint_fast32_t end = Faces.size();
    uint_fast32_t mid = 0;

    // Searches the list until a viable index if found
    while((end - begin) > 0)
    {
        // Update the middle of the range and check if the face is found, lower, or higher than the middle
        mid = (end + begin) / 2;
        if(Faces[mid] == Face)
            break;
        else if(Faces[mid] < Face)
            begin = mid + 1;
        else
            end = mid;
    }

    // Returns the best found index
    return mid;
}

template<typename T, uint_fast32_t V>
Mesh<T, V>::Mesh(const shared_ptr<MeshCreateInfo<T, V>>& Info) :
    VertexCount(Info->VertexCount), Vertices(Info->Vertices), Normals(Info->Normals), FaceCount(Info->FaceCount),
    Faces(Info->Faces), MaterialInfo(Info->MaterialInfo), BoundingBox(Info->BoundingBox)
{}

template<typename T, uint_fast32_t V>
Mesh<T, V>::~Mesh()
{}

template<typename T, uint_fast32_t V>
uint_fast32_t Mesh<T, V>::GetVertexCount() const
{
    return VertexCount;
}

template<typename T, uint_fast32_t V>
Matrix<T, V, 1> Mesh<T, V>::GetVertex(const uint_fast32_t VertexIndex) const
{
    return Vertices[VertexIndex];
}

template<typename T, uint_fast32_t V>
Matrix<T, V, 1> Mesh<T, V>::GetNormal(const uint_fast32_t VertexIndex) const
{
    return Normals[VertexIndex];
}

template<typename T, uint_fast32_t V>
uint_fast32_t Mesh<T, V>::GetFaceCount() const
{
    return FaceCount;
}

template<typename T, uint_fast32_t V>
MeshFace Mesh<T, V>::GetFace(const uint_fast32_t FaceIndex) const
{
    return Faces[FaceIndex];
}

template<typename T, uint_fast32_t V>
Triangle<T, V> Mesh<T, V>::GetTriangle(const uint_fast32_t FaceIndex) const
{
    const MeshFace f = Faces[FaceIndex];
    return Triangle<T, V>({Vertices[f.A], Vertices[f.B], Vertices[f.C]}, {Normals[f.A], Normals[f.B], Normals[f.C]});
}

template<typename T, uint_fast32_t V>
AABB<T, V> Mesh<T, V>::GetBoundingBox() const
{
	return BoundingBox;
}

template<typename T, uint_fast32_t V>
void Mesh<T, V>::SetBoundingBox(const AABB<T, V>& BBox)
{
	BoundingBox = BBox;
}

template<typename T, uint_fast32_t V>
Material<T> Mesh<T, V>::GetMaterial() const
{
    return MaterialInfo;
}

template<typename T, uint_fast32_t V>
shared_ptr<Submesh> Mesh<T, V>::GetFullSubmesh() const
{
    // Creates the submesh and returns a pointer to it
    shared_ptr<Submesh> sub = make_shared<Submesh>();
    for(uint_fast32_t i = 0; i < this->FaceCount; ++i)
        sub->AddFace(i);
    return sub;
}

template<typename T, uint_fast32_t V>
shared_ptr<Mesh<T, V>> Mesh<T, V>::BuildSubmesh(const shared_ptr<Submesh>& Sub) const
{
    // Creates lists of the new vertices, normals, and faces
    vector<Matrix<T, V, 1>> verts;
    vector<Matrix<T, V, 1>> norms;
    vector<MeshFace> faces;

    // Creates an index map for transitioning vertices and normals to the new mesh
    vector<uint32_t> i_map(this->GetVertexCount(), -1);

    // Goes through each face in the submesh
    uint_fast32_t index = 0;
    for(uint_fast32_t i = 0; i < Sub->GetFaceCount(); ++i)
    {
        // Gets the face of the mesh
        uint_fast32_t f_index = Sub->GetFace(i);
        MeshFace f = this->GetFace(f_index);

        // Maps the vertices into the new mesh
        if(i_map[f.A] == -1)
        {
            verts.push_back(this->GetVertex(f.A));
            norms.push_back(this->GetNormal(f.A));
            i_map[f.A] = index++;
        }
        if(i_map[f.B] == -1)
        {
            verts.push_back(this->GetVertex(f.B));
            norms.push_back(this->GetNormal(f.B));
            i_map[f.B] = index++;
        }
        if(i_map[f.C] == -1)
        {
            verts.push_back(this->GetVertex(f.C));
            norms.push_back(this->GetNormal(f.C));
            i_map[f.C] = index++;
        }

        // Adds the face to the new mesh
        faces.push_back({i_map[f.A], i_map[f.B], i_map[f.C]});
    }

    // Makes the creation info structure
    shared_ptr<MeshCreateInfo<T, V>> create_info = make_shared<MeshCreateInfo<T, V>>();
    create_info->VertexCount = verts.size();
    create_info->Vertices = shared_ptr<Matrix<T, V, 1>[]>(new Matrix<T, V, 1>[verts.size()]);
    create_info->Normals = shared_ptr<Matrix<T, V, 1>[]>(new Matrix<T, V, 1>[verts.size()]);
    for(uint_fast32_t i = 0; i < verts.size(); ++i)
    {
        create_info->Vertices[i] = verts[i];
        create_info->Normals[i] = norms[i];
    }
    create_info->FaceCount = faces.size();
    create_info->Faces = shared_ptr<MeshFace[]>(new MeshFace[faces.size()]);
    for(uint_fast32_t i = 0; i < faces.size(); ++i)
        create_info->Faces[i] = faces[i];
    create_info->MaterialInfo = this->MaterialInfo;

    // Creates the new mesh and returns the pointer to it.
    return make_shared<Mesh<T, V>>(create_info);
}

template class ArcaWave::Geometry::Mesh<float, 3>;
template class ArcaWave::Geometry::Mesh<double, 3>;
