
#include "Geometry/Object.h"

// STL incldues
#include <fstream>
#include <iostream>
#include <string>

// Assimp Includes (ignores warnings)
#ifdef __clang__
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Weverything"
#elif __GNUG__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wall"
#endif
#include <assimp/Exporter.hpp>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#ifdef __clang__
    #pragma clang diagnostic pop
#elif __GNUG__
    #pragma GCC diagnostic pop
#endif

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Geometry;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
Object<T, V>::Object(const shared_ptr<ObjectCreateInfo<T, V>>& Info) :
    ObjectName(Info->ObjectName), MeshCount(Info->MeshCount), Meshes(Info->Meshes), BoundingBox(Info->BoundingBox)
{} 

template<typename T, uint_fast32_t V>
Object<T, V>::~Object()
{}

template<typename T, uint_fast32_t V>
shared_ptr<Object<T, V>> Object<T, V>::BuildSubObject(const shared_ptr<shared_ptr<Submesh>[]>& Submeshes) const
{
    // Creates the information struct necessary for creating the new object
    shared_ptr<ObjectCreateInfo<T, V>> create_info = make_shared<ObjectCreateInfo<T, V>>();
    create_info->ObjectName = this->ObjectName;
    create_info->MeshCount = this->MeshCount;
    create_info->Meshes = shared_ptr<shared_ptr<Mesh<T, V>>[]>(new shared_ptr<Mesh<T, V>>[this->MeshCount]);
    for(uint_fast32_t i = 0; i < this->MeshCount; ++i)
        create_info->Meshes[i] = this->Meshes[i]->BuildSubmesh(Submeshes[i]);

    // Creates the new object and returns it
    return make_shared<Object<T, V>>(create_info);
}

template<typename T, uint_fast32_t V>
uint_fast32_t Object<T, V>::GetMeshCount() const
{
    return MeshCount;
}

template<typename T, uint_fast32_t V>
shared_ptr<Mesh<T, V>> Object<T, V>::GetMesh(const uint_fast32_t MeshIndex) const
{
    return Meshes[MeshIndex];
}

template<typename T, uint_fast32_t V>
string Object<T, V>::GetName() const
{
    return ObjectName;
}

template<typename T, uint_fast32_t V>
void Object<T, V>::Export(const shared_ptr<Object<T, V>>& Obj, const std::string& Filename, const ExportFileType Type)
{
    // Creates the node that represents the object
    aiNode* node            = new aiNode();
    node->mName             = aiString(Obj->ObjectName);
    node->mNumChildren      = 0;
    node->mNumMeshes        = static_cast<unsigned int>(Obj->MeshCount);
    node->mMeshes           = new unsigned int[Obj->MeshCount];
    for(uint_fast32_t i = 0; i < Obj->MeshCount; ++i)
        node->mMeshes[i] = static_cast<unsigned int>(i);

    // Creates the scene
    aiScene* scene          = new aiScene();
    scene->mRootNode        = node;
    scene->mNumMeshes       = static_cast<unsigned int>(Obj->MeshCount);
    scene->mMeshes          = new aiMesh*[Obj->MeshCount];
    scene->mNumMaterials    = static_cast<unsigned int>(Obj->MeshCount);
    scene->mMaterials       = new aiMaterial*[Obj->MeshCount];

    // Creates a new material for each mesh
    for(uint_fast32_t i = 0; i < Obj->MeshCount; ++i)
    {
        // Gets the mesh
        shared_ptr<Mesh<T, V>> mesh = Obj->Meshes[i];
        Color mat_col = mesh->MaterialInfo.MaterialColor;

        // Creates the material and places it in the list
        aiMaterial* mat = new aiMaterial();
        aiColor3D* ambient = new aiColor3D(1.0, 1.0, 1.0);
        aiColor3D* diffuse = new aiColor3D(mat_col.R, mat_col.G, mat_col.B);
        mat->AddProperty<aiColor3D>(ambient, 3, AI_MATKEY_COLOR_AMBIENT);
        mat->AddProperty<aiColor3D>(diffuse, 3, AI_MATKEY_COLOR_DIFFUSE);
        scene->mMaterials[i] = mat;
    }

    // Creates a mesh for each mesh in the object
    for(uint_fast32_t i = 0; i < Obj->MeshCount; ++i)
    {
        // Gets the mesh to export
        shared_ptr<Mesh<T, V>> mesh = Obj->Meshes[i];

        // Creates the mesh in assimp
        aiMesh* assimp_mesh = new aiMesh();
        assimp_mesh->mMaterialIndex = static_cast<unsigned int>(i);

        // Adds the vertices and normals of the mesh
        assimp_mesh->mNumVertices = mesh->GetVertexCount();
        assimp_mesh->mVertices = new aiVector3D[mesh->GetVertexCount()];
        assimp_mesh->mNormals = new aiVector3D[mesh->GetVertexCount()];
        for(uint_fast32_t j = 0; j < mesh->GetVertexCount(); ++j)
        {
            const Matrix<T, V, 1> vertex = mesh->GetVertex(j);
            const Matrix<T, V, 1> normal = mesh->GetNormal(j);
            assimp_mesh->mVertices[j] = aiVector3D(vertex.x(), vertex.y(), vertex.z());
            assimp_mesh->mNormals[j] = aiVector3D(normal.x(), normal.y(), normal.z());
        }

        // Adds the faces of the mesh
        assimp_mesh->mNumFaces = mesh->GetFaceCount();
        assimp_mesh->mFaces = new aiFace[mesh->GetFaceCount()];
        for(uint_fast32_t j = 0; j < mesh->GetFaceCount(); ++j)
        {
            const MeshFace face = mesh->GetFace(j);
            aiFace assimp_face;
            assimp_face.mNumIndices = 3;
            assimp_face.mIndices = new unsigned int[3]
            {
                static_cast<unsigned int>(face.A), 
                static_cast<unsigned int>(face.B), 
                static_cast<unsigned int>(face.C)
            };
            assimp_mesh->mFaces[j] = assimp_face;
        }

        // Adds the mesh to the scene
        scene->mMeshes[i] = assimp_mesh;
    }

    // Exports the file, and exits if it did not export correctly
    Assimp::Exporter exporter;
    const aiExportFormatDesc* desc = exporter.GetExportFormatDescription(Type);
    uint_fast32_t flags = aiProcess_ConvertToLeftHanded;
    const string path = Filename;
    if(exporter.Export(scene, desc->id, path.c_str(), flags) != AI_SUCCESS)
    {
        cout << "Failed to export file" << endl;
        exit(1);
    }

    // Delete the scene and clean up memory
    delete scene;
}

template<typename T, uint_fast32_t V>
shared_ptr<Object<T, V>> Object<T, V>::Import(const std::string& Filename, T MaxSmoothingAngle)
{
    return make_shared<Object<T, V>>(BuildObjectCreateInfo(Filename, MaxSmoothingAngle));
}

template<typename T, uint_fast32_t V>
AABB<T,V> Object<T, V>::GetBoundingBox() const
{
	return BoundingBox;
}

template<typename T, uint_fast32_t V>
void Object<T, V>::SetBoundingBox(const AABB<T, V>& BBox)
{
	BoundingBox = BBox;
}

template<typename T, uint_fast32_t V>
shared_ptr<ObjectCreateInfo<T, V>> Object<T, V>::BuildObjectCreateInfo(const std::string& Filename, const T MaxSmoothingAngle)
{
	// Defines the importer and sets its properties
	Assimp::Importer importer;
	uint_fast32_t flags = aiProcess_ConvertToLeftHanded | aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_GenBoundingBoxes;

	// If we want to smooth the angles of the mesh, adjust the importer
	if(MaxSmoothingAngle != 0.0f)
	{
		importer.SetPropertyFloat(AI_CONFIG_PP_GSN_MAX_SMOOTHING_ANGLE, MaxSmoothingAngle);
		flags |= aiProcess_GenSmoothNormals;
	}

	// Loads the scene and checks that the importer read the file correctly
	const aiScene* scene = importer.ReadFile(Filename.c_str(), flags);
	if(!scene)
	{
		cout << "Failed to import file " << Filename << endl;
		exit(1);
	}

	//Intialize object scope bounding box
	auto object_aabb = AABB<T, V>();

	// Imports each mesh of the object into the object
	vector<shared_ptr<Mesh<T, V>>> meshes(scene->mNumMeshes);
	// cout << "total meshes = " << scene->mNumMeshes << endl;
	// Initializes the information struct necessary for creating the object
	shared_ptr<ObjectCreateInfo<T, V>> obj_info = make_shared<ObjectCreateInfo<T, V>>();
	obj_info->ObjectName = std::string(scene->mRootNode->mName.data, scene->mRootNode->mName.data + scene->mRootNode->mName.length-4);
	obj_info->MeshCount = scene->mNumMeshes;
	obj_info->Meshes = shared_ptr<shared_ptr<Mesh<T, V>>[]>(new shared_ptr<Mesh<T, V>>[scene->mNumMeshes]);

	// Imports each mesh of the object into the object
	for(uint_fast32_t i = 0; i < scene->mNumMeshes; ++i)
	{
		aiMesh* mesh = scene->mMeshes[i];
		vector<Matrix<T, V, 1>> verts(mesh->mNumVertices);
		vector<Matrix<T, V, 1>> norms(mesh->mNumVertices);
		vector<MeshFace> faces(mesh->mNumFaces);

		// Loads the vertices and normals into the mesh
		for(uint_fast32_t j = 0; j < mesh->mNumVertices; ++j)
		{
			aiVector3D vert = mesh->mVertices[j];
			aiVector3D norm = mesh->mNormals[j];

			verts[j] = Matrix<T, V, 1>(vert.x, vert.y, vert.z);
			norms[j] = Matrix<T, V, 1>(norm.x, norm.y, norm.z);
		}

		// Loads the faces into the mesh
		for(int j = 0; j < mesh->mNumFaces; ++j)
		{
			aiFace face = mesh->mFaces[j];
			faces[j] = {static_cast<uint32_t>(face.mIndices[0]), static_cast<uint32_t>(face.mIndices[1]), static_cast<uint32_t>(face.mIndices[2])};
		}

		// Makes the creation info structure
		shared_ptr<MeshCreateInfo<T, V>> mesh_info = make_shared<MeshCreateInfo<T, V>>();
		mesh_info->VertexCount = verts.size();
		mesh_info->Vertices = shared_ptr<Matrix<T, V, 1>[]>(new Matrix<T, V, 1>[verts.size()]);
		mesh_info->Normals = shared_ptr<Matrix<T, V, 1>[]>(new Matrix<T, V, 1>[verts.size()]);
		for(uint_fast32_t i = 0; i < verts.size(); ++i)
		{
			mesh_info->Vertices[i] = verts[i];
			mesh_info->Normals[i] = norms[i];
		}
		mesh_info->FaceCount = faces.size();
		mesh_info->Faces = shared_ptr<MeshFace[]>(new MeshFace[faces.size()]);
		for(uint_fast32_t i = 0; i < faces.size(); ++i)
			mesh_info->Faces[i] = faces[i];

        // Loads the Material for the mesh
        aiMaterial* mat = scene->mMaterials[mesh->mMaterialIndex];
        aiColor3D color;
        mat->Get(AI_MATKEY_COLOR_SPECULAR, color);
        mesh_info->MaterialInfo = Material<T>(color.r, color.g, color.b);

		// Loads the AABB for the mesh
		Matrix<T, V, 1> maxAABB = {mesh->mAABB.mMax.x, mesh->mAABB.mMax.y, mesh->mAABB.mMax.z};
		Matrix<T, V, 1> minAABB = {mesh->mAABB.mMin.x, mesh->mAABB.mMin.y, mesh->mAABB.mMin.z};
		mesh_info->BoundingBox = AABB<T, V>{maxAABB, minAABB};

		// Updates the AABB for the object
		obj_info->BoundingBox = obj_info->BoundingBox.Union(mesh_info->BoundingBox);

		// Creates the new mesh from the vertices and faces
		obj_info->Meshes[i] = make_shared<Mesh<T ,V>>(mesh_info);
	}
	return obj_info;
}

template class ArcaWave::Geometry::Object<float, 3>;
template class ArcaWave::Geometry::Object<double, 3>;
