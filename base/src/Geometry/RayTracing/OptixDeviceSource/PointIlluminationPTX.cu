// STL includes
#include <math.h>

// OPTIX includes
#include <optix_device.h>
#include <sutil/vec_math.h>
#include <curand_kernel.h>

// ArcaWave includes
#include "PipelineConfig.h"
#include "Utility/Constants.h"
#include "Geometry/RayTracing/OptixRayTracer.cuh"
#include "Radar/StochasticVCM.cuh"


using namespace ArcaWave::Geometry;

/**
 * @brief optixLaunch puts parameters into shared memory
 */
extern "C" __constant__ ArcaWave::Geometry::LaunchParameters<T, V> params;


// BEGIN HELPERS //
//@TODO Helpers need to be moved to a separate file for device side linking
static __forceinline__ __device__
void *unpackPointer( uint32_t i0, uint32_t i1 )
{
	const uint64_t uptr = static_cast<uint64_t>( i0 ) << 32 | i1;
	void*           ptr = reinterpret_cast<void*>( uptr );
	return ptr;
}

static __forceinline__ __device__
void packPointer( void* ptr, uint32_t& i0, uint32_t& i1 )
{
	const uint64_t uptr = reinterpret_cast<uint64_t>( ptr );
	i0 = uptr >> 32;
	i1 = uptr & 0x00000000ffffffff;
}


template<typename T>
static __forceinline__ __device__ T *getPRD()
{
	const uint32_t u0 = optixGetPayload_0();
	const uint32_t u1 = optixGetPayload_1();
	return reinterpret_cast<T*>( unpackPointer( u0, u1 ) );
}

// END HELPERS //

/**
 * @brief Generate rays from the perspective of the TX antenna
 * @TODO use z index to account for multiple TX
 * @TODO file/parameter antenna pattern (Launches unit hemisphere for now)
 */
extern "C" __global__ void __raygen__tx_project()
{
	// calculate position of current thread
	const uint3 idx = optixGetLaunchIndex();
    const uint32_t idx_lin = (idx.y * params.NumRaysX) + idx.x;
    const uint32_t num_rays = params.NumRaysX * params.NumRaysY;

    // set origin to TX position
    const float3 origin = make_float3(params.TXPosition.x(), params.TXPosition.y(), params.TXPosition.z());

    // distribute over unit hemisphere using Fibonacci spherical distribution
	const float gr=(sqrt(5.0) + 1.0) / 2.0;
	const float ga=(2.0 - gr) * (2* ArcaWave::Utility::PI<T>);
	const double lat = asin(-1.0 + 2.0 * double(idx_lin) / (num_rays+1));
	const double lon = ga * idx_lin;
	const double x = cos(lon)*cos(lat);
	const double y = sin(lon)*cos(lat);
	const double z = sin(lat);
	Eigen::Matrix<T, V, 1> rd = Matrix<T, V, 1>(abs(x), y, z).normalized();
	Eigen::Matrix<T, V, 1> rot_rd = params.TXOrientation * rd;
	auto ray_direction = normalize(make_float3(rot_rd.x(), rot_rd.y(), rot_rd.z()));

	// build payload struct
	ArcaWave::Geometry::ProjectedRay<T, V> hit_info[MAX_BOUNCE];
	memset(hit_info, 0, sizeof(ArcaWave::Geometry::ProjectedRay<T, V>) * (MAX_BOUNCE));

	// stack pointer to payload struct packed into ints
	uint32_t u0, u1;
	uint32_t bounce_count = 0;
	packPointer(hit_info, u0, u1);

	// launch the trace
	optixTrace(params.Handle, origin, ray_direction, 0.0f, 1e16f, 0.0f,
               OptixVisibilityMask(1),
               OPTIX_RAY_FLAG_NONE,
               TX_RAY_TYPE, RAY_TYPE_COUNT, TX_RAY_TYPE,
               u0, u1, bounce_count
	);

	// write result to buffer
	for (uint32_t i = 0; i < MAX_BOUNCE; ++i) params.PerRayData[idx_lin + (i*num_rays) ] = hit_info[i];
}

/**
 * @brief invoked when a tx ray misses
 */
extern "C" __global__ void __miss__tx_illuminate()
{
	// if tx ray didn't hit anything, its a miss
    getPRD<ArcaWave::Geometry::ProjectedRay<T, V>>()[optixGetPayload_2()].Contact = false;
}

/**
 * @brief invoked when a tx ray hits geometry
 */
extern "C" __global__ void __closesthit__tx_illuminate()
{
	// Get SBT Data
    const auto hit_obj = reinterpret_cast<ArcaWave::Radar::DeviceRadarObject<T, V>*>(optixGetSbtDataPointer());
    // calculate position of current thread
    const uint3 idx = optixGetLaunchIndex();
    const uint32_t idx_lin = (idx.y * params.NumRaysX) + idx.x;
    // Get ray parameters
    const float3 rayOrig = optixGetWorldRayOrigin();
	const float3 rayDir = optixGetWorldRayDirection();
	const float  rayT = optixGetRayTmax();
	
	// Gets the transformation from object to world and angle around y-axis
	float objToWorld[12] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	optixGetObjectToWorldTransformMatrix(objToWorld);
	float alpha = acos(objToWorld[0]);
	if(objToWorld[2] < 0)
		alpha = -alpha;

    // compute location of hit, and direction back to MONOSTATIC radar
    float3 hitPoint = rayOrig + (rayT * rayDir);
    float3 rx_dir = normalize(make_float3(params.TXPosition.x(), params.TXPosition.y(), params.TXPosition.z()) - hitPoint);

    float distance = length(rayT*rayDir);

    // compute surface normal:
    const int TriangleIndex = optixGetPrimitiveIndex();
	const int mesh_index = hit_obj->GetMeshIndex(TriangleIndex);
    auto part = hit_obj->DevicePartitions[mesh_index];
    int vcm_index = part.Index.Index;
    if (vcm_index >= hit_obj->VCMCount) vcm_index--;
	auto vcm = hit_obj->DeviceVCMSet[vcm_index];
    auto hit_info = getPRD<ArcaWave::Geometry::ProjectedRay<T, V>>();
	auto bounce_count = optixGetPayload_2();
	uint32_t u0, u1;
	packPointer( hit_info, u0, u1 );

	// check if RX is visible

    optixTrace(params.Handle, hitPoint, rx_dir, 1e-3f, 1e16f, 0.0f,
    	OptixVisibilityMask(1),
		OPTIX_RAY_FLAG_DISABLE_ANYHIT | OPTIX_RAY_FLAG_TERMINATE_ON_FIRST_HIT,
		RX_RAY_TYPE, RAY_TYPE_COUNT, RX_RAY_TYPE,
		u0, u1, bounce_count
	);

    // set payload struct value
	hit_info[bounce_count].Contact = true;
	hit_info[bounce_count].Dist = hit_info[ (1 - bounce_count == 1) ? 0 : (bounce_count - 1)].Dist + distance;
	hit_info[bounce_count].Point = {hitPoint.x, hitPoint.y, hitPoint.z};

	if(part.Enabled)
	{
		// Gets the incidence and reflection angles
		Matrix<T, 2, 1> inc_angles = vcm.ConvertToAngle(rayDir);
		Matrix<T, 2, 1> ref_angles = vcm.ConvertToAngle(rx_dir);

		// Rotates angles around x axis for object
		inc_angles.x() -= alpha;
		ref_angles.x() -= alpha;

		// Flips along x axis if needed.
		if(part.Index.XFlip)
		{
			inc_angles.x() = -inc_angles.x();
			ref_angles.x() = -ref_angles.x();
		}

		// Makes sure theta angles are in the correct interval
		while(inc_angles.x() > PI<T> || inc_angles.x() < -PI<T>)
			inc_angles.x() += (inc_angles.x() > 0 ? -2*PI<T> : 2*PI<T>);
		while(ref_angles.x() > PI<T> || ref_angles.x() < -PI<T>)
			ref_angles.x() += (ref_angles.x() > 0 ? -2*PI<T> : 2*PI<T>);

		// Gets the moment from the VCM

		hit_info[bounce_count].Amplitude = vcm.GetMoment(inc_angles, ref_angles);
	}
	else
		hit_info[bounce_count].Amplitude = Eigen::Matrix<thrust::complex<T>, V, 1>(0, 0, 0);

	//increment bounce count
	bounce_count++;
	// if not exceded max bounces, launch another TX ray
	if (bounce_count < MAX_BOUNCE)
	{

	}
}

extern "C" __global__ void __closesthit__rx_shadow()
{
    if (optixGetInstanceId() == 2)
    {
        auto hit_info = getPRD<ArcaWave::Geometry::ProjectedRay<T, V>>();
        hit_info[optixGetPayload_2()].RxVisible = true;
    }
}

extern "C" __global__ void __miss__rx_shadow()
{
    auto hit_info = getPRD<ArcaWave::Geometry::ProjectedRay<T, V>>();
	hit_info[optixGetPayload_2()].RxVisible = true;
}
