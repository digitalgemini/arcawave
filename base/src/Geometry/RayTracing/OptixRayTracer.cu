#include "Geometry/RayTracing/OptixRayTracer.cuh"
const uint32_t MAX_BOUNCE = 2;
// STL includes
#include <functional>
#include <iostream>
#include <chrono>

// LIB includes
#include <sutil/Exception.h>
#include <optix.h>
#include <optix_stubs.h>


using namespace std;
using namespace Eigen;
using namespace ArcaWave::Geometry;

extern "C" char PointIlluminationPTX[];

static void context_log_cb(unsigned int level, const char *tag, const char *message, void *)
{
    fprintf( stderr, "[%2d][%12s]: %s\n", (int)level, tag, message );
}

template<typename T, uint_fast32_t V>
OptixRayTracer<T, V>::OptixRayTracer(const shared_ptr<Environment<T, V>>& env) :
RayTracer<T, V>(env),
TargetDevice(cuda::device::get(0))
{
    // Init optix setup on device
    TargetDevice.make_current();
    TargetDevice.synchronize();
    CreateContext();

    //Load environment into optix acceleration structure
    LoadEnvironment();

    // Compile optix pipeline
    CreateModule();
    CreateRaygenProgram();
    CreateMissPrograms();
    CreateHitgroupPrograms();
    CreatePipeline();

    // Create the SBT
    BuildSBT();
}

template<typename T, uint_fast32_t V>
OptixRayTracer<T, V>::~OptixRayTracer()
{
    for (auto& buffer : GASOutputBuffers) cuda::memory::device::free(buffer);
    cuda::memory::device::free(IASOutputBuffer);
    cuda::memory::device::free(reinterpret_cast<void*>(SBT.raygenRecord));
    cuda::memory::device::free(reinterpret_cast<void*>(SBT.missRecordBase));
    cuda::memory::device::free(reinterpret_cast<void*>(SBT.hitgroupRecordBase));

    for(auto& pg : HitgroupPGs) (optixProgramGroupDestroy(pg));
    for(auto& pg : MissPGs) (optixProgramGroupDestroy(pg));
    for(auto& pg : RaygenPGs) (optixProgramGroupDestroy(pg));

    (optixPipelineDestroy(Pipeline));
    (optixModuleDestroy(Module));
    (optixDeviceContextDestroy(OptixContext));

    TargetDevice.reset();
}

template<typename T, uint_fast32_t V>
vector<ProjectedRay<T, V>> OptixRayTracer<T, V>::ProjectRays(const LaunchInfo<T, V>& Info)
{
    // get device buffer
    auto device_output = ProjectRaysDevice(Info);

    // copy results to hos
    auto rays = std::vector<ProjectedRay<T, V>>();
    rays.resize(Info.Width * Info.Height);
    cuda::memory::copy(rays.data(), reinterpret_cast<void*>(device_output), sizeof(ProjectedRay<T, V>) * Info.Width * Info.Height);

    // free device memory
    cuda::memory::device::free(device_output);

    return rays;
}

template<typename T, uint_fast32_t V>
void OptixRayTracer<T, V>::LoadEnvironment()
{
    // Load geometries into device
    for (const auto& object : RayTracer<T, V>::Env->GetObjects()) LoadGAS(object);

    // Load transforms into device
    for (const auto& instance : RayTracer<T, V>::Env->GetInstances()) LoadIAS(instance);
    BuildIASAccel(OPTIX_BUILD_OPERATION_BUILD);
}

template<typename T, uint_fast32_t V>
void OptixRayTracer<T, V>::LoadGAS(shared_ptr<Object<T, V>> Obj)
{
    // Init build input struct
    OptixBuildInput triangle_input = {};
    memset(&triangle_input, 0, sizeof(OptixBuildInput));
    triangle_input.type = OPTIX_BUILD_INPUT_TYPE_TRIANGLES;

    // Calculate total number of triangles across meshes and linearize into one buffer
     uint32_t num_triangles = 0;

    std::vector<Eigen::Matrix<float, V, 1>> cumulative_vertex;
    std::vector<MeshFace> cumulative_index;

    for (auto mesh_index = 0; mesh_index < Obj->GetMeshCount(); ++mesh_index)
    {
        const auto& mesh = Obj->GetMesh(mesh_index);

        for (auto face_index = 0; face_index < mesh->GetFaceCount(); ++face_index)
        {
            const auto& face = mesh->GetFace(face_index);

            cumulative_vertex.push_back( (mesh->GetVertex(face.A)).template cast<float>());
            cumulative_vertex.push_back(mesh->GetVertex(face.B).template cast<float>());
            cumulative_vertex.push_back(mesh->GetVertex(face.C).template cast<float>());

            cumulative_index.push_back({face.A + num_triangles,
                                        face.B + num_triangles,
                                        face.C + num_triangles});
        }

        num_triangles += mesh->GetFaceCount();
    }

    size_t vertex_size = sizeof(Eigen::Matrix<float, V, 1>);
    size_t index_size = sizeof(MeshFace);
    auto vertex_buffer = reinterpret_cast<Eigen::Matrix<T, V, 1>* >(TargetDevice.memory().allocate(vertex_size * cumulative_vertex.size()));
    auto index_buffer = reinterpret_cast<MeshFace* >(TargetDevice.memory().allocate(index_size * num_triangles));

    // copy vertex and index to device
    std::cout << "Copying " << Obj->GetName() << " with " << num_triangles << " faces to device " << TargetDevice.name()  << std::endl;
    cuda::memory::copy(vertex_buffer, cumulative_vertex.data(), vertex_size * cumulative_vertex.size());
    cuda::memory::copy(index_buffer, cumulative_index.data(), index_size * num_triangles);

    // construct input struct
    triangle_input.triangleArray.vertexFormat           = OPTIX_VERTEX_FORMAT_FLOAT3;
    triangle_input.triangleArray.vertexStrideInBytes    = vertex_size;
    triangle_input.triangleArray.numVertices            = cumulative_vertex.size();
    triangle_input.triangleArray.vertexBuffers          = reinterpret_cast<CUdeviceptr*>(&(vertex_buffer));

    triangle_input.triangleArray.indexFormat         = OPTIX_INDICES_FORMAT_UNSIGNED_INT3;
    triangle_input.triangleArray.indexStrideInBytes  = sizeof(MeshFace);
    triangle_input.triangleArray.numIndexTriplets    = cumulative_index.size();
    triangle_input.triangleArray.indexBuffer         = reinterpret_cast<CUdeviceptr>(index_buffer);

    uint32_t flags[1] = { OPTIX_GEOMETRY_FLAG_DISABLE_ANYHIT };
    triangle_input.triangleArray.flags = flags;

    triangle_input.triangleArray.numSbtRecords  = 1;
    triangle_input.triangleArray.sbtIndexOffsetBuffer = 0;
    triangle_input.triangleArray.sbtIndexOffsetSizeInBytes = 0;
    triangle_input.triangleArray.sbtIndexOffsetStrideInBytes = 0;

    // load into optix accel structure
    GASHandles.push_back(BuildGASAccel(triangle_input));

    // free device memory
    cuda::memory::device::free(vertex_buffer);
    cuda::memory::device::free(index_buffer);
}

template<typename T, uint_fast32_t V>
void OptixRayTracer<T, V>::LoadIAS(shared_ptr<Instance<T, V>> Inst)
{
    OptixInstance ias_input;
    ias_input.traversableHandle = GASHandles[Inst->GetObjectIndex()];
    ias_input.instanceId = Inst->GetInstanceIndex();
    ias_input.flags = OPTIX_INSTANCE_FLAG_DISABLE_ANYHIT;
    ias_input.sbtOffset = Inst->GetObjectIndex() * RAY_TYPE_COUNT;
    ias_input.visibilityMask = 1;
    // CONVERT TO optix transform
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            ias_input.transform[i * 4 + j] = Inst->GetTransformation().coeff(i, j);
        }
    }

    OptixInstances.push_back(ias_input);
}

template<typename T, uint_fast32_t V>
OptixTraversableHandle OptixRayTracer<T, V>::BuildGASAccel(OptixBuildInput &TriangleInput)
{
    OptixAccelBuildOptions accel_options = {};
    memset(&accel_options, 0, sizeof(OptixAccelBuildOptions));
    accel_options.buildFlags             = OPTIX_BUILD_FLAG_NONE;
    accel_options.operation              = OPTIX_BUILD_OPERATION_BUILD;

    // Compute required memory
    OptixAccelBufferSizes blas_buffer_size;
    OPTIX_CHECK(optixAccelComputeMemoryUsage(
        OptixContext,
        &accel_options, &TriangleInput, 1,
        &blas_buffer_size
    ));

    // allocate temporary buffer and output for build
    CUdeviceptr temp_build_buffer   = reinterpret_cast<CUdeviceptr>(TargetDevice.memory().allocate(blas_buffer_size.tempSizeInBytes));
    CUdeviceptr output_buffer       = reinterpret_cast<CUdeviceptr>(TargetDevice.memory().allocate(blas_buffer_size.outputSizeInBytes));
    GASOutputBuffers.push_back(reinterpret_cast<void*>(output_buffer));

    OptixTraversableHandle GASHandle;
    OPTIX_CHECK(optixAccelBuild
    (
        OptixContext, cuda::stream::default_stream_id,
        &accel_options, &TriangleInput, 1,
        temp_build_buffer, blas_buffer_size.tempSizeInBytes, output_buffer, blas_buffer_size.outputSizeInBytes,
        &GASHandle, nullptr, 0
    ));

    cuda::memory::device::free(reinterpret_cast<void*>(temp_build_buffer));

    return GASHandle;
}

template<typename T, uint_fast32_t V>
OptixTraversableHandle OptixRayTracer<T, V>::BuildIASAccel(OptixBuildOperation BuildOperationType)
{
    // Copy instances to device
    auto instance_input_buffer = TargetDevice.memory().allocate(sizeof(OptixInstance) * OptixInstances.size());
    cuda::memory::copy(instance_input_buffer, OptixInstances.data(), sizeof(OptixInstance) * OptixInstances.size());

    // configure instance input struct
    OptixBuildInput instance_input              = {};
    memset(&instance_input, 0, sizeof(OptixBuildInput));
    instance_input.type                         = OPTIX_BUILD_INPUT_TYPE_INSTANCES;
    instance_input.instanceArray.instances      = reinterpret_cast<CUdeviceptr>(instance_input_buffer);
    instance_input.instanceArray.numInstances   = OptixInstances.size();

    // configure accel build struct
    OptixAccelBuildOptions accel_options = {};
    memset(&accel_options, 0, sizeof(OptixAccelBuildOptions));
    accel_options.buildFlags             = OPTIX_BUILD_FLAG_ALLOW_UPDATE;
    accel_options.operation              = BuildOperationType;

    // Compute required memory
    OptixAccelBufferSizes blas_buffer_size;
    OPTIX_CHECK(optixAccelComputeMemoryUsage(
            OptixContext,
            &accel_options, &instance_input, 1/*One build input contains all IAS structs*/,
            &blas_buffer_size
    ));

    // if building IAS, allocate output memory; (Use same when updating) and set tempSize
    size_t temp_size;
    if (BuildOperationType == OPTIX_BUILD_OPERATION_BUILD)
    {
        temp_size = blas_buffer_size.tempSizeInBytes;
        IASOutputBuffer = TargetDevice.memory().allocate(blas_buffer_size.outputSizeInBytes);
    }
    else temp_size = blas_buffer_size.tempUpdateSizeInBytes;
    // allocate appropriate temp buffer
    CUdeviceptr temp_build_buffer   = reinterpret_cast<CUdeviceptr>(TargetDevice.memory().allocate(temp_size));

    OPTIX_CHECK(optixAccelBuild
    (
        OptixContext, cuda::stream::default_stream_id,
        &accel_options, &instance_input, 1,
        temp_build_buffer, temp_size, reinterpret_cast<CUdeviceptr>(IASOutputBuffer), blas_buffer_size.outputSizeInBytes,
        &TopLevelAccel, nullptr, 0
    ));

    cuda::memory::device::free(instance_input_buffer);
    cuda::memory::device::free(reinterpret_cast<void*>(temp_build_buffer));

    return TopLevelAccel;
}

template<typename T, uint_fast32_t V>
void OptixRayTracer<T, V>::CreateContext()
{
    std::cout << "Executing on device: " << TargetDevice.name() << std::endl;

    // Create optix context
    OPTIX_CHECK(optixInit());
    OPTIX_CHECK(optixDeviceContextCreate(0, 0, &OptixContext));
    OPTIX_CHECK(optixDeviceContextSetLogCallback(OptixContext, context_log_cb, nullptr, 4));

    std::cout << "Successfully initialized Optix Context" << std::endl;
}

template<typename T, uint_fast32_t V>
void OptixRayTracer<T, V>::CreateModule()
{
    // SET compile options
    ModuleCompileOptions.maxRegisterCount   = OPTIX_COMPILE_DEFAULT_MAX_REGISTER_COUNT;
    ModuleCompileOptions.optLevel           = OPTIX_COMPILE_OPTIMIZATION_LEVEL_0;
    ModuleCompileOptions.debugLevel         = OPTIX_COMPILE_DEBUG_LEVEL_FULL;

    PipelineCompileOptions = {};
    PipelineCompileOptions.traversableGraphFlags            = OPTIX_TRAVERSABLE_GRAPH_FLAG_ALLOW_ANY;
    PipelineCompileOptions.usesMotionBlur                   = false;
    PipelineCompileOptions.numPayloadValues                 = 3;
    PipelineCompileOptions.numAttributeValues               = 3;
    PipelineCompileOptions.exceptionFlags                   = OPTIX_EXCEPTION_FLAG_STACK_OVERFLOW;
    PipelineCompileOptions.pipelineLaunchParamsVariableName = "params";

    PipelineLinkOptions.maxTraceDepth = 5;
    PipelineLinkOptions.debugLevel    = OPTIX_COMPILE_DEBUG_LEVEL_FULL;

    // get embedded code
    const std::string ptx_code = PointIlluminationPTX;

    // allocate log
    char log[LOG_NUM];
    std::size_t sizeof_log = sizeof(log);

    // compile the embedded code
    OPTIX_CHECK(optixModuleCreateFromPTX
    (
        OptixContext,
        &ModuleCompileOptions,
        &PipelineCompileOptions,
        ptx_code.c_str(),
        ptx_code.size(),
        log, &sizeof_log,
        &Module
        ));

    if (sizeof(log) >0)
        std::cout << log << std::endl;
}

template<typename T, uint_fast32_t V>
void OptixRayTracer<T, V>::CreateRaygenProgram()
{
    RaygenPGs.resize(1);

    OptixProgramGroupOptions pgOptions = {};
    OptixProgramGroupDesc pgDesc    = {};
    pgDesc.kind                     = OPTIX_PROGRAM_GROUP_KIND_RAYGEN;
    pgDesc.raygen.module            = Module;
    pgDesc.raygen.entryFunctionName = "__raygen__tx_project";

    char log[2048];
    size_t sizeof_log = sizeof( log );
    OPTIX_CHECK(optixProgramGroupCreate
    (
        OptixContext,
        &pgDesc, 1, &pgOptions,
        log, &sizeof_log,
        &RaygenPGs[0]
    ));

    if (sizeof_log > 1) std::cout << (log) << std::endl;
}

template<typename T, uint_fast32_t V>
void OptixRayTracer<T, V>::CreateMissPrograms()
{
    MissPGs.resize(RAY_TYPE_COUNT);

    char log[2048];
    size_t sizeof_log = sizeof(log);

    OptixProgramGroupOptions pg_options = {};
    OptixProgramGroupDesc pg_desc       = {};
    pg_desc.kind                        = OPTIX_PROGRAM_GROUP_KIND_MISS;
    pg_desc.miss.module                 = Module;

    //TX RAY MISS
    pg_desc.miss.entryFunctionName = "__miss__tx_illuminate";
    OPTIX_CHECK(optixProgramGroupCreate(
            OptixContext,
            &pg_desc, 1, &pg_options,
            log, &sizeof_log,
            &MissPGs[TX_RAY_TYPE]
            ));
    if (sizeof_log > 1) std::cout << (log) << std::endl;

    //RX RAY MISS
    pg_desc.miss.entryFunctionName = "__miss__rx_shadow";
    OPTIX_CHECK(optixProgramGroupCreate
    (
        OptixContext,
        &pg_desc, 1, &pg_options,
        log, &sizeof_log,
        &MissPGs[RX_RAY_TYPE]
    ));
    if (sizeof_log > 1) std::cout << (log) << std::endl;

}

template<typename T, uint_fast32_t V>
void OptixRayTracer<T, V>::CreateHitgroupPrograms()
{
    HitgroupPGs.resize(RAY_TYPE_COUNT);

    char log[2048];
    size_t sizeof_log = sizeof( log );

    OptixProgramGroupOptions pg_options = {};
    OptixProgramGroupDesc pg_desc       = {};
    pg_desc.kind                        = OPTIX_PROGRAM_GROUP_KIND_HITGROUP;
    pg_desc.hitgroup.moduleCH           = Module;

    //TX RAY Closest Hit
    pg_desc.hitgroup.entryFunctionNameCH = "__closesthit__tx_illuminate";
    OPTIX_CHECK(optixProgramGroupCreate
    (
        OptixContext,
        &pg_desc, 1, &pg_options,
        log, &sizeof_log,
        &HitgroupPGs[TX_RAY_TYPE]
    ));

    //RX RAY Closest Hit
    pg_desc.hitgroup.entryFunctionNameCH = "__closesthit__rx_shadow";
    OPTIX_CHECK(optixProgramGroupCreate
    (
        OptixContext,
        &pg_desc, 1, &pg_options,
        log, &sizeof_log,
        &HitgroupPGs[RX_RAY_TYPE]
    ));
    if (sizeof_log > 1) std::cout << (log) << std::endl;
}

template<typename T, uint_fast32_t V>
void OptixRayTracer<T, V>::CreatePipeline()
{
    std::vector<OptixProgramGroup> program_groups;
    program_groups.insert(program_groups.cbegin(), RaygenPGs.cbegin(), RaygenPGs.cend());
    program_groups.insert(program_groups.cend(), MissPGs.cbegin(), MissPGs.cend());
    program_groups.insert(program_groups.cend(), HitgroupPGs.cbegin(), HitgroupPGs.cend());
    std::cout << "Loading a total of " << program_groups.size() << " program groups" << std::endl;

    char log[2048];
    size_t sizeof_log = sizeof( log );
    OPTIX_CHECK(optixPipelineCreate
    (
        OptixContext,
        &PipelineCompileOptions, &PipelineLinkOptions,
        program_groups.data(), program_groups.size(),
        log, &sizeof_log,
        &Pipeline
    ));
}

template<typename T, uint_fast32_t V>
void OptixRayTracer<T, V>::BuildSBT()
{
    memset(&SBT, 0, sizeof(OptixShaderBindingTable));
    // BUILD RAYGEN RECORDS
    std::vector<RaygenRecord> raygen_sbt_records;
    for (auto& pg : RaygenPGs)
    {
        RaygenRecord record;
        OPTIX_CHECK(optixSbtRecordPackHeader(pg, &record));
        raygen_sbt_records.push_back(record);
    }
    auto raygen_records_device = TargetDevice.memory().allocate(sizeof(RaygenRecord) * raygen_sbt_records.size());
    cuda::memory::copy(raygen_records_device, raygen_sbt_records.data(), sizeof(RaygenRecord) * raygen_sbt_records.size());
    SBT.raygenRecord = reinterpret_cast<CUdeviceptr>(raygen_records_device);

    // BUILD MISS RECORDS
    std::vector<MissRecord> miss_sbt_records;
    for (auto& pg : MissPGs)
    {
        MissRecord record;
        OPTIX_CHECK(optixSbtRecordPackHeader(pg, &record));
        miss_sbt_records.push_back(record);
    }
    auto miss_records_device = TargetDevice.memory().allocate(sizeof(MissRecord) * miss_sbt_records.size());
    cuda::memory::copy(miss_records_device, miss_sbt_records.data(), sizeof(MissRecord) * miss_sbt_records.size());
    SBT.missRecordBase          = reinterpret_cast<CUdeviceptr>(miss_records_device);
    SBT.missRecordCount         = miss_sbt_records.size();
    SBT.missRecordStrideInBytes = sizeof(MissRecord);

    // BUILD HIT RECORDS
    std::vector<HitgroupRecord<T, V>> hit_sbt_records;
    for (uint_fast32_t i = 0; i < RayTracer<T, V>::Env->GetObjectCount(); ++i)
    {
        for (auto& pg : HitgroupPGs)
        {
            //@TODO FIX DIRTY CASTS
            HitgroupRecord<T, V> record;
            const auto& host_obj = dynamic_pointer_cast<ArcaWave::Radar::RadarObject<T, V>>(RayTracer<T, V>::Env->GetObject(i));
            record.RadObj = LoadRadarObject(host_obj);
            OPTIX_CHECK(optixSbtRecordPackHeader(pg, &record));
            hit_sbt_records.push_back(record);
        }
    }
    auto hit_records_device = TargetDevice.memory().allocate(sizeof(HitgroupRecord<T, V>) * hit_sbt_records.size());
    cuda::memory::copy(hit_records_device, hit_sbt_records.data(), sizeof(HitgroupRecord<T, V>) * hit_sbt_records.size());
    SBT.hitgroupRecordBase          = reinterpret_cast<CUdeviceptr>(hit_records_device);
    SBT.hitgroupRecordCount         = hit_sbt_records.size();
    SBT.hitgroupRecordStrideInBytes = sizeof(HitgroupRecord<T, V>);

}

template<typename T, uint_fast32_t V>
ProjectedRay<T, V>* OptixRayTracer<T, V>::ProjectRaysDevice(const LaunchInfo<T, V> &Info)
{
    auto begin = chrono::system_clock::now();
    // Update transforms
    OptixInstances.clear();
    for (const auto& instance : RayTracer<T, V>::Env->GetInstances()) LoadIAS(instance);
    BuildIASAccel(OPTIX_BUILD_OPERATION_UPDATE);

    // construct launch parameters struct
    LaunchParameters<T, V> launch_parameters;
    launch_parameters.TXPosition = Info.Pos;
    launch_parameters.TXOrientation = Info.Ori;
    launch_parameters.NumRaysX = Info.Width;
    launch_parameters.NumRaysY = Info.Height;
    launch_parameters.Handle = TopLevelAccel;
    // allocate device memory for output
    launch_parameters.PerRayData = reinterpret_cast<ProjectedRay<T, V>*>(TargetDevice.memory().allocate(sizeof(ProjectedRay<T, V>) * Info.Width * MAX_BOUNCE * MAX_BOUNCE * Info.Height));

    // copy launch parameters to device;
    auto launch_parameters_device = TargetDevice.memory().allocate(sizeof(LaunchParameters<T, V>));
    cuda::memory::copy(launch_parameters_device, &launch_parameters, sizeof(LaunchParameters<T, V>));

    OPTIX_CHECK(optixLaunch
                        (
                                Pipeline, cuda::stream::default_stream_id,
                                reinterpret_cast<CUdeviceptr>(launch_parameters_device), sizeof(LaunchParameters<T, V>),
                                &SBT, Info.Width, Info.Height, 1
                        ));
    TargetDevice.synchronize();

    cuda::memory::device::free(launch_parameters_device);

    auto end = chrono::system_clock::now();
    chrono::duration<double> t = end - begin;
    std::cout << "OPTIX Completed in " << t.count() << std::endl;


    return launch_parameters.PerRayData;
}

template<typename T, uint_fast32_t V>
ArcaWave::Radar::DeviceRadarObject<T, V> OptixRayTracer<T, V>::LoadRadarObject(shared_ptr<ArcaWave::Radar::RadarObject<T, V>> Obj)
{
    std::vector<ArcaWave::Radar::StochasticVCM<T, V>> dev_vcms;
    std::vector<ArcaWave::Geometry::Partition<T, V>> dev_partitions;
    for (int i = 0; i < Obj->VCMCount; ++i)
    {
        const auto& host_vcm = Obj->VCMSet[i];
        auto dev_vcm = ArcaWave::Radar::StochasticVCM<T, V>(Obj->VCMSet[i]->Moments,
                                                            Obj->VCMSet[i]->Build->IncThetaInterval,
                                                            Obj->VCMSet[i]->Build->IncPhiInterval,
                                                            Obj->VCMSet[i]->Build->RefThetaInterval,
                                                            Obj->VCMSet[i]->Build->RefPhiInterval);
        dev_vcms.push_back(dev_vcm);
    }

    for(int i = 0; i < Obj->GetMeshCount(); ++i)
        dev_partitions.push_back(*(Obj->Partitions[i]));

    // Scan mesh count
    std::vector<uint_fast32_t> dev_mesh_index;
    for (int i = 0; i < Obj->GetMeshCount(); ++i)
        {
            if (i - 1 > 0) dev_mesh_index.push_back(Obj->GetMesh(i)->GetFaceCount() + dev_mesh_index[i - 1]);
            else dev_mesh_index.push_back(Obj->GetMesh(i)->GetFaceCount());
        }

    return ArcaWave::Radar::DeviceRadarObject<T, V>(dev_mesh_index.data(), dev_partitions.data(), dev_vcms.data(), Obj->VCMCount, Obj->GetMeshCount());
}


// UNIMPLEMENTED FOR NOW
template<typename T, uint_fast32_t V>
shared_ptr<IlluminatedInstance<T, V>> OptixRayTracer<T, V>::IlluminateInstance(const shared_ptr<IlluminateInstanceInfo<T, V>>& Info) const
{
    return make_shared<IlluminatedInstance<T, V>>();
}

template<typename T, uint_fast32_t V>
shared_ptr<IlluminatedEnvironment<T, V>> OptixRayTracer<T, V>::IlluminateEnvironment(const std::shared_ptr<IlluminateEnvironmentInfo<T, V>>& Info) const
{
    return make_shared<IlluminatedEnvironment<T, V>>();
}

template<typename T, uint_fast32_t V>
ProjectedRay<T, V> OptixRayTracer<T, V>::ProjectRay(const shared_ptr<ProjectRayInfo<T, V>>& Info) const
{
    return ProjectedRay<T, V>();
}

template class ArcaWave::Geometry::OptixRayTracer<float, 3>;
template class ArcaWave::Geometry::OptixRayTracer<double, 3>;
