
#include "Geometry/RayTracing/CPURayTracer.h"

// STL includes
#include <functional>
#include <iostream>

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Geometry;

template<typename T, uint_fast32_t V>
CPURayTracer<T, V>::CPURayTracer()
{}

template<typename T, uint_fast32_t V>
CPURayTracer<T, V>::CPURayTracer(const shared_ptr<Environment<T, V>>& env)
{
	RayTracer<T,V>::Env = env;
}

template<typename T, uint_fast32_t V>
CPURayTracer<T, V>::~CPURayTracer()
{}

template<typename T, uint_fast32_t V>
shared_ptr<IlluminatedInstance<T, V>> CPURayTracer<T, V>::IlluminateInstance(const shared_ptr<IlluminateInstanceInfo<T, V>>& Info) const
{
    // Chooses the function to illumination triangles with based on the given illumination type
    function<bool(const Triangle<T, V>&, const Matrix<T, V, 1>&, const Pose<T, V>&)> illum_func;
    switch(Info->Type)
    {
        // If we want to use point illumination
        case IlluminationType::ILLUMINATION_POINT:
            illum_func = function<bool(const Triangle<T, V>&, const Matrix<T, V, 1>&, const Pose<T, V>&)>(PointIllumination);
            break;

        // If we want to use directional illumination
        case IlluminationType::ILLUMINATION_DIRECTION:
            illum_func = function<bool(const Triangle<T, V>&, const Matrix<T, V, 1>&, const Pose<T, V>&)>(DirectionIllumination);
            break;
    }

    // Gets the pose from the instance
    const Pose<T, V> pose = Info->Inst->GetPose();

    // Initializes the illuminated instance structure
    shared_ptr<IlluminatedInstance<T, V>> illum_inst = make_shared<IlluminatedInstance<T, V>>();
    illum_inst->Obj = Info->Obj;
    illum_inst->Inst = Info->Inst;

    illum_inst->IlluminatedSubmeshes = shared_ptr<shared_ptr<Submesh>[]>(new shared_ptr<Submesh>[Info->Obj->GetMeshCount()]);

    // Illuminates each mesh of the instance individually
    const uint_fast32_t mesh_count = Info->Obj->GetMeshCount();
    for(uint_fast32_t m = 0; m < mesh_count; ++m)
    {
        // Makes the illuminated submesh
        illum_inst->IlluminatedSubmeshes[m] = make_shared<Submesh>();

        // Gets the mesh and checks if each face is illuminated or not
        const shared_ptr<Mesh<T, V>> mesh = illum_inst->Obj->GetMesh(m);
        for(uint_fast32_t f = 0; f < mesh->GetFaceCount(); ++f)
        {
            const Triangle<T, V> tri = mesh->GetTriangle(f);
            if(illum_func(tri, Info->Vec, pose))
                illum_inst->IlluminatedSubmeshes[m]->AddFace(f);
        }
    }

    // Goes back through each mesh and face to determine if some are shadowed by others
    for(uint_fast32_t m = 0; m < mesh_count; ++m)
    {
        // Gest the current mesh and illuminated submesh
        const shared_ptr<Mesh<T, V>> mesh = Info->Obj->GetMesh(m);
        shared_ptr<Submesh> submesh = illum_inst->IlluminatedSubmeshes[m];

        // Goes through each face in the submesh
        for(int_fast32_t f = submesh->GetFaceCount() - 1; f >= 0; --f)
        {
            // Gets the faces in the submesh
            const uint_fast32_t face = submesh->GetFace(f);

            // Determines the direction and origin of the ray used to check intersect
            const Matrix<T, V, 1> org = pose.TransformPoint(mesh->GetTriangle(face).Center());
            Matrix<T, V, 1> dir;
            switch(Info->Type)
            {
                // If we want to use point illumination
                case IlluminationType::ILLUMINATION_POINT:
                    dir = (Info->Vec - org);
                    break;

                // If we want to use directional illumination
                case IlluminationType::ILLUMINATION_DIRECTION:
                    dir = -Info->Vec;
                    break;
            }

            // Creates the ray to checking intersecting and check if it intersects any other faces in the instance
            const Ray<T, V> ray(org, dir);
            const shared_ptr<shared_ptr<Submesh>[]> intersecting = IntersectsInstance(illum_inst, ray);
            bool shadowed = false;
            for(uint_fast32_t inter_m = 0; inter_m < mesh_count; ++inter_m)
            {
                if(intersecting[inter_m]->IsEmpty() || (inter_m == m && intersecting[inter_m]->GetFaceCount() == 1 && intersecting[inter_m]->ContainsFace(face)))
                    continue;
                else
                {
                    shadowed = true;
                    break;
                }
            }

            // If the face is shadowed, remove it from this list of illuminated faces
            if(shadowed)
                submesh->RemoveFace(face);
        }
    }

    // Returns the list of illuminated submeshes
    return illum_inst;
}

template<typename T, uint_fast32_t V>
shared_ptr<IlluminatedEnvironment<T, V>> CPURayTracer<T, V>::IlluminateEnvironment(const std::shared_ptr<IlluminateEnvironmentInfo<T, V>>& Info) const
{
    // Creates the illuminated environment structure
    shared_ptr<IlluminatedEnvironment<T, V>> illum_env = make_shared<IlluminatedEnvironment<T, V>>();
    illum_env->Env = Info->Env;
    illum_env->IlluminatedInstances = shared_ptr<shared_ptr<IlluminatedInstance<T, V>>[]>(new shared_ptr<IlluminatedInstance<T, V>>[Info->Env->GetInstanceCount()]);

    // Goes through each instance in the environment and illuminates each individually
    for(uint_fast32_t i = 0; i < Info->Env->GetInstanceCount(); ++i)
    {
        // Creates the information struct necessary to illuminate the instance individually
        shared_ptr<IlluminateInstanceInfo<T, V>> inst_info = make_shared<IlluminateInstanceInfo<T, V>>();
        inst_info->Type = Info->Type;
        inst_info->Vec = Info->Vec;
        inst_info->Inst = Info->Env->GetInstance(i);
        inst_info->Obj = Info->Env->GetObject(inst_info->Inst->GetObjectIndex());

        // Individually illuminates the instance
        illum_env->IlluminatedInstances[i] = IlluminateInstance(inst_info);
    }

    // Goes throuch each instance and check if they are shadowed by other instances
    for(uint_fast32_t i = 0; i < Info->Env->GetInstanceCount(); ++i)
    {
        // Gets the necessary instance information refered to by the given ID
        const shared_ptr<Instance<T, V>> inst = Info->Env->GetInstance(i);
        const shared_ptr<Object<T, V>> obj = Info->Env->GetObject(inst->GetObjectIndex());
        const Pose<T, V> pose = inst->GetPose();
        const uint_fast32_t mesh_count = obj->GetMeshCount();
        shared_ptr<IlluminatedInstance<T, V>> illum_inst = illum_env->IlluminatedInstances[i];

        // Goes back through each mesh and face to determine if some are shadowed by some other instance
        for(uint_fast32_t m = 0; m < mesh_count; ++m)
        {
            const shared_ptr<Mesh<T, V>> mesh = obj->GetMesh(m);
            const shared_ptr<Submesh> submesh = illum_inst->IlluminatedSubmeshes[m];
            for(int_fast32_t f = submesh->GetFaceCount() - 1; f >= 0; --f)
            {
                // Gets the faces in the submesh
                const uint_fast32_t face = submesh->GetFace(f);

                // Determines the direction and origin of the ray used to check intersect
                const Matrix<T, V, 1> org = pose.TransformPoint(mesh->GetTriangle(face).Center());
                Matrix<T, V, 1> dir;
                switch(Info->Type)
                {
                    // If we want to use point illumination
                    case IlluminationType::ILLUMINATION_POINT:
                        dir = (Info->Vec - org);
                        break;

                    // If we want to use directional illumination
                    case IlluminationType::ILLUMINATION_DIRECTION:
                        dir = -Info->Vec;
                        break;
                }

                // Creates the ray to check against, and checks it against each other instance in the environment
                bool shadowed = false;
                const Ray<T, V> ray(org, dir);
                for(uint_fast32_t inter_i = 0; inter_i < Info->Env->GetInstanceCount(); ++inter_i)
                {
                    // If we are comparing to the exact same instance, just skip it
                    if(inter_i == i)
                        continue;

                    // Gets the other instance and its object
                    const shared_ptr<Instance<T, V>> other_inst = Info->Env->GetInstance(inter_i);
                    const shared_ptr<Object<T, V>> other_obj = Info->Env->GetObject(other_inst->GetObjectIndex());

                    // Checks for an intersecting face in the other instance 
                    const shared_ptr<shared_ptr<Submesh>[]> intersecting = IntersectsInstance(illum_env->IlluminatedInstances[inter_i], ray);
                    for(uint_fast32_t inter_m = 0; inter_m < other_obj->GetMeshCount(); ++inter_m)
                    {
                        // If there is an intersecting face, exit
                        if(!intersecting[inter_m]->IsEmpty())
                        {
                            shadowed = true;
                            break;
                        }
                    }

                    // If there is an intersecting face, just exit
                    if(shadowed)
                        break;
                }

                // If the face is shadowed, remove it from this list of illuminated faces
                if(shadowed)
                    submesh->RemoveFace(face);
            }
        }
    }

    // Returns the illuminated environment
    return illum_env;
}

template<typename T, uint_fast32_t V>
ProjectedRay<T, V> CPURayTracer<T, V>::ProjectRay(const shared_ptr<ProjectRayInfo<T, V>>& Info) const
{
    // Creates the projected ray return structure
    ProjectedRay<T, V> projected = ProjectedRay<T, V>();

    // Goes through each index and finds the index of the instance the closest intersects with the ray if it exists
	projected.Point = Eigen::Matrix<T, V, 1>{numeric_limits<T>::infinity(), numeric_limits<T>::infinity(), numeric_limits<T>::infinity()};
    shared_ptr<Environment<T, V>> env = Info->Env;
    for(uint_fast32_t i = 0; i < env->GetInstanceCount(); ++i)
    {
        // Creates the full illuminated instance (counts all faces as illuminated)
        shared_ptr<IlluminatedInstance<T, V>> illum_inst = make_shared<IlluminatedInstance<T, V>>();
        illum_inst->Inst = env->GetInstance(i);
        illum_inst->Obj = env->GetObject(illum_inst->Inst->GetObjectIndex());
        illum_inst->IlluminatedSubmeshes = shared_ptr<shared_ptr<Submesh>[]>(new shared_ptr<Submesh>[illum_inst->Obj->GetMeshCount()]);
        for(uint_fast32_t m = 0; m < illum_inst->Obj->GetMeshCount(); ++m)
            illum_inst->IlluminatedSubmeshes[m] = illum_inst->Obj->GetMesh(m)->GetFullSubmesh();

        // Gets the submesh with the intersecting faces
        shared_ptr<shared_ptr<Submesh>[]> intersecting = IntersectsInstance(illum_inst, Info->Ry);
        for(uint_fast32_t m = 0; m < illum_inst->Obj->GetMeshCount(); ++m)
        {
            shared_ptr<Mesh<T, V>> mesh = illum_inst->Obj->GetMesh(m);
            shared_ptr<Submesh> submesh = intersecting[m];
            if(!submesh->IsEmpty())
            {
                projected.Contact = true;
                //@TODO if we ever do multipath on the CPU, launch shadow ray
                projected.RxVisible = true;

                for(uint_fast32_t j = 0; j < submesh->GetFaceCount(); ++j)
                {
                    uint_fast32_t face = submesh->GetFace(j);
                    T new_dist = (illum_inst->Inst->GetPose().TransformPoint(mesh->GetTriangle(face).Center()) - Info->Ry.Origin).norm();
                    if(new_dist < projected.Point.norm())
                    {
                        //projected.InstanceIndex = i;
                        projected.Point = illum_inst->Inst->GetPose().TransformPoint(mesh->GetTriangle(face).Center());
                        projected.Dist = new_dist;
                        //projected.MeshID = m;
                        //projected.FaceID = face;
                    }
                }
            }
        }
    }

    // Returns the project ray
    return projected;
}

template<typename T, uint_fast32_t V>
shared_ptr<shared_ptr<Submesh>[]> CPURayTracer<T, V>::IntersectsInstance(const std::shared_ptr<IlluminatedInstance<T, V>>& Inst, const Ray<T, V>& R) const
{
    // Gets the pose of the instance
    const Pose<T, V> pose = Inst->Inst->GetPose();

    // Creates the list of submeshes that intersect with the ray of the given submeshes
    shared_ptr<shared_ptr<Submesh>[]> intersecting = shared_ptr<shared_ptr<Submesh>[]>(new shared_ptr<Submesh>[Inst->Obj->GetMeshCount()]);

    // Goes through each given submesh
    for(uint_fast32_t m = 0; m < Inst->Obj->GetMeshCount(); ++m)
    {
        // Creates the intersecting submesh
        intersecting[m] = make_shared<Submesh>();

        // Goes through each given face in the submesh
        const shared_ptr<Mesh<T, V>> mesh = Inst->Obj->GetMesh(m);
        const shared_ptr<Submesh> submesh = Inst->IlluminatedSubmeshes[m];
        for(uint_fast32_t f = 0; f < submesh->GetFaceCount(); ++f)
        {
            // Gets the index of the face
            const uint_fast32_t face = submesh->GetFace(f);

            // Transforms the triangle based on the instance's pose
            const Triangle<T, V> tri = mesh->GetTriangle(face);
            const Triad<T, V> trans_verts = {pose.TransformPoint(tri.Vertices.A), pose.TransformPoint(tri.Vertices.B), pose.TransformPoint(tri.Vertices.C)};
            const Triad<T, V> trans_norms = {pose.TransformVector(tri.Normals.A), pose.TransformVector(tri.Normals.B), pose.TransformVector(tri.Normals.C)};
            const Triangle<T, V> trans_tri(trans_verts, trans_norms);

            // If the transformed triangle intersects the ray, add it
            if(trans_tri.Intersects(R) != numeric_limits<T>::infinity())
                intersecting[m]->AddFace(face);
        }
    }

    // Returns the list of submeshes intersecting with the given ray of the given submeshes
    return intersecting;
}

template<typename T, uint_fast32_t V>
bool CPURayTracer<T, V>::PointIllumination(const Triangle<T, V>& Tri, const Matrix<T, V, 1>& Point, const Pose<T, V>& Pos)
{
    return (Pos.TransformPoint(Tri.Center()) - Point).normalized().dot(Pos.TransformVector(Tri.Normal())) < static_cast<T>(0);
}

template<typename T, uint_fast32_t V>
bool CPURayTracer<T, V>::DirectionIllumination(const Triangle<T, V>& Tri, const Matrix<T, V, 1>& Direction, const Pose<T, V>& Pos)
{
    return Direction.normalized().dot(Pos.TransformVector(Tri.Normal())) < static_cast<T>(0);
}

template<typename T, uint_fast32_t V>
vector<ProjectedRay<T, V>> CPURayTracer<T, V>::ProjectRays(const LaunchInfo<T, V>& Info)
{
	auto rays = std::vector<ProjectedRay<T, V>>();

	for (uint_fast32_t w = 0; w < Info.Width; ++w)
	{
		for (uint_fast32_t h = 0; h < Info.Height; ++h)
		{
			// Creates the ray
			// @TODO Change to spherical like optix
			const Matrix<T, V, 1> pos(Info.Pos.x() + Info.FocalLen, Info.Pos.y() - (Info.WinHeight/2) + (Info.WinHeight/Info.Height * h), Info.Pos.z() - (Info.WinWidth/2) + (Info.WinWidth/Info.Width * w));
			const Ray<T, V> ry(Info.Pos, (pos - Info.Pos).normalized());

			shared_ptr<ProjectRayInfo<T, V>> proj_info = make_shared<ProjectRayInfo<T, V>>();
			proj_info->Ry = ry;
			proj_info->Env = RayTracer<T, V>::Env;
			ProjectedRay<T, V> projected = ProjectRay(proj_info);

			if (projected.Contact)
			{
				rays.push_back(projected);
			}
		}
	}
	return rays;
}

template class ArcaWave::Geometry::CPURayTracer<float, 3>;
template class ArcaWave::Geometry::CPURayTracer<double, 3>;
