#include <Geometry/AABB.h>

// STL Includes
#include <algorithm>

template<typename T, uint_fast32_t V>
ArcaWave::Geometry::AABB<T, V>::AABB()
{
	Max.setZero();
	Min.setZero();
}

template<typename T, uint_fast32_t V>
ArcaWave::Geometry::AABB<T, V>::AABB(Eigen::Matrix<T, V, 1> Max, Eigen::Matrix<T, V, 1> Min) : Max(Max), Min(Min)
{}

template<typename T, uint_fast32_t V>
bool ArcaWave::Geometry::AABB<T, V>::PointInAABB(Eigen::Matrix<T, V, 1> Point)
{
	return (Point.x() >=Min.x() && Point.x() <= Max.x())
	&& (Point.y() >= Min.y() && Point.y() <= Max.y())
	&& (Point.z() >= Min.z() && Point.z() <= Max.z());
}

template<typename T, uint_fast32_t V>
bool ArcaWave::Geometry::AABB<T, V>::Intersects(const AABB<T, V>& Other)
{
	return (Min.x() <= Other.Max.x() && Max.x() >= Other.Min.x()) &&
		(Min.y() <= Other.Max.y() && Max.y() >= Other.Min.y()) &&
		(Min.z() <= Other.Max.z() && Max.z() >= Other.Min.z());
}

template<typename T, uint_fast32_t V>
ArcaWave::Geometry::AABB<T, V> ArcaWave::Geometry::AABB<T, V>::Union(const ArcaWave::Geometry::AABB<T, V>& Other)
{
	Eigen::Matrix<T, V, 1> new_max =
	{
			std::max(Max.x(), Other.Max.x()),
			std::max(Max.y(), Other.Max.y()),
			std::max(Max.z(), Other.Max.z())
	};

	Eigen::Matrix<T, V, 1> new_min =
		{
			std::min(Min.x(), Other.Min.x()),
			std::min(Min.y(), Other.Min.y()),
			std::min(Min.z(), Other.Min.z())
		};

	return ArcaWave::Geometry::AABB<T, V>(new_max, new_min);
}

template<typename T, uint_fast32_t V>
uint_fast32_t ArcaWave::Geometry::AABB<T, V>::NumPointsInAABB(const std::vector<Eigen::Matrix<T, V, 1>>& Points)
{
	uint_fast32_t count = 0;
	for (auto const& point : Points)
	{
		if (PointInAABB(point)) ++count;
	}
	return count;
}

template class ArcaWave::Geometry::AABB<float, 3>;
template class ArcaWave::Geometry::AABB<double, 3>;