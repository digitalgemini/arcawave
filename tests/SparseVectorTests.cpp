
// STL includes
#include <random>
#include <vector>
#include <thread>
#include <future>

// Catch includes
#include <catch2/catch.hpp>

// ArcaWave includes
#include "Utility/SparseVector.hpp"

using namespace std;
using namespace ArcaWave::Utility;

//Test the container using ints
typedef int T;

auto insert_seq = [](SparseVector<T>& container, int start,  int end)
{
	for (int i = start; i < end; i++)
	{
		container[i] = i;
	}
};

auto check_seq = [](SparseVector<T>& container, int start,  int end)
{
  for (int i = start; i < end; i++)
  {
	  if (T(container[i]) != i) return false;
  }
  return true;
};


TEST_CASE("Sparse Vector Tests", "[SparseVector]")
{
	// Creates the test Sparse Vector
	auto DUT = SparseVector<T>(-1);

	// Checks to make sure the constructor worked properly
	SECTION("Sequential Inserts")
	{
		// init container
		insert_seq(DUT, 0, 100);

		// Sort
		DUT.Sort();

		// check contents
		for (int i = 0; i < 100; ++i) CHECK(i == T(DUT[i]));
	}

	SECTION("Random Inserts")
	{
		//refrence container
		auto ref_container = std::vector<std::pair<int, T>>();

		std::default_random_engine engine(0);
		std::uniform_int_distribution<T> uniform_dist(1, 1000);
		for (int i = 0; i < 100; ++i)
		{
			int idx = uniform_dist(engine);

			//Check if already inserted
			DUT.Sort();
			if (DUT[idx] != -1) continue;

			DUT[idx] = i;
			ref_container.emplace_back(idx, i);
		}
		// Sort
		DUT.Sort();

		// check contents
		for (const auto& pair : ref_container) CHECK(pair.second == T(DUT[pair.first]));
	}

	SECTION("Missing Values return default value")
	{
		// Fill only even values
		for (int i = 0; i < 50; i+=2) DUT[i] = i;


		DUT.Sort();

		// check all values
		for (int i = 0; i < 50; ++i)
		{
			if (i % 2 == 0) CHECK( T(DUT[i]) == i);
			else CHECK( T(DUT[i]) == -1);
		}
	}

	SECTION("Concurrent Inserts and Reads")
	{
		std::vector<std::future<void>> execs;

		for (int i = 0; i < 3; ++i) execs.push_back(std::async(std::launch::async, insert_seq, std::ref(DUT), i*100, (i+1)*100));
		for (auto& exec : execs) exec.wait();

		DUT.Sort();

		std::vector<std::future<bool>> checks;

		for (int i = 0; i < 3; ++i) checks.push_back(std::async(std::launch::async, check_seq, std::ref(DUT), i*100, (i+1)*100));
		for (auto& check : checks) CHECK(check.get() == true);
	}

	SECTION("Saving Container to disk and reading it back")
	{
		//refrence container
		auto ref_container = std::vector<std::pair<int, T>>();

		std::default_random_engine engine(0);
		std::uniform_int_distribution<T> uniform_dist(1, 1000);
		for (int i = 0; i < 100; ++i)
		{
			int idx = uniform_dist(engine);

			//Check if already inserted
			DUT.Sort();
			if (DUT[idx] != -1) continue;

			DUT[idx] = i;
			ref_container.emplace_back(idx, i);
		}
		// Sort
		DUT.Sort();

		DUT.SaveToFile("test_run_sparse.bin");

		SparseVector<T> DiskDUT(-1);
		DiskDUT.ReadFromFile("test_run_sparse.bin");

		// check contents
		for (const auto& pair : ref_container) CHECK(pair.second == T(DiskDUT[pair.first]));
	}
}
