
/**
 * @file dox.h
 * 
 * @brief General documentation for the ArcaWave project.
 */

/**
 @mainpage ArcaWave - Domain Agnostic Simulation Platform

 @section intro Introduction

 This is an introduction.

 */

/**
 @page template Templatization

 This page describes generally describes the templatization decisions through ArcaWave.

 T - The floating-point type to use for this object. Should either be single precision (float) or double precision (double). \
 V - The number of entries used in the vector types for the object. Should either be 3 for efficent memory use but no SIMD optimization, or 4 for less efficent memory 
    use but actual SIMD optimization. Note, these will still represent three-dimensional vectors in either case, thus this parameter is purely for optimization purposes, and the
    fourth element should generally not be used if included.
 */
